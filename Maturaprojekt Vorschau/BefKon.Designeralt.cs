﻿namespace Maturaprojekt_Vorschau
{
    partial class BefKon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nein = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.lblBirthday = new System.Windows.Forms.Label();
            this.lblAge = new System.Windows.Forms.Label();
            this.lblTelefone = new System.Windows.Forms.Label();
            this.lblGender = new System.Windows.Forms.Label();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdSave = new System.Windows.Forms.Button();
            this.lblClient = new System.Windows.Forms.Label();
            this.lblBefKonNr = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tabAnamnese = new System.Windows.Forms.TabPage();
            this.anamUmgang = new Maturaprojekt_Vorschau.ownTextbox();
            this.anamEltern = new Maturaprojekt_Vorschau.ownTextbox();
            this.anamEntw = new Maturaprojekt_Vorschau.ownTextbox();
            this.anamFami = new Maturaprojekt_Vorschau.ownTextbox();
            this.button6 = new System.Windows.Forms.Button();
            this.tabAllgemeines = new System.Windows.Forms.TabPage();
            this.allgDiagnose = new Maturaprojekt_Vorschau.ownTextbox();
            this.allgSymptomatik = new Maturaprojekt_Vorschau.ownTextbox();
            this.dtpEnde = new System.Windows.Forms.DateTimePicker();
            this.dtpBeginn = new System.Windows.Forms.DateTimePicker();
            this.dtpAnmeldedatum = new System.Windows.Forms.DateTimePicker();
            this.lblUberweiser = new System.Windows.Forms.Label();
            this.lblDiverses = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblFallfuehrender = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblFallfuehrend2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblInterventionskategorie = new System.Windows.Forms.Label();
            this.cBFall = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lblBehandlungsanlass = new System.Windows.Forms.Label();
            this.cBFallZwei = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.gBSetting = new System.Windows.Forms.GroupBox();
            this.B_Anzeige = new System.Windows.Forms.CheckBox();
            this.B_Gefaehrdungsmeldung = new System.Windows.Forms.CheckBox();
            this.B_FU = new System.Windows.Forms.CheckBox();
            this.txtUeberweiser = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.cBBehandlungsanlass = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cBInterventionskategorie = new System.Windows.Forms.ComboBox();
            this.cmsDgvFiles = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.öffnenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.löschenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabKontakte = new System.Windows.Forms.TabPage();
            this.cmdAddFile = new System.Windows.Forms.Button();
            this.dgvFiles = new System.Windows.Forms.DataGridView();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.c_Kommentar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblPTyp = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnUpdatePers = new System.Windows.Forms.Button();
            this.dgvPERS = new System.Windows.Forms.DataGridView();
            this.c_pnr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmPTyp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.c_vorname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.c_Nachname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.c_Gebd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnNewPers = new System.Windows.Forms.Button();
            this.btnAddPers = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnUpdateIns = new System.Windows.Forms.Button();
            this.dgvINST = new System.Windows.Forms.DataGridView();
            this.c_INr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.c_Bezeichnung = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.c_Kategorie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_Ansprechpartner = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnNewIns = new System.Windows.Forms.Button();
            this.btnAddIns = new System.Windows.Forms.Button();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.txtAnmerkungen = new Maturaprojekt_Vorschau.ownTextbox();
            this.ownTextbox1 = new Maturaprojekt_Vorschau.ownTextbox();
            this.allgAuftrag = new Maturaprojekt_Vorschau.ownTextbox();
            this.allgZiel = new Maturaprojekt_Vorschau.ownTextbox();
            this.allgMedikation = new Maturaprojekt_Vorschau.ownTextbox();
            this.txtAnmerkungen2 = new Maturaprojekt_Vorschau.ownTextbox();
            this.tabAnamnese.SuspendLayout();
            this.tabAllgemeines.SuspendLayout();
            this.gBSetting.SuspendLayout();
            this.cmsDgvFiles.SuspendLayout();
            this.tabKontakte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFiles)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPERS)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvINST)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nachname";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(137, 9);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Vorname";
            // 
            // nein
            // 
            this.nein.AutoSize = true;
            this.nein.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nein.Location = new System.Drawing.Point(236, 9);
            this.nein.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nein.Name = "nein";
            this.nein.Size = new System.Drawing.Size(110, 17);
            this.nein.TabIndex = 2;
            this.nein.Text = "Geburtsdatum";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(362, 9);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Alter";
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label.Location = new System.Drawing.Point(441, 9);
            this.label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(120, 17);
            this.label.TabIndex = 4;
            this.label.Text = "Telefonnummer";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(584, 9);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Geschlecht";
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastName.Location = new System.Drawing.Point(29, 29);
            this.lblLastName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(13, 17);
            this.lblLastName.TabIndex = 6;
            this.lblLastName.Text = "-";
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirstName.Location = new System.Drawing.Point(137, 29);
            this.lblFirstName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(13, 17);
            this.lblFirstName.TabIndex = 7;
            this.lblFirstName.Text = "-";
            // 
            // lblBirthday
            // 
            this.lblBirthday.AutoSize = true;
            this.lblBirthday.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBirthday.Location = new System.Drawing.Point(236, 29);
            this.lblBirthday.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBirthday.Name = "lblBirthday";
            this.lblBirthday.Size = new System.Drawing.Size(13, 17);
            this.lblBirthday.TabIndex = 8;
            this.lblBirthday.Text = "-";
            // 
            // lblAge
            // 
            this.lblAge.AutoSize = true;
            this.lblAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAge.Location = new System.Drawing.Point(362, 29);
            this.lblAge.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAge.Name = "lblAge";
            this.lblAge.Size = new System.Drawing.Size(13, 17);
            this.lblAge.TabIndex = 9;
            this.lblAge.Text = "-";
            // 
            // lblTelefone
            // 
            this.lblTelefone.AutoSize = true;
            this.lblTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefone.Location = new System.Drawing.Point(441, 29);
            this.lblTelefone.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.Size = new System.Drawing.Size(13, 17);
            this.lblTelefone.TabIndex = 10;
            this.lblTelefone.Text = "-";
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGender.Location = new System.Drawing.Point(584, 29);
            this.lblGender.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(13, 17);
            this.lblGender.TabIndex = 11;
            this.lblGender.Text = "-";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(1141, 669);
            this.cmdCancel.Margin = new System.Windows.Forms.Padding(2);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(89, 30);
            this.cmdCancel.TabIndex = 77;
            this.cmdCancel.Text = "Abbrechen";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSave.Location = new System.Drawing.Point(1234, 669);
            this.cmdSave.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(90, 30);
            this.cmdSave.TabIndex = 76;
            this.cmdSave.Text = "Speichern";
            this.cmdSave.UseVisualStyleBackColor = true;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // lblClient
            // 
            this.lblClient.AutoSize = true;
            this.lblClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClient.Location = new System.Drawing.Point(865, 29);
            this.lblClient.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblClient.Name = "lblClient";
            this.lblClient.Size = new System.Drawing.Size(13, 17);
            this.lblClient.TabIndex = 81;
            this.lblClient.Text = "-";
            // 
            // lblBefKonNr
            // 
            this.lblBefKonNr.AutoSize = true;
            this.lblBefKonNr.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBefKonNr.Location = new System.Drawing.Point(722, 29);
            this.lblBefKonNr.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBefKonNr.Name = "lblBefKonNr";
            this.lblBefKonNr.Size = new System.Drawing.Size(13, 17);
            this.lblBefKonNr.TabIndex = 80;
            this.lblBefKonNr.Text = "-";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(865, 9);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(124, 17);
            this.label15.TabIndex = 79;
            this.label15.Text = "Klientennummer";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(722, 9);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(77, 17);
            this.label16.TabIndex = 78;
            this.label16.Text = "BefKonNr";
            // 
            // tabAnamnese
            // 
            this.tabAnamnese.AutoScroll = true;
            this.tabAnamnese.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabAnamnese.Controls.Add(this.anamUmgang);
            this.tabAnamnese.Controls.Add(this.anamEltern);
            this.tabAnamnese.Controls.Add(this.anamEntw);
            this.tabAnamnese.Controls.Add(this.anamFami);
            this.tabAnamnese.Controls.Add(this.button6);
            this.tabAnamnese.Location = new System.Drawing.Point(4, 27);
            this.tabAnamnese.Margin = new System.Windows.Forms.Padding(2);
            this.tabAnamnese.Name = "tabAnamnese";
            this.tabAnamnese.Padding = new System.Windows.Forms.Padding(2);
            this.tabAnamnese.Size = new System.Drawing.Size(1539, 516);
            this.tabAnamnese.TabIndex = 2;
            this.tabAnamnese.Text = "Anamnesen";
            // 
            // anamUmgang
            // 
            this.anamUmgang.editTitle = "Umgangskontakte";
            this.anamUmgang.Location = new System.Drawing.Point(97, 2140);
            this.anamUmgang.Margin = new System.Windows.Forms.Padding(4, 129, 4, 129);
            this.anamUmgang.Name = "anamUmgang";
            this.anamUmgang.Size = new System.Drawing.Size(4124, 525);
            this.anamUmgang.TabIndex = 82;
            // 
            // anamEltern
            // 
            this.anamEltern.editTitle = "Elternanamnese";
            this.anamEltern.Location = new System.Drawing.Point(97, 1544);
            this.anamEltern.Margin = new System.Windows.Forms.Padding(4, 129, 4, 129);
            this.anamEltern.Name = "anamEltern";
            this.anamEltern.Size = new System.Drawing.Size(4124, 525);
            this.anamEltern.TabIndex = 81;
            // 
            // anamEntw
            // 
            this.anamEntw.editTitle = "Entwicklungsanamnese";
            this.anamEntw.Location = new System.Drawing.Point(97, 947);
            this.anamEntw.Margin = new System.Windows.Forms.Padding(4, 129, 4, 129);
            this.anamEntw.Name = "anamEntw";
            this.anamEntw.Size = new System.Drawing.Size(4124, 525);
            this.anamEntw.TabIndex = 80;
            // 
            // anamFami
            // 
            this.anamFami.editTitle = "Familienanamnese";
            this.anamFami.Location = new System.Drawing.Point(97, 349);
            this.anamFami.Margin = new System.Windows.Forms.Padding(4, 129, 4, 129);
            this.anamFami.Name = "anamFami";
            this.anamFami.Size = new System.Drawing.Size(4124, 525);
            this.anamFami.TabIndex = 79;
            this.anamFami.Load += new System.EventHandler(this.ownTextbox1_Load);
            // 
            // button6
            // 
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Location = new System.Drawing.Point(33, 43);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(105, 31);
            this.button6.TabIndex = 78;
            this.button6.Text = "hinzufügen";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // tabAllgemeines
            // 
            this.tabAllgemeines.AutoScroll = true;
            this.tabAllgemeines.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabAllgemeines.Controls.Add(this.allgMedikation);
            this.tabAllgemeines.Controls.Add(this.allgZiel);
            this.tabAllgemeines.Controls.Add(this.allgAuftrag);
            this.tabAllgemeines.Controls.Add(this.allgDiagnose);
            this.tabAllgemeines.Controls.Add(this.allgSymptomatik);
            this.tabAllgemeines.Controls.Add(this.dtpEnde);
            this.tabAllgemeines.Controls.Add(this.dtpBeginn);
            this.tabAllgemeines.Controls.Add(this.dtpAnmeldedatum);
            this.tabAllgemeines.Controls.Add(this.lblUberweiser);
            this.tabAllgemeines.Controls.Add(this.lblDiverses);
            this.tabAllgemeines.Controls.Add(this.label7);
            this.tabAllgemeines.Controls.Add(this.lblFallfuehrender);
            this.tabAllgemeines.Controls.Add(this.label5);
            this.tabAllgemeines.Controls.Add(this.lblFallfuehrend2);
            this.tabAllgemeines.Controls.Add(this.label3);
            this.tabAllgemeines.Controls.Add(this.label8);
            this.tabAllgemeines.Controls.Add(this.lblInterventionskategorie);
            this.tabAllgemeines.Controls.Add(this.cBFall);
            this.tabAllgemeines.Controls.Add(this.label9);
            this.tabAllgemeines.Controls.Add(this.lblBehandlungsanlass);
            this.tabAllgemeines.Controls.Add(this.cBFallZwei);
            this.tabAllgemeines.Controls.Add(this.label10);
            this.tabAllgemeines.Controls.Add(this.gBSetting);
            this.tabAllgemeines.Controls.Add(this.txtUeberweiser);
            this.tabAllgemeines.Controls.Add(this.button4);
            this.tabAllgemeines.Controls.Add(this.cBBehandlungsanlass);
            this.tabAllgemeines.Controls.Add(this.label12);
            this.tabAllgemeines.Controls.Add(this.label11);
            this.tabAllgemeines.Controls.Add(this.cBInterventionskategorie);
            this.tabAllgemeines.Location = new System.Drawing.Point(4, 27);
            this.tabAllgemeines.Margin = new System.Windows.Forms.Padding(2);
            this.tabAllgemeines.Name = "tabAllgemeines";
            this.tabAllgemeines.Padding = new System.Windows.Forms.Padding(2);
            this.tabAllgemeines.Size = new System.Drawing.Size(1305, 468);
            this.tabAllgemeines.TabIndex = 1;
            this.tabAllgemeines.Text = "Allgemeines";
            // 
            // allgDiagnose
            // 
            this.allgDiagnose.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.allgDiagnose.editTitle = "Diagnose";
            this.allgDiagnose.Location = new System.Drawing.Point(6, 418);
            this.allgDiagnose.Margin = new System.Windows.Forms.Padding(4, 145, 4, 145);
            this.allgDiagnose.Name = "allgDiagnose";
            this.allgDiagnose.Size = new System.Drawing.Size(372, 173);
            this.allgDiagnose.TabIndex = 13;
            // 
            // allgSymptomatik
            // 
            this.allgSymptomatik.editTitle = "Symptomatik";
            this.allgSymptomatik.Location = new System.Drawing.Point(6, 232);
            this.allgSymptomatik.Margin = new System.Windows.Forms.Padding(4, 145, 4, 145);
            this.allgSymptomatik.Name = "allgSymptomatik";
            this.allgSymptomatik.Size = new System.Drawing.Size(372, 177);
            this.allgSymptomatik.TabIndex = 12;
            // 
            // dtpEnde
            // 
            this.dtpEnde.Location = new System.Drawing.Point(119, 88);
            this.dtpEnde.Name = "dtpEnde";
            this.dtpEnde.Size = new System.Drawing.Size(270, 24);
            this.dtpEnde.TabIndex = 3;
            // 
            // dtpBeginn
            // 
            this.dtpBeginn.Location = new System.Drawing.Point(119, 51);
            this.dtpBeginn.Name = "dtpBeginn";
            this.dtpBeginn.Size = new System.Drawing.Size(270, 24);
            this.dtpBeginn.TabIndex = 2;
            // 
            // dtpAnmeldedatum
            // 
            this.dtpAnmeldedatum.Location = new System.Drawing.Point(119, 14);
            this.dtpAnmeldedatum.Name = "dtpAnmeldedatum";
            this.dtpAnmeldedatum.Size = new System.Drawing.Size(270, 24);
            this.dtpAnmeldedatum.TabIndex = 1;
            // 
            // lblUberweiser
            // 
            this.lblUberweiser.AutoSize = true;
            this.lblUberweiser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUberweiser.Location = new System.Drawing.Point(230, 151);
            this.lblUberweiser.Name = "lblUberweiser";
            this.lblUberweiser.Size = new System.Drawing.Size(14, 18);
            this.lblUberweiser.TabIndex = 90;
            this.lblUberweiser.Text = "-";
            // 
            // lblDiverses
            // 
            this.lblDiverses.AutoSize = true;
            this.lblDiverses.Location = new System.Drawing.Point(1067, 127);
            this.lblDiverses.Name = "lblDiverses";
            this.lblDiverses.Size = new System.Drawing.Size(74, 18);
            this.lblDiverses.TabIndex = 87;
            this.lblDiverses.Text = "Diversees";
            this.lblDiverses.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 93);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 18);
            this.label7.TabIndex = 4;
            this.label7.Text = "Ende";
            // 
            // lblFallfuehrender
            // 
            this.lblFallfuehrender.AutoSize = true;
            this.lblFallfuehrender.Location = new System.Drawing.Point(917, 164);
            this.lblFallfuehrender.Name = "lblFallfuehrender";
            this.lblFallfuehrender.Size = new System.Drawing.Size(88, 18);
            this.lblFallfuehrender.TabIndex = 81;
            this.lblFallfuehrender.Text = "Fallfuehrend";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 18);
            this.label5.TabIndex = 2;
            this.label5.Text = "Beginn";
            // 
            // lblFallfuehrend2
            // 
            this.lblFallfuehrend2.AutoSize = true;
            this.lblFallfuehrend2.Location = new System.Drawing.Point(774, 136);
            this.lblFallfuehrend2.Name = "lblFallfuehrend2";
            this.lblFallfuehrend2.Size = new System.Drawing.Size(96, 18);
            this.lblFallfuehrend2.TabIndex = 80;
            this.lblFallfuehrend2.Text = "Fallfuehrend2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 18);
            this.label3.TabIndex = 0;
            this.label3.Text = "Anmeldedatum";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(410, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(166, 18);
            this.label8.TabIndex = 62;
            this.label8.Text = "Fallführender Mitarbeiter";
            // 
            // lblInterventionskategorie
            // 
            this.lblInterventionskategorie.AutoSize = true;
            this.lblInterventionskategorie.Location = new System.Drawing.Point(917, 127);
            this.lblInterventionskategorie.Name = "lblInterventionskategorie";
            this.lblInterventionskategorie.Size = new System.Drawing.Size(91, 18);
            this.lblInterventionskategorie.TabIndex = 79;
            this.lblInterventionskategorie.Text = "Interventions";
            // 
            // cBFall
            // 
            this.cBFall.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBFall.FormattingEnabled = true;
            this.cBFall.Location = new System.Drawing.Point(615, 12);
            this.cBFall.Name = "cBFall";
            this.cBFall.Size = new System.Drawing.Size(169, 26);
            this.cBFall.Sorted = true;
            this.cBFall.TabIndex = 4;
            this.cBFall.SelectedIndexChanged += new System.EventHandler(this.cBFall_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(410, 45);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(182, 18);
            this.label9.TabIndex = 64;
            this.label9.Text = "2. Fallführender Mitarbeiter";
            // 
            // lblBehandlungsanlass
            // 
            this.lblBehandlungsanlass.AutoSize = true;
            this.lblBehandlungsanlass.Location = new System.Drawing.Point(1067, 174);
            this.lblBehandlungsanlass.Name = "lblBehandlungsanlass";
            this.lblBehandlungsanlass.Size = new System.Drawing.Size(136, 18);
            this.lblBehandlungsanlass.TabIndex = 78;
            this.lblBehandlungsanlass.Text = "Behandlungsanlass";
            // 
            // cBFallZwei
            // 
            this.cBFallZwei.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBFallZwei.FormattingEnabled = true;
            this.cBFallZwei.Location = new System.Drawing.Point(615, 44);
            this.cBFallZwei.Name = "cBFallZwei";
            this.cBFallZwei.Size = new System.Drawing.Size(169, 26);
            this.cBFallZwei.Sorted = true;
            this.cBFallZwei.TabIndex = 5;
            this.cBFallZwei.SelectedIndexChanged += new System.EventHandler(this.cBFallZwei_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 151);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(179, 18);
            this.label10.TabIndex = 66;
            this.label10.Text = "Überweisende Einrichtung";
            // 
            // gBSetting
            // 
            this.gBSetting.Controls.Add(this.B_Anzeige);
            this.gBSetting.Controls.Add(this.B_Gefaehrdungsmeldung);
            this.gBSetting.Controls.Add(this.B_FU);
            this.gBSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gBSetting.Location = new System.Drawing.Point(529, 93);
            this.gBSetting.Name = "gBSetting";
            this.gBSetting.Size = new System.Drawing.Size(191, 131);
            this.gBSetting.TabIndex = 77;
            this.gBSetting.TabStop = false;
            this.gBSetting.Text = "Diverses";
            // 
            // B_Anzeige
            // 
            this.B_Anzeige.AutoSize = true;
            this.B_Anzeige.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.B_Anzeige.Location = new System.Drawing.Point(14, 98);
            this.B_Anzeige.Margin = new System.Windows.Forms.Padding(4);
            this.B_Anzeige.Name = "B_Anzeige";
            this.B_Anzeige.Size = new System.Drawing.Size(82, 22);
            this.B_Anzeige.TabIndex = 11;
            this.B_Anzeige.Text = "Anzeige";
            this.B_Anzeige.UseVisualStyleBackColor = true;
            // 
            // B_Gefaehrdungsmeldung
            // 
            this.B_Gefaehrdungsmeldung.AutoSize = true;
            this.B_Gefaehrdungsmeldung.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.B_Gefaehrdungsmeldung.Location = new System.Drawing.Point(14, 64);
            this.B_Gefaehrdungsmeldung.Margin = new System.Windows.Forms.Padding(4);
            this.B_Gefaehrdungsmeldung.Name = "B_Gefaehrdungsmeldung";
            this.B_Gefaehrdungsmeldung.Size = new System.Drawing.Size(171, 22);
            this.B_Gefaehrdungsmeldung.TabIndex = 10;
            this.B_Gefaehrdungsmeldung.Text = "Gefährdungsmeldung";
            this.B_Gefaehrdungsmeldung.UseVisualStyleBackColor = true;
            // 
            // B_FU
            // 
            this.B_FU.AutoSize = true;
            this.B_FU.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.B_FU.Location = new System.Drawing.Point(14, 30);
            this.B_FU.Margin = new System.Windows.Forms.Padding(4);
            this.B_FU.Name = "B_FU";
            this.B_FU.Size = new System.Drawing.Size(50, 22);
            this.B_FU.TabIndex = 9;
            this.B_FU.Text = "FU";
            this.B_FU.UseVisualStyleBackColor = true;
            // 
            // txtUeberweiser
            // 
            this.txtUeberweiser.AllowDrop = true;
            this.txtUeberweiser.Location = new System.Drawing.Point(266, 148);
            this.txtUeberweiser.Name = "txtUeberweiser";
            this.txtUeberweiser.ReadOnly = true;
            this.txtUeberweiser.Size = new System.Drawing.Size(159, 24);
            this.txtUeberweiser.TabIndex = 67;
            // 
            // button4
            // 
            this.button4.AutoSize = true;
            this.button4.BackgroundImage = global::Maturaprojekt_Vorschau.Properties.Resources.search_icon;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Location = new System.Drawing.Point(431, 144);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(36, 33);
            this.button4.TabIndex = 8;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // cBBehandlungsanlass
            // 
            this.cBBehandlungsanlass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBBehandlungsanlass.FormattingEnabled = true;
            this.cBBehandlungsanlass.Location = new System.Drawing.Point(960, 11);
            this.cBBehandlungsanlass.Name = "cBBehandlungsanlass";
            this.cBBehandlungsanlass.Size = new System.Drawing.Size(240, 26);
            this.cBBehandlungsanlass.Sorted = true;
            this.cBBehandlungsanlass.TabIndex = 6;
            this.cBBehandlungsanlass.SelectedIndexChanged += new System.EventHandler(this.cBVorstellungsgrund_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(802, 47);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(152, 18);
            this.label12.TabIndex = 73;
            this.label12.Text = "Interventionskategorie";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(802, 14);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(136, 18);
            this.label11.TabIndex = 75;
            this.label11.Text = "Behandlungsanlass";
            // 
            // cBInterventionskategorie
            // 
            this.cBInterventionskategorie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBInterventionskategorie.FormattingEnabled = true;
            this.cBInterventionskategorie.Location = new System.Drawing.Point(960, 44);
            this.cBInterventionskategorie.Name = "cBInterventionskategorie";
            this.cBInterventionskategorie.Size = new System.Drawing.Size(240, 26);
            this.cBInterventionskategorie.Sorted = true;
            this.cBInterventionskategorie.TabIndex = 7;
            this.cBInterventionskategorie.SelectedIndexChanged += new System.EventHandler(this.cBAngebotskategorie_SelectedIndexChanged);
            // 
            // cmsDgvFiles
            // 
            this.cmsDgvFiles.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmsDgvFiles.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.öffnenToolStripMenuItem,
            this.löschenToolStripMenuItem});
            this.cmsDgvFiles.Name = "cmsDgvFiles";
            this.cmsDgvFiles.Size = new System.Drawing.Size(132, 52);
            // 
            // öffnenToolStripMenuItem
            // 
            this.öffnenToolStripMenuItem.Name = "öffnenToolStripMenuItem";
            this.öffnenToolStripMenuItem.Size = new System.Drawing.Size(131, 24);
            this.öffnenToolStripMenuItem.Text = "Öffnen";
            this.öffnenToolStripMenuItem.Click += new System.EventHandler(this.öffnenToolStripMenuItem_Click);
            // 
            // löschenToolStripMenuItem
            // 
            this.löschenToolStripMenuItem.Name = "löschenToolStripMenuItem";
            this.löschenToolStripMenuItem.Size = new System.Drawing.Size(131, 24);
            this.löschenToolStripMenuItem.Text = "Löschen";
            this.löschenToolStripMenuItem.Click += new System.EventHandler(this.löschenToolStripMenuItem_Click);
            // 
            // tabKontakte
            // 
            this.tabKontakte.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabKontakte.Controls.Add(this.cmdAddFile);
            this.tabKontakte.Controls.Add(this.dgvFiles);
            this.tabKontakte.Controls.Add(this.lblPTyp);
            this.tabKontakte.Controls.Add(this.groupBox1);
            this.tabKontakte.Controls.Add(this.groupBox3);
            this.tabKontakte.Location = new System.Drawing.Point(4, 27);
            this.tabKontakte.Margin = new System.Windows.Forms.Padding(2);
            this.tabKontakte.Name = "tabKontakte";
            this.tabKontakte.Padding = new System.Windows.Forms.Padding(2);
            this.tabKontakte.Size = new System.Drawing.Size(1305, 468);
            this.tabKontakte.TabIndex = 0;
            this.tabKontakte.Text = "Kontakte/Dateien";
            // 
            // cmdAddFile
            // 
            this.cmdAddFile.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdAddFile.Location = new System.Drawing.Point(14, 508);
            this.cmdAddFile.Name = "cmdAddFile";
            this.cmdAddFile.Size = new System.Drawing.Size(166, 32);
            this.cmdAddFile.TabIndex = 66;
            this.cmdAddFile.Text = "Datei anfügen";
            this.cmdAddFile.UseVisualStyleBackColor = true;
            this.cmdAddFile.Click += new System.EventHandler(this.btnData_Click);
            // 
            // dgvFiles
            // 
            this.dgvFiles.AllowUserToAddRows = false;
            this.dgvFiles.AllowUserToDeleteRows = false;
            this.dgvFiles.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvFiles.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFiles.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Column1,
            this.c_Kommentar});
            this.dgvFiles.ContextMenuStrip = this.cmsDgvFiles;
            this.dgvFiles.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dgvFiles.Location = new System.Drawing.Point(17, 252);
            this.dgvFiles.MultiSelect = false;
            this.dgvFiles.Name = "dgvFiles";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFiles.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvFiles.RowTemplate.Height = 31;
            this.dgvFiles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFiles.Size = new System.Drawing.Size(1268, 199);
            this.dgvFiles.TabIndex = 65;
            this.dgvFiles.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellDoubleClick);
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column3.HeaderText = "Dateipfad";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 200;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Dateiname";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 200;
            // 
            // c_Kommentar
            // 
            this.c_Kommentar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.c_Kommentar.HeaderText = "Kommentar";
            this.c_Kommentar.Name = "c_Kommentar";
            this.c_Kommentar.ReadOnly = true;
            // 
            // lblPTyp
            // 
            this.lblPTyp.AutoSize = true;
            this.lblPTyp.Location = new System.Drawing.Point(653, 63);
            this.lblPTyp.Name = "lblPTyp";
            this.lblPTyp.Size = new System.Drawing.Size(0, 18);
            this.lblPTyp.TabIndex = 64;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnUpdatePers);
            this.groupBox1.Controls.Add(this.dgvPERS);
            this.groupBox1.Controls.Add(this.btnNewPers);
            this.groupBox1.Controls.Add(this.btnAddPers);
            this.groupBox1.Location = new System.Drawing.Point(18, 13);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(534, 230);
            this.groupBox1.TabIndex = 63;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Angehörige";
            // 
            // btnUpdatePers
            // 
            this.btnUpdatePers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdatePers.Location = new System.Drawing.Point(299, 28);
            this.btnUpdatePers.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpdatePers.Name = "btnUpdatePers";
            this.btnUpdatePers.Size = new System.Drawing.Size(172, 27);
            this.btnUpdatePers.TabIndex = 3;
            this.btnUpdatePers.Text = "Person bearbeiten";
            this.btnUpdatePers.UseVisualStyleBackColor = true;
            this.btnUpdatePers.Click += new System.EventHandler(this.button1_Click);
            // 
            // dgvPERS
            // 
            this.dgvPERS.AllowUserToAddRows = false;
            this.dgvPERS.AllowUserToDeleteRows = false;
            this.dgvPERS.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvPERS.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvPERS.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPERS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPERS.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.c_pnr,
            this.clmPTyp,
            this.c_vorname,
            this.c_Nachname,
            this.c_Gebd});
            this.dgvPERS.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvPERS.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dgvPERS.Location = new System.Drawing.Point(2, 68);
            this.dgvPERS.Margin = new System.Windows.Forms.Padding(2);
            this.dgvPERS.MultiSelect = false;
            this.dgvPERS.Name = "dgvPERS";
            this.dgvPERS.ReadOnly = true;
            this.dgvPERS.RowTemplate.Height = 24;
            this.dgvPERS.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvPERS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPERS.Size = new System.Drawing.Size(530, 160);
            this.dgvPERS.TabIndex = 2;
            // 
            // c_pnr
            // 
            this.c_pnr.HeaderText = "PNr";
            this.c_pnr.Name = "c_pnr";
            this.c_pnr.ReadOnly = true;
            this.c_pnr.Width = 63;
            // 
            // clmPTyp
            // 
            this.clmPTyp.HeaderText = "PTyp";
            this.clmPTyp.Name = "clmPTyp";
            this.clmPTyp.ReadOnly = true;
            this.clmPTyp.Width = 71;
            // 
            // c_vorname
            // 
            this.c_vorname.HeaderText = "Vorname";
            this.c_vorname.Name = "c_vorname";
            this.c_vorname.ReadOnly = true;
            this.c_vorname.Width = 97;
            // 
            // c_Nachname
            // 
            this.c_Nachname.HeaderText = "Nachname";
            this.c_Nachname.Name = "c_Nachname";
            this.c_Nachname.ReadOnly = true;
            this.c_Nachname.Width = 109;
            // 
            // c_Gebd
            // 
            this.c_Gebd.HeaderText = "Geburtsdatum";
            this.c_Gebd.Name = "c_Gebd";
            this.c_Gebd.ReadOnly = true;
            this.c_Gebd.Width = 131;
            // 
            // btnNewPers
            // 
            this.btnNewPers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewPers.Location = new System.Drawing.Point(166, 28);
            this.btnNewPers.Margin = new System.Windows.Forms.Padding(2);
            this.btnNewPers.Name = "btnNewPers";
            this.btnNewPers.Size = new System.Drawing.Size(129, 27);
            this.btnNewPers.TabIndex = 1;
            this.btnNewPers.Text = "neue Person";
            this.btnNewPers.UseVisualStyleBackColor = true;
            this.btnNewPers.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnAddPers
            // 
            this.btnAddPers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddPers.Location = new System.Drawing.Point(7, 28);
            this.btnAddPers.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddPers.Name = "btnAddPers";
            this.btnAddPers.Size = new System.Drawing.Size(155, 27);
            this.btnAddPers.TabIndex = 0;
            this.btnAddPers.Text = "bestehende Person";
            this.btnAddPers.UseVisualStyleBackColor = true;
            this.btnAddPers.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnUpdateIns);
            this.groupBox3.Controls.Add(this.dgvINST);
            this.groupBox3.Controls.Add(this.btnNewIns);
            this.groupBox3.Controls.Add(this.btnAddIns);
            this.groupBox3.Location = new System.Drawing.Point(596, 15);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(548, 228);
            this.groupBox3.TabIndex = 62;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Institution";
            // 
            // btnUpdateIns
            // 
            this.btnUpdateIns.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdateIns.Location = new System.Drawing.Point(317, 28);
            this.btnUpdateIns.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpdateIns.Name = "btnUpdateIns";
            this.btnUpdateIns.Size = new System.Drawing.Size(170, 27);
            this.btnUpdateIns.TabIndex = 3;
            this.btnUpdateIns.Text = "Institution bearbeiten";
            this.btnUpdateIns.UseVisualStyleBackColor = true;
            this.btnUpdateIns.Click += new System.EventHandler(this.btnUpdateIns_Click);
            // 
            // dgvINST
            // 
            this.dgvINST.AllowUserToAddRows = false;
            this.dgvINST.AllowUserToDeleteRows = false;
            this.dgvINST.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvINST.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvINST.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvINST.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvINST.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.c_INr,
            this.c_Bezeichnung,
            this.c_Kategorie,
            this.C_Ansprechpartner});
            this.dgvINST.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvINST.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dgvINST.Location = new System.Drawing.Point(2, 63);
            this.dgvINST.Margin = new System.Windows.Forms.Padding(2);
            this.dgvINST.MultiSelect = false;
            this.dgvINST.Name = "dgvINST";
            this.dgvINST.ReadOnly = true;
            this.dgvINST.RowTemplate.Height = 24;
            this.dgvINST.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvINST.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvINST.Size = new System.Drawing.Size(544, 163);
            this.dgvINST.TabIndex = 2;
            // 
            // c_INr
            // 
            this.c_INr.HeaderText = "INr";
            this.c_INr.Name = "c_INr";
            this.c_INr.ReadOnly = true;
            this.c_INr.Width = 56;
            // 
            // c_Bezeichnung
            // 
            this.c_Bezeichnung.HeaderText = "Bezeichnung";
            this.c_Bezeichnung.Name = "c_Bezeichnung";
            this.c_Bezeichnung.ReadOnly = true;
            this.c_Bezeichnung.Width = 122;
            // 
            // c_Kategorie
            // 
            this.c_Kategorie.HeaderText = "Kategorie";
            this.c_Kategorie.Name = "c_Kategorie";
            this.c_Kategorie.ReadOnly = true;
            // 
            // C_Ansprechpartner
            // 
            this.C_Ansprechpartner.HeaderText = "Ansprechpartner";
            this.C_Ansprechpartner.Name = "C_Ansprechpartner";
            this.C_Ansprechpartner.ReadOnly = true;
            this.C_Ansprechpartner.Width = 145;
            // 
            // btnNewIns
            // 
            this.btnNewIns.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewIns.Location = new System.Drawing.Point(181, 28);
            this.btnNewIns.Margin = new System.Windows.Forms.Padding(2);
            this.btnNewIns.Name = "btnNewIns";
            this.btnNewIns.Size = new System.Drawing.Size(132, 27);
            this.btnNewIns.TabIndex = 1;
            this.btnNewIns.Text = "neue Institution";
            this.btnNewIns.UseVisualStyleBackColor = true;
            this.btnNewIns.Click += new System.EventHandler(this.button8_Click);
            // 
            // btnAddIns
            // 
            this.btnAddIns.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddIns.Location = new System.Drawing.Point(7, 28);
            this.btnAddIns.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddIns.Name = "btnAddIns";
            this.btnAddIns.Size = new System.Drawing.Size(170, 27);
            this.btnAddIns.TabIndex = 0;
            this.btnAddIns.Text = "bestehende Institution";
            this.btnAddIns.UseVisualStyleBackColor = true;
            this.btnAddIns.Click += new System.EventHandler(this.btnAddIns_Click);
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Datentyp";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 95;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabKontakte);
            this.tabControl1.Controls.Add(this.tabAllgemeines);
            this.tabControl1.Controls.Add(this.tabAnamnese);
            this.tabControl1.HotTrack = true;
            this.tabControl1.Location = new System.Drawing.Point(11, 61);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1313, 499);
            this.tabControl1.TabIndex = 12;
            // 
            // txtAnmerkungen
            // 
            this.txtAnmerkungen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAnmerkungen.editTitle = "Anmerkungen";
            this.txtAnmerkungen.Location = new System.Drawing.Point(19, 723);
            this.txtAnmerkungen.Name = "txtAnmerkungen";
            this.txtAnmerkungen.Size = new System.Drawing.Size(1438, 200);
            this.txtAnmerkungen.TabIndex = 82;
            // 
            // ownTextbox1
            // 
            this.ownTextbox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ownTextbox1.editTitle = "Anmerkungen";
            this.ownTextbox1.Location = new System.Drawing.Point(12, 1884);
            this.ownTextbox1.Name = "ownTextbox1";
            this.ownTextbox1.Size = new System.Drawing.Size(1223, 151);
            this.ownTextbox1.TabIndex = 83;
            // 
            // allgAuftrag
            // 
            this.allgAuftrag.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.allgAuftrag.editTitle = "Auftrag/Fragestellung";
            this.allgAuftrag.Location = new System.Drawing.Point(386, 232);
            this.allgAuftrag.Margin = new System.Windows.Forms.Padding(4, 163, 4, 163);
            this.allgAuftrag.Name = "allgAuftrag";
            this.allgAuftrag.Size = new System.Drawing.Size(408, 177);
            this.allgAuftrag.TabIndex = 91;
            // 
            // allgZiel
            // 
            this.allgZiel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.allgZiel.editTitle = "Ziel";
            this.allgZiel.Location = new System.Drawing.Point(386, 418);
            this.allgZiel.Margin = new System.Windows.Forms.Padding(4, 163, 4, 163);
            this.allgZiel.Name = "allgZiel";
            this.allgZiel.Size = new System.Drawing.Size(408, 173);
            this.allgZiel.TabIndex = 92;
            // 
            // allgMedikation
            // 
            this.allgMedikation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.allgMedikation.editTitle = "Medikation";
            this.allgMedikation.Location = new System.Drawing.Point(805, 232);
            this.allgMedikation.Margin = new System.Windows.Forms.Padding(4, 163, 4, 163);
            this.allgMedikation.Name = "allgMedikation";
            this.allgMedikation.Size = new System.Drawing.Size(411, 177);
            this.allgMedikation.TabIndex = 93;
            // 
            // txtAnmerkungen2
            // 
            this.txtAnmerkungen2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAnmerkungen2.editTitle = "Anmerkungen";
            this.txtAnmerkungen2.Location = new System.Drawing.Point(11, 567);
            this.txtAnmerkungen2.Margin = new System.Windows.Forms.Padding(4, 163, 4, 163);
            this.txtAnmerkungen2.Name = "txtAnmerkungen2";
            this.txtAnmerkungen2.Size = new System.Drawing.Size(1109, 149);
            this.txtAnmerkungen2.TabIndex = 84;
            // 
            // BefKon
            // 
            this.AcceptButton = this.cmdSave;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.AntiqueWhite;
            this.ClientSize = new System.Drawing.Size(1348, 721);
            this.Controls.Add(this.txtAnmerkungen2);
            this.Controls.Add(this.ownTextbox1);
            this.Controls.Add(this.txtAnmerkungen);
            this.Controls.Add(this.lblClient);
            this.Controls.Add(this.lblBefKonNr);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdSave);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblGender);
            this.Controls.Add(this.lblTelefone);
            this.Controls.Add(this.lblAge);
            this.Controls.Add(this.lblBirthday);
            this.Controls.Add(this.lblFirstName);
            this.Controls.Add(this.lblLastName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.nein);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "BefKon";
            this.Text = "neuen Befassungskontext erstellen";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BefKon_FormClosing);
            this.Load += new System.EventHandler(this.new_BefKon_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.new_BefKon_Paint);
            this.tabAnamnese.ResumeLayout(false);
            this.tabAllgemeines.ResumeLayout(false);
            this.tabAllgemeines.PerformLayout();
            this.gBSetting.ResumeLayout(false);
            this.gBSetting.PerformLayout();
            this.cmsDgvFiles.ResumeLayout(false);
            this.tabKontakte.ResumeLayout(false);
            this.tabKontakte.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFiles)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPERS)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvINST)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label nein;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblBirthday;
        private System.Windows.Forms.Label lblAge;
        private System.Windows.Forms.Label lblTelefone;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmdSave;
        private System.Windows.Forms.Label lblClient;
        private System.Windows.Forms.Label lblBefKonNr;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TabPage tabAnamnese;
        private ownTextbox anamUmgang;
        private ownTextbox anamEltern;
        private ownTextbox anamEntw;
        private ownTextbox anamFami;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TabPage tabAllgemeines;
        private System.Windows.Forms.Label lblDiverses;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblFallfuehrender;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblFallfuehrend2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblInterventionskategorie;
        private System.Windows.Forms.ComboBox cBFall;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblBehandlungsanlass;
        private System.Windows.Forms.ComboBox cBFallZwei;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox gBSetting;
        private System.Windows.Forms.CheckBox B_Anzeige;
        private System.Windows.Forms.CheckBox B_Gefaehrdungsmeldung;
        private System.Windows.Forms.CheckBox B_FU;
        private System.Windows.Forms.TextBox txtUeberweiser;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ComboBox cBBehandlungsanlass;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cBInterventionskategorie;
        private System.Windows.Forms.TabPage tabKontakte;
        private System.Windows.Forms.Button cmdAddFile;
        private System.Windows.Forms.DataGridView dgvFiles;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.Label lblPTyp;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnUpdatePers;
        private System.Windows.Forms.DataGridView dgvPERS;
        private System.Windows.Forms.DataGridViewTextBoxColumn c_pnr;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmPTyp;
        private System.Windows.Forms.DataGridViewTextBoxColumn c_vorname;
        private System.Windows.Forms.DataGridViewTextBoxColumn c_Nachname;
        private System.Windows.Forms.DataGridViewTextBoxColumn c_Gebd;
        private System.Windows.Forms.Button btnNewPers;
        private System.Windows.Forms.Button btnAddPers;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnUpdateIns;
        private System.Windows.Forms.DataGridView dgvINST;
        private System.Windows.Forms.DataGridViewTextBoxColumn c_INr;
        private System.Windows.Forms.DataGridViewTextBoxColumn c_Bezeichnung;
        private System.Windows.Forms.DataGridViewTextBoxColumn c_Kategorie;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Ansprechpartner;
        private System.Windows.Forms.Button btnNewIns;
        private System.Windows.Forms.Button btnAddIns;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Label lblUberweiser;
        private System.Windows.Forms.DateTimePicker dtpAnmeldedatum;
        private System.Windows.Forms.DateTimePicker dtpEnde;
        private System.Windows.Forms.DateTimePicker dtpBeginn;
        private ownTextbox allgDiagnose;
        private ownTextbox allgSymptomatik;
        private ownTextbox txtAnmerkungen;
        private System.Windows.Forms.ContextMenuStrip cmsDgvFiles;
        private System.Windows.Forms.ToolStripMenuItem öffnenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem löschenToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn c_Kommentar;
        private ownTextbox ownTextbox1;
        private ownTextbox allgMedikation;
        private ownTextbox allgZiel;
        private ownTextbox allgAuftrag;
        private ownTextbox txtAnmerkungen2;
    }
}