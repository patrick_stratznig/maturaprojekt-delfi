﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Allgemeine Informationen ueber eine Assembly werden ueber die folgenden 
// Attribute gesteuert. aendern Sie diese Attributwerte, um die Informationen zu aendern,
// die einer Assembly zugeordnet sind.
[assembly: AssemblyTitle("Maturaprojekt Vorschau")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Maturaprojekt Vorschau")]
[assembly: AssemblyCopyright("Copyright ©  2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Durch Festlegen von ComVisible auf "false" werden die Typen in dieser Assembly unsichtbar 
// fuer COM-Komponenten.  Wenn Sie auf einen Typ in dieser Assembly von 
// COM aus zugreifen muessen, sollten Sie das ComVisible-Attribut fuer diesen Typ auf "True" festlegen.
[assembly: ComVisible(false)]

// Die folgende GUID bestimmt die ID der Typbibliothek, wenn dieses Projekt fuer COM verfuegbar gemacht wird
[assembly: Guid("024b0b0a-8fc2-4ce5-9aea-b7d09438d304")]

// Versionsinformationen fuer eine Assembly bestehen aus den folgenden vier Werten:
//
//      Hauptversion
//      Nebenversion 
//      Buildnummer
//      Revision
//
// Sie können alle Werte angeben oder die standardmaeßigen Build- und Revisionsnummern 
// uebernehmen, indem Sie "*" eingeben:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
