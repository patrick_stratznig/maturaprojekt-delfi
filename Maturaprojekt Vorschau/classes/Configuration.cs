﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;


namespace Tools
{
    public class Configuration
    {
        private string configurationFile;       // Name der Konfigurationsdatei
        private Dictionary<string, string> werte = new Dictionary<string, string>();

        /// <summary>
        /// Konstruktor
        /// Die Konfigurationsdatei ist mit vollem Pfadnamen anzugeben oder durch einfachen Dateinamen. 
        /// Bei Angabe eines Dateinamens wird im BaseDirectory und den 20 darueberliegenden Verzeichnissen gesucht.
        /// Wird die Datei nicht gefunden, wird ein Fehler gemeldet.
        /// </summary>
        /// <param name="configurationFile">Konfigurationsdatei</param>
        public Configuration(string configurationFile)
        {
            if (!File.Exists(configurationFile))
            {
                int i = 20;
                configurationFile = AppDomain.CurrentDomain.BaseDirectory + "Configuration.xml ";
                while (!File.Exists(configurationFile) && configurationFile.Contains("\\") && i-- > 0)
                {
                    string file = Path.GetFileName(configurationFile);
                    configurationFile = Path.GetDirectoryName(configurationFile);
                    configurationFile = configurationFile.Substring(0, configurationFile.LastIndexOf("\\"));
                    configurationFile = configurationFile + "\\" + file;
                }
            }
            if (!File.Exists(configurationFile))
                throw new Exception("Die Konfigurationsdatei " + configurationFile + " existiert nicht.");
            this.configurationFile = configurationFile;
            // Konfigurationsdatei lesen
            XmlDocument doc = new XmlDocument();
            doc.Load(configurationFile);
            XmlElement docElem = doc.DocumentElement;
            //XmlNode rootNode = docElem.SelectSingleNode("/CONFIGURATION");
            XmlNodeList nodeList = docElem.SelectNodes("/CONFIGURATION");
            XmlNode currentNode = nodeList[0].FirstChild;
            while (currentNode != null)
            {
                werte.Add(currentNode.Name, currentNode.FirstChild.Value);
                currentNode = currentNode.NextSibling;
            }           
        }
        /// <summary>
        /// Abfrage eines Wertes aus der Konfigurationsdatei als String
        /// </summary>
        /// <param name="key">Schluessel des Wertes</param>
        /// <returns>Konfigurationswert (Datentyp String)</returns>
        public string getString(string key)
        {
            return werte[key];
        }
        /// <summary>
        /// Abfrage eines Wertes aus der Konfigurationsdatei als Integer
        /// </summary>
        /// <param name="key">Schluessel des Wertes</param>
        /// <returns>Konfigurationswert (Datentyp Integer)</returns>
        public int getInt(string key)
        {
            return Convert.ToInt32(werte[key]);
        }
        /// <summary>
        /// Veraendern eines Wertes in der Konfigurationsdatei
        /// </summary>
        /// <param name="key">Schluessel</param>
        /// <param name="value">Neuer Wert</param>
        public void setString(string key, string value)
        {
            werte[key] = value;
        }
        /// <summary>
        /// Veraendern eines Integerwertes in der Konfigurationsdatei
        /// </summary>
        /// <param name="key">Schluessel</param>
        /// <param name="value">Neuer Wert</param>
        public void setInt(string key, int value)
        {
            werte[key] = value.ToString();
        }
        public void saveConfig()
        {
            XmlDocument config = new XmlDocument();
            config.Load(configurationFile);
            XmlElement configElem = config.DocumentElement;
            XmlNode nodeConfiguration = configElem.SelectSingleNode("/CONFIGURATION");
            for (int i = 0; i < werte.Count; i++)
                nodeConfiguration[werte.Keys.ElementAt<string>(i)].FirstChild.Value=werte.Values.ElementAt<string>(i);                                
            config.Save(configurationFile);
        }
    }
}
