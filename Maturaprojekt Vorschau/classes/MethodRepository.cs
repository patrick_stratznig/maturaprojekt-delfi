﻿using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Klientenverwaltung.Properties;

namespace Maturaprojekt_Vorschau
{
    class MethodRepository
    {
        #region Variablen

        private SqlConnection _con;
      
        private SqlDataAdapter _adaptClient;
        private SqlDataAdapter _adaptTermin;
        private SqlDataAdapter _adaptBefKon;
        private SqlDataAdapter _adaptMa;
        private SqlDataAdapter _adaptInst;
        private SqlDataAdapter _adaptEvent;
        private SqlDataAdapter _adaptPerson;
        private SqlDataAdapter _adaptProzess;
        private SqlDataAdapter _adaptTelefon;
        private SqlDataAdapter _adaptIntervision;

        private SqlDataAdapter _adaptSonstige;


        public SqlDataAdapter AdaptEvent => _adaptEvent;
        public SqlDataAdapter AdaptClient => _adaptClient;

        public SqlDataAdapter AuswahlBefKon => _adaptBefKon;

        public SqlDataAdapter AdaptTermin => _adaptTermin;
        public SqlDataAdapter AdaptBefKon => _adaptBefKon;
        public SqlDataAdapter AdaptMa => _adaptMa;
        public SqlDataAdapter AdaptInst => _adaptInst;

        public SqlDataAdapter GetAdaptPerson(string PNr, string PTyp)
        {
            #region adaptPerson
            _adaptPerson = new SqlDataAdapter("Select * from Person where PNr = @PNr and PTyp = @PTyp", _con);
            _adaptPerson.SelectCommand.Parameters.Add("@PNr", SqlDbType.VarChar, 8).Value = PNr;
            _adaptPerson.SelectCommand.Parameters.Add("@PTyp", SqlDbType.VarChar, 4).Value = PTyp;

            _adaptPerson.UpdateCommand = new SqlCommand("Update Person set Nachname = @nn, Vorname = @vn, Straße = @str, Hausnummer = @hn, PLZ = @plz, Ort = @ort, Landcode = @lcd, TelNr = @tel, Geburtsdatum = @gebdat, Religion = @rel, Obsorge = @obs, Beruf = @brf, Familienstand = @famstnd, Geschlecht = @geschl, Staatsbuergerschaft = @staat, Anmerkungen = @anm where PNr = @pnr and PTyp = @ptyp", _con);
            _adaptPerson.UpdateCommand.Parameters.Add("@nn", SqlDbType.VarChar, 255, "Nachname");
            _adaptPerson.UpdateCommand.Parameters.Add("@ptyp", SqlDbType.VarChar, 4, "PTyp");
            _adaptPerson.UpdateCommand.Parameters.Add("@vn", SqlDbType.VarChar, 255, "Vorname");
            _adaptPerson.UpdateCommand.Parameters.Add("@str", SqlDbType.VarChar, 255, "Straße");
            _adaptPerson.UpdateCommand.Parameters.Add("@hn", SqlDbType.VarChar, 10, "Hausnummer");
            _adaptPerson.UpdateCommand.Parameters.Add("@plz", SqlDbType.VarChar, 10, "PLZ");
            _adaptPerson.UpdateCommand.Parameters.Add("@ort", SqlDbType.VarChar, 255, "Ort");
            _adaptPerson.UpdateCommand.Parameters.Add("@lcd", SqlDbType.VarChar, 2, "Landcode");
            _adaptPerson.UpdateCommand.Parameters.Add("@tel", SqlDbType.VarChar, 20, "TelNr");
            _adaptPerson.UpdateCommand.Parameters.Add("@gebdat", SqlDbType.Date, 9, "Geburtsdatum");
            _adaptPerson.UpdateCommand.Parameters.Add("@rel", SqlDbType.VarChar, 255, "Religion");
            _adaptPerson.UpdateCommand.Parameters.Add("@obs", SqlDbType.VarChar, 255, "Obsorge");
            _adaptPerson.UpdateCommand.Parameters.Add("@brf", SqlDbType.VarChar, 255, "Beruf");
            _adaptPerson.UpdateCommand.Parameters.Add("@famstnd", SqlDbType.VarChar, 255, "Familienstand");
            _adaptPerson.UpdateCommand.Parameters.Add("@geschl", SqlDbType.VarChar, 8, "Geschlecht");
            _adaptPerson.UpdateCommand.Parameters.Add("@staat", SqlDbType.VarChar, 255, "Staatsbuergerschaft");
            _adaptPerson.UpdateCommand.Parameters.Add("@anm", SqlDbType.NVarChar, 50000, "Anmerkungen");
            _adaptPerson.UpdateCommand.Parameters.Add("@pnr", SqlDbType.VarChar, 8, "PNr");

            _adaptPerson.InsertCommand = new SqlCommand("Insert into Person values(@pnr, @ptyp, @nn, @vn, @str, @hn, @plz, @ort, @lcd, @tel, @gebdat, @rel, @obs, @brf,  @famstnd, @geschl, @staat, @anm )", _con);
            _adaptPerson.InsertCommand.Parameters.Add("@pnr", SqlDbType.VarChar, 8, "PNr");
            _adaptPerson.InsertCommand.Parameters.Add("@ptyp", SqlDbType.VarChar, 4, "PTyp");
            _adaptPerson.InsertCommand.Parameters.Add("@nn", SqlDbType.VarChar, 255, "Nachname");
            _adaptPerson.InsertCommand.Parameters.Add("@vn", SqlDbType.VarChar, 255, "Vorname");
            _adaptPerson.InsertCommand.Parameters.Add("@str", SqlDbType.VarChar, 255, "Straße");
            _adaptPerson.InsertCommand.Parameters.Add("@hn", SqlDbType.VarChar, 10, "Hausnummer");
            _adaptPerson.InsertCommand.Parameters.Add("@plz", SqlDbType.VarChar, 10, "PLZ");
            _adaptPerson.InsertCommand.Parameters.Add("@ort", SqlDbType.VarChar, 255, "Ort");
            _adaptPerson.InsertCommand.Parameters.Add("@lcd", SqlDbType.VarChar, 2, "Landcode");
            _adaptPerson.InsertCommand.Parameters.Add("@tel", SqlDbType.VarChar, 20, "TelNr");
            _adaptPerson.InsertCommand.Parameters.Add("@gebdat", SqlDbType.Date, 9, "Geburtsdatum");
            _adaptPerson.InsertCommand.Parameters.Add("@rel", SqlDbType.VarChar, 255, "Religion");
            _adaptPerson.InsertCommand.Parameters.Add("@obs", SqlDbType.VarChar, 255, "Obsorge");
            _adaptPerson.InsertCommand.Parameters.Add("@brf", SqlDbType.VarChar, 255, "Beruf");
            _adaptPerson.InsertCommand.Parameters.Add("@famstnd", SqlDbType.VarChar, 255, "Familienstand");
            _adaptPerson.InsertCommand.Parameters.Add("@geschl", SqlDbType.VarChar, 8, "Geschlecht");
            _adaptPerson.InsertCommand.Parameters.Add("@staat", SqlDbType.VarChar, 255, "Staatsbuergerschaft");
            _adaptPerson.InsertCommand.Parameters.Add("@anm", SqlDbType.NVarChar, 50000, "Anmerkungen");
            #endregion

            return _adaptPerson;
        }

        public SqlDataAdapter AdaptProzess => _adaptProzess;

        public SqlDataAdapter AdpaptTelefon => _adaptTelefon;

        public SqlDataAdapter AdaptIntervision => _adaptIntervision;

        public SqlDataAdapter AdaptSonstige => _adaptSonstige;

        #endregion
        

        public MethodRepository(SqlConnection con)
        {
            _con = con;
            Initiieren(false);
        }

        public MethodRepository(SqlConnection con, bool geloescht)
        {
            _con = con;
            Initiieren(true);
        }

        public void Initiieren(bool geloescht)
        {
            #region adpatClient
            _adaptClient = new SqlDataAdapter("Select * from Person where PTyp = 'IK'", _con);          //adaptClient ist fuer die KlPtypienten zustaendig, er nimmt alle wo PTyp = IK

            _adaptClient.UpdateCommand = new SqlCommand("Update Person set Nachname = @nn, Vorname = @vn, Straße = @str, Hausnummer = @hn, PLZ = @plz, Ort = @ort, Landcode = @lcd, TelNr = @tel, Geburtsdatum = @gebdat, Religion = @rel, Obsorge = @obs, Beruf = @brf, Familienstand = @famstnd, Geschlecht = @geschl, Staatsbuergerschaft = @staat, Anmerkungen = @anm where PNr = @pnr and PTyp = @ptyp", _con);
            _adaptClient.UpdateCommand.Parameters.Add("@nn", SqlDbType.VarChar, 255, "Nachname");
            _adaptClient.UpdateCommand.Parameters.Add("@ptyp", SqlDbType.VarChar, 4, "PTyp").Value = "IK";
            _adaptClient.UpdateCommand.Parameters.Add("@vn", SqlDbType.VarChar, 255, "Vorname");
            _adaptClient.UpdateCommand.Parameters.Add("@str", SqlDbType.VarChar, 255, "Straße");
            _adaptClient.UpdateCommand.Parameters.Add("@hn", SqlDbType.VarChar, 10, "Hausnummer");
            _adaptClient.UpdateCommand.Parameters.Add("@plz", SqlDbType.VarChar, 10, "PLZ");
            _adaptClient.UpdateCommand.Parameters.Add("@ort", SqlDbType.VarChar, 255, "Ort");
            _adaptClient.UpdateCommand.Parameters.Add("@lcd", SqlDbType.VarChar, 2, "Landcode");
            _adaptClient.UpdateCommand.Parameters.Add("@tel", SqlDbType.VarChar, 20, "TelNr");
            _adaptClient.UpdateCommand.Parameters.Add("@gebdat", SqlDbType.Date, 9, "Geburtsdatum");
            _adaptClient.UpdateCommand.Parameters.Add("@rel", SqlDbType.VarChar, 255, "Religion");
            _adaptClient.UpdateCommand.Parameters.Add("@obs", SqlDbType.VarChar, 255, "Obsorge");
            _adaptClient.UpdateCommand.Parameters.Add("@brf", SqlDbType.VarChar, 255, "Beruf");
            _adaptClient.UpdateCommand.Parameters.Add("@famstnd", SqlDbType.VarChar, 255, "Familienstand");
            _adaptClient.UpdateCommand.Parameters.Add("@geschl", SqlDbType.VarChar, 8, "Geschlecht");
            _adaptClient.UpdateCommand.Parameters.Add("@staat", SqlDbType.VarChar, 255, "Staatsbuergerschaft");
            _adaptClient.UpdateCommand.Parameters.Add("@anm", SqlDbType.NVarChar, 50000, "Anmerkungen");
            _adaptClient.UpdateCommand.Parameters.Add("@pnr", SqlDbType.VarChar, 8, "PNr");

            _adaptClient.InsertCommand = new SqlCommand("Insert into Person values(@pnr, @ptyp, @nn, @vn, @str, @hn, @plz, @ort, @lcd, @tel, @gebdat, @rel, @obs, @brf,  @famstnd, @geschl, @staat, @anm )", _con);
            _adaptClient.InsertCommand.Parameters.Add("@pnr", SqlDbType.VarChar, 8, "PNr");
            _adaptClient.InsertCommand.Parameters.Add("@ptyp", SqlDbType.VarChar, 4, "PTyp").Value = "IK";
            _adaptClient.InsertCommand.Parameters.Add("@nn", SqlDbType.VarChar, 255, "Nachname");
            _adaptClient.InsertCommand.Parameters.Add("@vn", SqlDbType.VarChar, 255, "Vorname");
            _adaptClient.InsertCommand.Parameters.Add("@str", SqlDbType.VarChar, 255, "Straße");
            _adaptClient.InsertCommand.Parameters.Add("@hn", SqlDbType.VarChar, 10, "Hausnummer");
            _adaptClient.InsertCommand.Parameters.Add("@plz", SqlDbType.VarChar, 10, "PLZ");
            _adaptClient.InsertCommand.Parameters.Add("@ort", SqlDbType.VarChar, 255, "Ort");
            _adaptClient.InsertCommand.Parameters.Add("@lcd", SqlDbType.VarChar, 2, "Landcode");
            _adaptClient.InsertCommand.Parameters.Add("@tel", SqlDbType.VarChar, 20, "TelNr");
            _adaptClient.InsertCommand.Parameters.Add("@gebdat", SqlDbType.Date, 9, "Geburtsdatum");
            _adaptClient.InsertCommand.Parameters.Add("@rel", SqlDbType.VarChar, 255, "Religion");
            _adaptClient.InsertCommand.Parameters.Add("@obs", SqlDbType.VarChar, 255, "Obsorge");
            _adaptClient.InsertCommand.Parameters.Add("@brf", SqlDbType.VarChar, 255, "Beruf");
            _adaptClient.InsertCommand.Parameters.Add("@famstnd", SqlDbType.VarChar, 255, "Familienstand");
            _adaptClient.InsertCommand.Parameters.Add("@geschl", SqlDbType.VarChar, 8, "Geschlecht");
            _adaptClient.InsertCommand.Parameters.Add("@staat", SqlDbType.VarChar, 255, "Staatsbuergerschaft");
            _adaptClient.InsertCommand.Parameters.Add("@anm", SqlDbType.NVarChar, 50000, "Anmerkungen");
            #endregion

            #region Prozessbegleitung

            _adaptProzess = new SqlDataAdapter("Select * from Person where PTyp = 'Pb'", _con);          //adaptClient ist fuer die KlPtypienten zustaendig, er nimmt alle wo PTyp = IK

            _adaptProzess.UpdateCommand = new SqlCommand("Update Person set Nachname = @nn, Vorname = @vn, Straße = @str, Hausnummer = @hn, PLZ = @plz, Ort = @ort, Landcode = @lcd, TelNr = @tel, Geburtsdatum = @gebdat, Religion = @rel, Obsorge = @obs, Beruf = @brf, Familienstand = @famstnd, Geschlecht = @geschl, Staatsbuergerschaft = @staat, Anmerkungen = @anm where PNr = @pnr and PTyp = @ptyp", _con);
            _adaptProzess.UpdateCommand.Parameters.Add("@nn", SqlDbType.VarChar, 255, "Nachname");
            _adaptProzess.UpdateCommand.Parameters.Add("@ptyp", SqlDbType.VarChar, 4, "PTyp").Value = "Pb";
            _adaptProzess.UpdateCommand.Parameters.Add("@vn", SqlDbType.VarChar, 255, "Vorname");
            _adaptProzess.UpdateCommand.Parameters.Add("@str", SqlDbType.VarChar, 255, "Straße");
            _adaptProzess.UpdateCommand.Parameters.Add("@hn", SqlDbType.VarChar, 10, "Hausnummer");
            _adaptProzess.UpdateCommand.Parameters.Add("@plz", SqlDbType.VarChar, 10, "PLZ");
            _adaptProzess.UpdateCommand.Parameters.Add("@ort", SqlDbType.VarChar, 255, "Ort");
            _adaptProzess.UpdateCommand.Parameters.Add("@lcd", SqlDbType.VarChar, 2, "Landcode");
            _adaptProzess.UpdateCommand.Parameters.Add("@tel", SqlDbType.VarChar, 20, "TelNr");
            _adaptProzess.UpdateCommand.Parameters.Add("@gebdat", SqlDbType.Date, 9, "Geburtsdatum");
            _adaptProzess.UpdateCommand.Parameters.Add("@rel", SqlDbType.VarChar, 255, "Religion");
            _adaptProzess.UpdateCommand.Parameters.Add("@obs", SqlDbType.VarChar, 255, "Obsorge");
            _adaptProzess.UpdateCommand.Parameters.Add("@brf", SqlDbType.VarChar, 255, "Beruf");
            _adaptProzess.UpdateCommand.Parameters.Add("@famstnd", SqlDbType.VarChar, 255, "Familienstand");
            _adaptProzess.UpdateCommand.Parameters.Add("@geschl", SqlDbType.VarChar, 8, "Geschlecht");
            _adaptProzess.UpdateCommand.Parameters.Add("@staat", SqlDbType.VarChar, 255, "Staatsbuergerschaft");
            _adaptProzess.UpdateCommand.Parameters.Add("@anm", SqlDbType.NVarChar, 50000, "Anmerkungen");
            _adaptProzess.UpdateCommand.Parameters.Add("@pnr", SqlDbType.VarChar, 8, "PNr");

            _adaptProzess.InsertCommand = new SqlCommand("Insert into Person values(@pnr, @ptyp, @nn, @vn, @str, @hn, @plz, @ort, @lcd, @tel, @gebdat, @rel, @obs, @brf,  @famstnd, @geschl, @staat, @anm )", _con);
            _adaptProzess.InsertCommand.Parameters.Add("@pnr", SqlDbType.VarChar, 8, "PNr");
            _adaptProzess.InsertCommand.Parameters.Add("@ptyp", SqlDbType.VarChar, 4, "PTyp").Value = "Pb";
            _adaptProzess.InsertCommand.Parameters.Add("@nn", SqlDbType.VarChar, 255, "Nachname");
            _adaptProzess.InsertCommand.Parameters.Add("@vn", SqlDbType.VarChar, 255, "Vorname");
            _adaptProzess.InsertCommand.Parameters.Add("@str", SqlDbType.VarChar, 255, "Straße");
            _adaptProzess.InsertCommand.Parameters.Add("@hn", SqlDbType.VarChar, 10, "Hausnummer");
            _adaptProzess.InsertCommand.Parameters.Add("@plz", SqlDbType.VarChar, 10, "PLZ");
            _adaptProzess.InsertCommand.Parameters.Add("@ort", SqlDbType.VarChar, 255, "Ort");
            _adaptProzess.InsertCommand.Parameters.Add("@lcd", SqlDbType.VarChar, 2, "Landcode");
            _adaptProzess.InsertCommand.Parameters.Add("@tel", SqlDbType.VarChar, 20, "TelNr");
            _adaptProzess.InsertCommand.Parameters.Add("@gebdat", SqlDbType.Date, 9, "Geburtsdatum");
            _adaptProzess.InsertCommand.Parameters.Add("@rel", SqlDbType.VarChar, 255, "Religion");
            _adaptProzess.InsertCommand.Parameters.Add("@obs", SqlDbType.VarChar, 255, "Obsorge");
            _adaptProzess.InsertCommand.Parameters.Add("@brf", SqlDbType.VarChar, 255, "Beruf");
            _adaptProzess.InsertCommand.Parameters.Add("@famstnd", SqlDbType.VarChar, 255, "Familienstand");
            _adaptProzess.InsertCommand.Parameters.Add("@geschl", SqlDbType.VarChar, 8, "Geschlecht");
            _adaptProzess.InsertCommand.Parameters.Add("@staat", SqlDbType.VarChar, 255, "Staatsbuergerschaft");
            _adaptProzess.InsertCommand.Parameters.Add("@anm", SqlDbType.NVarChar, 50000, "Anmerkungen");


            #endregion

            #region telefonische Befassung

            _adaptTelefon = new SqlDataAdapter("Select * from Person where PTyp = 'TB'", _con);

            _adaptTelefon.UpdateCommand = new SqlCommand("Update Person set Nachname = @nn, Vorname = @vn, Straße = @str, Hausnummer = @hn, PLZ = @plz, Ort = @ort, Landcode = @lcd, TelNr = @tel, Geburtsdatum = @gebdat, Religion = @rel, Obsorge = @obs, Beruf = @brf, Familienstand = @famstnd, Geschlecht = @geschl, Staatsbuergerschaft = @staat, Anmerkungen = @anm where PNr = @pnr and PTyp = @ptyp", _con);
            _adaptTelefon.UpdateCommand.Parameters.Add("@nn", SqlDbType.VarChar, 255, "Nachname");
            _adaptTelefon.UpdateCommand.Parameters.Add("@ptyp", SqlDbType.VarChar, 4, "PTyp").Value = "TB";
            _adaptTelefon.UpdateCommand.Parameters.Add("@vn", SqlDbType.VarChar, 255, "Vorname");
            _adaptTelefon.UpdateCommand.Parameters.Add("@str", SqlDbType.VarChar, 255, "Straße");
            _adaptTelefon.UpdateCommand.Parameters.Add("@hn", SqlDbType.VarChar, 10, "Hausnummer");
            _adaptTelefon.UpdateCommand.Parameters.Add("@plz", SqlDbType.VarChar, 10, "PLZ");
            _adaptTelefon.UpdateCommand.Parameters.Add("@ort", SqlDbType.VarChar, 255, "Ort");
            _adaptTelefon.UpdateCommand.Parameters.Add("@lcd", SqlDbType.VarChar, 2, "Landcode");
            _adaptTelefon.UpdateCommand.Parameters.Add("@tel", SqlDbType.VarChar, 20, "TelNr");
            _adaptTelefon.UpdateCommand.Parameters.Add("@gebdat", SqlDbType.Date, 9, "Geburtsdatum");
            _adaptTelefon.UpdateCommand.Parameters.Add("@rel", SqlDbType.VarChar, 255, "Religion");
            _adaptTelefon.UpdateCommand.Parameters.Add("@obs", SqlDbType.VarChar, 255, "Obsorge");
            _adaptTelefon.UpdateCommand.Parameters.Add("@brf", SqlDbType.VarChar, 255, "Beruf");
            _adaptTelefon.UpdateCommand.Parameters.Add("@famstnd", SqlDbType.VarChar, 255, "Familienstand");
            _adaptTelefon.UpdateCommand.Parameters.Add("@geschl", SqlDbType.VarChar, 8, "Geschlecht");
            _adaptTelefon.UpdateCommand.Parameters.Add("@staat", SqlDbType.VarChar, 255, "Staatsbuergerschaft");
            _adaptTelefon.UpdateCommand.Parameters.Add("@anm", SqlDbType.NVarChar, 50000, "Anmerkungen");
            _adaptTelefon.UpdateCommand.Parameters.Add("@pnr", SqlDbType.VarChar, 8, "PNr");

            _adaptTelefon.InsertCommand = new SqlCommand("Insert into Person values(@pnr, @ptyp, @nn, @vn, @str, @hn, @plz, @ort, @lcd, @tel, @gebdat, @rel, @obs, @brf,  @famstnd, @geschl, @staat, @anm )", _con);
            _adaptTelefon.InsertCommand.Parameters.Add("@pnr", SqlDbType.VarChar, 8, "PNr");
            _adaptTelefon.InsertCommand.Parameters.Add("@ptyp", SqlDbType.VarChar, 4, "PTyp").Value = "TB";
            _adaptTelefon.InsertCommand.Parameters.Add("@nn", SqlDbType.VarChar, 255, "Nachname");
            _adaptTelefon.InsertCommand.Parameters.Add("@vn", SqlDbType.VarChar, 255, "Vorname");
            _adaptTelefon.InsertCommand.Parameters.Add("@str", SqlDbType.VarChar, 255, "Straße");
            _adaptTelefon.InsertCommand.Parameters.Add("@hn", SqlDbType.VarChar, 10, "Hausnummer");
            _adaptTelefon.InsertCommand.Parameters.Add("@plz", SqlDbType.VarChar, 10, "PLZ");
            _adaptTelefon.InsertCommand.Parameters.Add("@ort", SqlDbType.VarChar, 255, "Ort");
            _adaptTelefon.InsertCommand.Parameters.Add("@lcd", SqlDbType.VarChar, 2, "Landcode");
            _adaptTelefon.InsertCommand.Parameters.Add("@tel", SqlDbType.VarChar, 20, "TelNr");
            _adaptTelefon.InsertCommand.Parameters.Add("@gebdat", SqlDbType.Date, 9, "Geburtsdatum");
            _adaptTelefon.InsertCommand.Parameters.Add("@rel", SqlDbType.VarChar, 255, "Religion");
            _adaptTelefon.InsertCommand.Parameters.Add("@obs", SqlDbType.VarChar, 255, "Obsorge");
            _adaptTelefon.InsertCommand.Parameters.Add("@brf", SqlDbType.VarChar, 255, "Beruf");
            _adaptTelefon.InsertCommand.Parameters.Add("@famstnd", SqlDbType.VarChar, 255, "Familienstand");
            _adaptTelefon.InsertCommand.Parameters.Add("@geschl", SqlDbType.VarChar, 8, "Geschlecht");
            _adaptTelefon.InsertCommand.Parameters.Add("@staat", SqlDbType.VarChar, 255, "Staatsbuergerschaft");
            _adaptTelefon.InsertCommand.Parameters.Add("@anm", SqlDbType.NVarChar, 50000, "Anmerkungen");


            #endregion

            #region Intervision mit Fachwissen

            _adaptIntervision = new SqlDataAdapter("Select * from Person where PTyp = 'ImF'", _con);

            _adaptIntervision.UpdateCommand = new SqlCommand("Update Person set Nachname = @nn, Vorname = @vn, Straße = @str, Hausnummer = @hn, PLZ = @plz, Ort = @ort, Landcode = @lcd, TelNr = @tel, Geburtsdatum = @gebdat, Religion = @rel, Obsorge = @obs, Beruf = @brf, Familienstand = @famstnd, Geschlecht = @geschl, Staatsbuergerschaft = @staat, Anmerkungen = @anm where PNr = @pnr and PTyp = @ptyp", _con);
            _adaptIntervision.UpdateCommand.Parameters.Add("@nn", SqlDbType.VarChar, 255, "Nachname");
            _adaptIntervision.UpdateCommand.Parameters.Add("@ptyp", SqlDbType.VarChar, 4).Value = "ImF";
            _adaptIntervision.UpdateCommand.Parameters.Add("@vn", SqlDbType.VarChar, 255, "Vorname");
            _adaptIntervision.UpdateCommand.Parameters.Add("@str", SqlDbType.VarChar, 255, "Straße");
            _adaptIntervision.UpdateCommand.Parameters.Add("@hn", SqlDbType.VarChar, 10, "Hausnummer");
            _adaptIntervision.UpdateCommand.Parameters.Add("@plz", SqlDbType.VarChar, 10, "PLZ");
            _adaptIntervision.UpdateCommand.Parameters.Add("@ort", SqlDbType.VarChar, 255, "Ort");
            _adaptIntervision.UpdateCommand.Parameters.Add("@lcd", SqlDbType.VarChar, 2, "Landcode");
            _adaptIntervision.UpdateCommand.Parameters.Add("@tel", SqlDbType.VarChar, 20, "TelNr");
            _adaptIntervision.UpdateCommand.Parameters.Add("@gebdat", SqlDbType.Date, 9, "Geburtsdatum");
            _adaptIntervision.UpdateCommand.Parameters.Add("@rel", SqlDbType.VarChar, 255, "Religion");
            _adaptIntervision.UpdateCommand.Parameters.Add("@obs", SqlDbType.VarChar, 255, "Obsorge");
            _adaptIntervision.UpdateCommand.Parameters.Add("@brf", SqlDbType.VarChar, 255, "Beruf");
            _adaptIntervision.UpdateCommand.Parameters.Add("@famstnd", SqlDbType.VarChar, 255, "Familienstand");
            _adaptIntervision.UpdateCommand.Parameters.Add("@geschl", SqlDbType.VarChar, 8, "Geschlecht");
            _adaptIntervision.UpdateCommand.Parameters.Add("@staat", SqlDbType.VarChar, 255, "Staatsbuergerschaft");
            _adaptIntervision.UpdateCommand.Parameters.Add("@anm", SqlDbType.NVarChar, 50000, "Anmerkungen");
            _adaptIntervision.UpdateCommand.Parameters.Add("@pnr", SqlDbType.VarChar, 8, "PNr");

            _adaptIntervision.InsertCommand = new SqlCommand("Insert into Person values(@pnr, @ptyp, @nn, @vn, @str, @hn, @plz, @ort, @lcd, @tel, @gebdat, @rel, @obs, @brf,  @famstnd, @geschl, @staat, @anm )", _con);
            _adaptIntervision.InsertCommand.Parameters.Add("@pnr", SqlDbType.VarChar, 8, "PNr");
            _adaptIntervision.InsertCommand.Parameters.Add("@ptyp", SqlDbType.VarChar, 4).Value = "ImF";
            _adaptIntervision.InsertCommand.Parameters.Add("@nn", SqlDbType.VarChar, 255, "Nachname");
            _adaptIntervision.InsertCommand.Parameters.Add("@vn", SqlDbType.VarChar, 255, "Vorname");
            _adaptIntervision.InsertCommand.Parameters.Add("@str", SqlDbType.VarChar, 255, "Straße");
            _adaptIntervision.InsertCommand.Parameters.Add("@hn", SqlDbType.VarChar, 10, "Hausnummer");
            _adaptIntervision.InsertCommand.Parameters.Add("@plz", SqlDbType.VarChar, 10, "PLZ");
            _adaptIntervision.InsertCommand.Parameters.Add("@ort", SqlDbType.VarChar, 255, "Ort");
            _adaptIntervision.InsertCommand.Parameters.Add("@lcd", SqlDbType.VarChar, 2, "Landcode");
            _adaptIntervision.InsertCommand.Parameters.Add("@tel", SqlDbType.VarChar, 20, "TelNr");
            _adaptIntervision.InsertCommand.Parameters.Add("@gebdat", SqlDbType.Date, 9, "Geburtsdatum");
            _adaptIntervision.InsertCommand.Parameters.Add("@rel", SqlDbType.VarChar, 255, "Religion");
            _adaptIntervision.InsertCommand.Parameters.Add("@obs", SqlDbType.VarChar, 255, "Obsorge");
            _adaptIntervision.InsertCommand.Parameters.Add("@brf", SqlDbType.VarChar, 255, "Beruf");
            _adaptIntervision.InsertCommand.Parameters.Add("@famstnd", SqlDbType.VarChar, 255, "Familienstand");
            _adaptIntervision.InsertCommand.Parameters.Add("@geschl", SqlDbType.VarChar, 8, "Geschlecht");
            _adaptIntervision.InsertCommand.Parameters.Add("@staat", SqlDbType.VarChar, 255, "Staatsbuergerschaft");
            _adaptIntervision.InsertCommand.Parameters.Add("@anm", SqlDbType.NVarChar, 50000, "Anmerkungen");


            #endregion

            #region Sonstige Befassung

            _adaptSonstige = new SqlDataAdapter("Select * from Person where PTyp = 'SB'", _con);

            _adaptSonstige.UpdateCommand = new SqlCommand("Update Person set Nachname = @nn, Vorname = @vn, Straße = @str, Hausnummer = @hn, PLZ = @plz, Ort = @ort, Landcode = @lcd, TelNr = @tel, Geburtsdatum = @gebdat, Religion = @rel, Obsorge = @obs, Beruf = @brf, Familienstand = @famstnd, Geschlecht = @geschl, Staatsbuergerschaft = @staat, Anmerkungen = @anm where PNr = @pnr and PTyp = @ptyp", _con);
            _adaptSonstige.UpdateCommand.Parameters.Add("@nn", SqlDbType.VarChar, 255, "Nachname");
            _adaptSonstige.UpdateCommand.Parameters.Add("@ptyp", SqlDbType.VarChar, 4).Value = "SB";
            _adaptSonstige.UpdateCommand.Parameters.Add("@vn", SqlDbType.VarChar, 255, "Vorname");
            _adaptSonstige.UpdateCommand.Parameters.Add("@str", SqlDbType.VarChar, 255, "Straße");
            _adaptSonstige.UpdateCommand.Parameters.Add("@hn", SqlDbType.VarChar, 10, "Hausnummer");
            _adaptSonstige.UpdateCommand.Parameters.Add("@plz", SqlDbType.VarChar, 10, "PLZ");
            _adaptSonstige.UpdateCommand.Parameters.Add("@ort", SqlDbType.VarChar, 255, "Ort");
            _adaptSonstige.UpdateCommand.Parameters.Add("@lcd", SqlDbType.VarChar, 2, "Landcode");
            _adaptSonstige.UpdateCommand.Parameters.Add("@tel", SqlDbType.VarChar, 20, "TelNr");
            _adaptSonstige.UpdateCommand.Parameters.Add("@gebdat", SqlDbType.Date, 9, "Geburtsdatum");
            _adaptSonstige.UpdateCommand.Parameters.Add("@rel", SqlDbType.VarChar, 255, "Religion");
            _adaptSonstige.UpdateCommand.Parameters.Add("@obs", SqlDbType.VarChar, 255, "Obsorge");
            _adaptSonstige.UpdateCommand.Parameters.Add("@brf", SqlDbType.VarChar, 255, "Beruf");
            _adaptSonstige.UpdateCommand.Parameters.Add("@famstnd", SqlDbType.VarChar, 255, "Familienstand");
            _adaptSonstige.UpdateCommand.Parameters.Add("@geschl", SqlDbType.VarChar, 8, "Geschlecht");
            _adaptSonstige.UpdateCommand.Parameters.Add("@staat", SqlDbType.VarChar, 255, "Staatsbuergerschaft");
            _adaptSonstige.UpdateCommand.Parameters.Add("@anm", SqlDbType.NVarChar, 50000, "Anmerkungen");
            _adaptSonstige.UpdateCommand.Parameters.Add("@pnr", SqlDbType.VarChar, 8, "PNr");

            _adaptSonstige.InsertCommand = new SqlCommand("Insert into Person values(@pnr, @ptyp, @nn, @vn, @str, @hn, @plz, @ort,@lcd, @tel, @gebdat, @rel, @obs, @brf,  @famstnd, @geschl, @staat, @anm )", _con);
            _adaptSonstige.InsertCommand.Parameters.Add("@pnr", SqlDbType.VarChar, 8, "PNr");
            _adaptSonstige.InsertCommand.Parameters.Add("@ptyp", SqlDbType.VarChar, 4).Value = "SB";
            _adaptSonstige.InsertCommand.Parameters.Add("@nn", SqlDbType.VarChar, 255, "Nachname");
            _adaptSonstige.InsertCommand.Parameters.Add("@vn", SqlDbType.VarChar, 255, "Vorname");
            _adaptSonstige.InsertCommand.Parameters.Add("@str", SqlDbType.VarChar, 255, "Straße");
            _adaptSonstige.InsertCommand.Parameters.Add("@hn", SqlDbType.VarChar, 10, "Hausnummer");
            _adaptSonstige.InsertCommand.Parameters.Add("@plz", SqlDbType.VarChar, 10, "PLZ");
            _adaptSonstige.InsertCommand.Parameters.Add("@ort", SqlDbType.VarChar, 255, "Ort");
            _adaptSonstige.InsertCommand.Parameters.Add("@lcd", SqlDbType.VarChar, 2, "Landcode");
            _adaptSonstige.InsertCommand.Parameters.Add("@tel", SqlDbType.VarChar, 20, "TelNr");
            _adaptSonstige.InsertCommand.Parameters.Add("@gebdat", SqlDbType.Date, 9, "Geburtsdatum");
            _adaptSonstige.InsertCommand.Parameters.Add("@rel", SqlDbType.VarChar, 255, "Religion");
            _adaptSonstige.InsertCommand.Parameters.Add("@obs", SqlDbType.VarChar, 255, "Obsorge");
            _adaptSonstige.InsertCommand.Parameters.Add("@brf", SqlDbType.VarChar, 255, "Beruf");
            _adaptSonstige.InsertCommand.Parameters.Add("@famstnd", SqlDbType.VarChar, 255, "Familienstand");
            _adaptSonstige.InsertCommand.Parameters.Add("@geschl", SqlDbType.VarChar, 8, "Geschlecht");
            _adaptSonstige.InsertCommand.Parameters.Add("@staat", SqlDbType.VarChar, 255, "Staatsbuergerschaft");
            _adaptSonstige.InsertCommand.Parameters.Add("@anm", SqlDbType.NVarChar, 50000, "Anmerkungen");
            #endregion
            

            #region adaptMA
            if(!geloescht)
                _adaptMa = new SqlDataAdapter("select * from Mitarbeiter where geloescht = 0", _con);
            else
                _adaptMa = new SqlDataAdapter("select * from Mitarbeiter where geloescht = 1", _con);

            _adaptMa.UpdateCommand = new SqlCommand("update Mitarbeiter set Nachname = @nn, Vorname = @vn, Straße = @st, Hausnummer = @hn, PLZ = @plz, Ort = @ort, Landcode = @lcd, TelNr=@num, Geburtsdatum = @gd, Geschlecht = @ge, Familienstand = @fs, Religion = @rel, geloescht = @gel, Staatsbuergerschaft = @sta where MNr = @mnr ", _con);
            _adaptMa.UpdateCommand.Parameters.Add("@nn", SqlDbType.VarChar, 255, "Nachname");
            _adaptMa.UpdateCommand.Parameters.Add("@vn", SqlDbType.VarChar, 255, "Vorname");
            _adaptMa.UpdateCommand.Parameters.Add("@st", SqlDbType.VarChar, 255, "Straße");
            _adaptMa.UpdateCommand.Parameters.Add("@hn", SqlDbType.VarChar, 10, "Hausnummer");
            _adaptMa.UpdateCommand.Parameters.Add("@plz", SqlDbType.VarChar, 255, "PLZ");
            _adaptMa.UpdateCommand.Parameters.Add("@ort", SqlDbType.VarChar, 255, "Ort");
            _adaptMa.UpdateCommand.Parameters.Add("@lcd", SqlDbType.VarChar, 2, "Landcode");
            _adaptMa.UpdateCommand.Parameters.Add("@num", SqlDbType.VarChar, 20, "TelNr");
            _adaptMa.UpdateCommand.Parameters.Add("@gd", SqlDbType.Date, 9, "Geburtsdatum");
            _adaptMa.UpdateCommand.Parameters.Add("@ge", SqlDbType.VarChar, 8, "Geschlecht");
            _adaptMa.UpdateCommand.Parameters.Add("@fs", SqlDbType.VarChar, 255, "Familienstand");
            _adaptMa.UpdateCommand.Parameters.Add("@rel", SqlDbType.VarChar, 255, "Religion");
            _adaptMa.UpdateCommand.Parameters.Add("@sta", SqlDbType.VarChar, 255, "Staatsbuergerschaft");
            _adaptMa.UpdateCommand.Parameters.Add("@MNr", SqlDbType.Int, 4, "MNr");
            _adaptMa.UpdateCommand.Parameters.Add("@gel", SqlDbType.Bit, 1, "geloescht");

            _adaptMa.InsertCommand = new SqlCommand("insert into Mitarbeiter values(@mnr, @nn, @vn, @st, @hn, @plz, @ort, @lcd, @num, @gd, @sta, @ge,  @fs,@rel,0)", _con);
            _adaptMa.InsertCommand.Parameters.Add("@nn", SqlDbType.VarChar, 255, "Nachname");
            _adaptMa.InsertCommand.Parameters.Add("@vn", SqlDbType.VarChar, 255, "Vorname");
            _adaptMa.InsertCommand.Parameters.Add("@st", SqlDbType.VarChar, 255, "Straße");
            _adaptMa.InsertCommand.Parameters.Add("@hn", SqlDbType.VarChar, 10, "Hausnummer");
            _adaptMa.InsertCommand.Parameters.Add("@plz", SqlDbType.VarChar, 10, "PLZ");
            _adaptMa.InsertCommand.Parameters.Add("@ort", SqlDbType.VarChar, 255, "Ort");
            _adaptMa.InsertCommand.Parameters.Add("@lcd", SqlDbType.VarChar, 2, "Landcode");
            _adaptMa.InsertCommand.Parameters.Add("@num", SqlDbType.VarChar, 20, "TelNr");
            _adaptMa.InsertCommand.Parameters.Add("@gd", SqlDbType.Date, 9, "Geburtsdatum");
            _adaptMa.InsertCommand.Parameters.Add("@ge", SqlDbType.VarChar, 8, "Geschlecht");
            _adaptMa.InsertCommand.Parameters.Add("@fs", SqlDbType.VarChar, 255, "Familienstand");
            _adaptMa.InsertCommand.Parameters.Add("@sta", SqlDbType.VarChar, 255, "Staatsbuergerschaft");
            _adaptMa.InsertCommand.Parameters.Add("@MNr", SqlDbType.Int, 4, "MNr");
            _adaptMa.InsertCommand.Parameters.Add("@rel", SqlDbType.VarChar, 255, "Religion");
            #endregion adaptMA

            #region adaptBefKon
            _adaptBefKon = new SqlDataAdapter("select b.BefKonNr, p.PNr, p.Nachname, p.Vorname, Convert(varchar, p.Geburtsdatum, 104)'Geburtsdatum', Convert(varchar, b.Datum_Beginn, 104)'Datum_Beginn', Convert(varchar, b.Datum_Ende, 104)'Datum_Ende' from BefKon b join Person p on(b.PNr = p.PNr)", _con);
                        
            #endregion

            #region adaptTermin
            _adaptTermin = new SqlDataAdapter("select * from Termin", _con);


            _adaptTermin.UpdateCommand = new SqlCommand("Update Termin set Berater = @ber, Zweitberater = @zweiber, Datum = @dat, Beginn = @beg, Ende = @end, Dauer = @dauer, Befassungsart = @befart, Angebotskategorie = @angeb, Akt = @akt where BefKonNr = @nr and TNr = @tnr", _con);
            _adaptTermin.UpdateCommand.Parameters.Add("@ber", SqlDbType.Int, 1000, "Berater");
            _adaptTermin.UpdateCommand.Parameters.Add("@zweiber", SqlDbType.Int, 1000, "Zweitberater");
            _adaptTermin.UpdateCommand.Parameters.Add("@dat", SqlDbType.Date, 8, "Datum");
            _adaptTermin.UpdateCommand.Parameters.Add("@beg", SqlDbType.VarChar, 5, "Beginn");
            _adaptTermin.UpdateCommand.Parameters.Add("@end", SqlDbType.VarChar, 5, "Ende");
            _adaptTermin.UpdateCommand.Parameters.Add("@dauer", SqlDbType.Int, 1000, "Dauer");
            _adaptTermin.UpdateCommand.Parameters.Add("@befart", SqlDbType.Int, 255, "Befassungsart");
            _adaptTermin.UpdateCommand.Parameters.Add("@angeb", SqlDbType.Int, 255, "Angebotskategorie");
            _adaptTermin.UpdateCommand.Parameters.Add("@akt", SqlDbType.VarChar, 8000, "Akt");
            _adaptTermin.UpdateCommand.Parameters.Add("@nr", SqlDbType.Int, 10000, "BefKonNr");
            _adaptTermin.UpdateCommand.Parameters.Add("@tnr", SqlDbType.Int, 10000, "TNr");

            _adaptTermin.InsertCommand = new SqlCommand("insert into Termin values(@nr, @tnr, @ber, @zweiber, @dat, @beg, @end, @dauer, @befart, @angeb, @akt)", _con);
            _adaptTermin.InsertCommand.Parameters.Add("@ber", SqlDbType.Int, 1000, "Berater");
            _adaptTermin.InsertCommand.Parameters.Add("@zweiber", SqlDbType.Int, 1000, "Zweitberater");
            _adaptTermin.InsertCommand.Parameters.Add("@dat", SqlDbType.Date, 8, "Datum");
            _adaptTermin.InsertCommand.Parameters.Add("@beg", SqlDbType.VarChar, 5, "Beginn");
            _adaptTermin.InsertCommand.Parameters.Add("@end", SqlDbType.VarChar, 5, "Ende");
            _adaptTermin.InsertCommand.Parameters.Add("@dauer", SqlDbType.Int, 1000, "Dauer");
            _adaptTermin.InsertCommand.Parameters.Add("@befart", SqlDbType.Int, 255, "Befassungsart");
            _adaptTermin.InsertCommand.Parameters.Add("@angeb", SqlDbType.Int, 255, "Angebotskategorie");
            _adaptTermin.InsertCommand.Parameters.Add("@akt", SqlDbType.VarChar, 8000, "Akt");
            _adaptTermin.InsertCommand.Parameters.Add("@nr", SqlDbType.Int, 10000, "BefKonNr");
            _adaptTermin.InsertCommand.Parameters.Add("@tnr", SqlDbType.Int, 10000, "TNr");
            #endregion

            #region adaptInst
            if(!geloescht)
                _adaptInst = new SqlDataAdapter("select * from Institution where geloescht = 0", _con);
            else
                _adaptInst = new SqlDataAdapter("select * from Institution where geloescht = 1", _con);

            _adaptInst.InsertCommand = new SqlCommand("Insert into Institution values (@nr,@typ,@bez,@pers,@straße,@haus,@plz,@ort,@lcd,@telnr,@tel,@email,@anmerk,0)", _con);
            _adaptInst.InsertCommand.Parameters.Add("@nr", SqlDbType.VarChar, 8, "INr");
            _adaptInst.InsertCommand.Parameters.Add("@typ", SqlDbType.VarChar, 4, "ITyp");
            _adaptInst.InsertCommand.Parameters.Add("@bez", SqlDbType.VarChar, 255, "Bezeichnung");
            _adaptInst.InsertCommand.Parameters.Add("@pers", SqlDbType.VarChar, 255, "Ansprechperson");
            _adaptInst.InsertCommand.Parameters.Add("@telnr", SqlDbType.VarChar, 20, "TelNr");
            _adaptInst.InsertCommand.Parameters.Add("@tel", SqlDbType.VarChar, 20, "TelNr2");
            _adaptInst.InsertCommand.Parameters.Add("@straße", SqlDbType.VarChar, 255, "Straße");
            _adaptInst.InsertCommand.Parameters.Add("@plz", SqlDbType.VarChar, 10, "PLZ");
            _adaptInst.InsertCommand.Parameters.Add("@haus", SqlDbType.VarChar, 10, "Hausnummer");
            _adaptInst.InsertCommand.Parameters.Add("@ort", SqlDbType.VarChar, 255, "Ort");
            _adaptInst.InsertCommand.Parameters.Add("@lcd", SqlDbType.VarChar, 2, "Landcode");
            _adaptInst.InsertCommand.Parameters.Add("@email", SqlDbType.VarChar, 255, "Email");
            _adaptInst.InsertCommand.Parameters.Add("@anmerk", SqlDbType.VarChar, 50000, "Anmerkungen");

            _adaptInst.UpdateCommand = new SqlCommand("Update Institution set Email=@email,Ansprechperson=@pers, Bezeichnung=@bez,TelNr=@telnr, TelNr2 = @tel, Straße=@straße,PLZ=@plz,Hausnummer=@haus,Ort=@ort, Landcode = @lcd, Anmerkungen=@anmerk where INr =@nr", _con);
            _adaptInst.UpdateCommand.Parameters.Add("@nr", SqlDbType.VarChar, 8, "INr");
            _adaptInst.UpdateCommand.Parameters.Add("@bez", SqlDbType.VarChar, 255, "Bezeichnung");
            _adaptInst.UpdateCommand.Parameters.Add("@pers", SqlDbType.VarChar, 255, "Ansprechperson");
            _adaptInst.UpdateCommand.Parameters.Add("@telnr", SqlDbType.VarChar, 20, "TelNr");
            _adaptInst.UpdateCommand.Parameters.Add("@tel", SqlDbType.VarChar, 20, "TelNr2");
            _adaptInst.UpdateCommand.Parameters.Add("@straße", SqlDbType.VarChar, 255, "Straße");
            _adaptInst.UpdateCommand.Parameters.Add("@plz", SqlDbType.VarChar, 10, "PLZ");
            _adaptInst.UpdateCommand.Parameters.Add("@haus", SqlDbType.VarChar, 10, "Hausnummer");
            _adaptInst.UpdateCommand.Parameters.Add("@ort", SqlDbType.VarChar, 255, "Ort");
            _adaptInst.UpdateCommand.Parameters.Add("@lcd", SqlDbType.VarChar, 2, "Landcode");
            _adaptInst.UpdateCommand.Parameters.Add("@email", SqlDbType.VarChar, 255, "Email");
            _adaptInst.UpdateCommand.Parameters.Add("@anmerk", SqlDbType.VarChar, 50000, "Anmerkungen");
            #endregion

            #region adaptEvent
            _adaptEvent = new SqlDataAdapter("Select * from _Events", _con);

            _adaptEvent.InsertCommand = new SqlCommand("Insert into _Events values(@enr,@etyp,@titel,@beginn,@ende,@strasse,@haus,@plz,@ort,@lcd,@komm)", _con);
            _adaptEvent.InsertCommand.Parameters.Add("@enr", SqlDbType.Int, 1000, "ENr");
            _adaptEvent.InsertCommand.Parameters.Add("@etyp", SqlDbType.VarChar, 6, "ETyp");
            _adaptEvent.InsertCommand.Parameters.Add("@titel", SqlDbType.VarChar, 255, "Titel");
            _adaptEvent.InsertCommand.Parameters.Add("@beginn", SqlDbType.DateTime2, 50, "Datum_Beginn");
            _adaptEvent.InsertCommand.Parameters.Add("@ende", SqlDbType.DateTime2, 50, "Datum_Ende");
            _adaptEvent.InsertCommand.Parameters.Add("@strasse", SqlDbType.VarChar, 255, "Straße");
            _adaptEvent.InsertCommand.Parameters.Add("@haus", SqlDbType.VarChar, 10, "Hausnummer");
            _adaptEvent.InsertCommand.Parameters.Add("@plz", SqlDbType.VarChar, 10, "PLZ");
            _adaptEvent.InsertCommand.Parameters.Add("@ort", SqlDbType.VarChar, 255, "Ort");
            _adaptEvent.InsertCommand.Parameters.Add("@lcd", SqlDbType.VarChar, 2, "Landcode");
            _adaptEvent.InsertCommand.Parameters.Add("@komm", SqlDbType.VarChar, 50000, "Kommentar");

            _adaptEvent.UpdateCommand = new SqlCommand("Update _Events set ETyp = @etyp, Titel = @titel, Datum_Beginn = @beginn, Datum_Ende = @ende, Straße = @strasse,PLZ = @plz, Hausnummer = @haus, Ort = @ort, Landcode = @lcd, Kommentar = @komm where ENr = @enr", _con);
            _adaptEvent.UpdateCommand.Parameters.Add("@enr", SqlDbType.Int, 1000, "ENr");
            _adaptEvent.UpdateCommand.Parameters.Add("@etyp", SqlDbType.VarChar, 6, "ETyp");
            _adaptEvent.UpdateCommand.Parameters.Add("@titel", SqlDbType.VarChar, 255, "Titel");
            _adaptEvent.UpdateCommand.Parameters.Add("@beginn", SqlDbType.DateTime2, 50, "Datum_Beginn");
            _adaptEvent.UpdateCommand.Parameters.Add("@ende", SqlDbType.DateTime2, 50, "Datum_Ende");
            _adaptEvent.UpdateCommand.Parameters.Add("@strasse", SqlDbType.VarChar, 255, "Straße");
            _adaptEvent.UpdateCommand.Parameters.Add("@haus", SqlDbType.VarChar, 10, "Hausnummer");
            _adaptEvent.UpdateCommand.Parameters.Add("@plz", SqlDbType.VarChar, 10, "PLZ");
            _adaptEvent.UpdateCommand.Parameters.Add("@ort", SqlDbType.VarChar, 255, "Ort");
            _adaptEvent.UpdateCommand.Parameters.Add("@lcd", SqlDbType.VarChar, 2, "Landcode");
            _adaptEvent.UpdateCommand.Parameters.Add("@komm", SqlDbType.VarChar, 50000, "Kommentar");

            #endregion

        }
    }
}

