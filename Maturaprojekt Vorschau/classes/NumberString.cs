﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maturaprojekt_Vorschau
{
    class NumberString
    {
       
            private string ID;
            private string Name;

            public NumberString(string ID, string Name)
            {
                this.ID = ID;
                this.Name = Name;
            }

            public string getID { get { return ID; } }
            public string getName { get { return Name; } }


        
        public string findName(string ID, List<NumberString> ma)
        {
            for (int i = 0; i < ma.Count; i++)
            {
                NumberString temp = ma[i];
                if (temp.getID == ID)
                    return temp.getName;
            }
            return "Person auswaehlen";
        }
        public string findID(string Name, List<NumberString> ma)
        {
            for (int i = 0; i < ma.Count; i++)
            {
                NumberString temp = ma[i];
                if (temp.getName == Name)
                    return temp.getID;
            }
            return "Diesee Person gibt es nicht";
        }
    }
}

