﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Maturaprojekt_Vorschau
{
    class Tables
    {
        #region Variablen
        private SqlConnection con;


        #endregion

        /// <summary>
        /// Hier die Connection mitgeben.
        /// </summary>
        /// <param name="connection"></param>

        public Tables(SqlConnection connection)
        {
            this.con = connection;  
        }

        /// <summary>
        /// >Hier einfach den CommandString eingeben und der Befehl wird ausgefuehrt
        /// </summary>
        /// <param name="CommandString"></param>
        public void execute(string CommandString)
        {
            try
            {
                SqlCommand command = new SqlCommand(CommandString, con);
                con.Open();
                command.ExecuteNonQuery();
                con.Close();
                
            }catch(Exception ex) { throw new Exception(ex.Message); }
        }    

        
        
        public void insertTest()
        {
            try
            {
                con.Open();



                new SqlCommand("insert into Mitarbeiter values(1,'Lassenberger','Adele','Fakestreet',34,9400,'Wolfsberg','AT','0664123456789','1-1-1998','maennlich','LD','röm.-kath.')", con).ExecuteNonQuery();
                new SqlCommand("insert into Mitarbeiter values(2,'Preßlauer','Thomas','Fakestreet',21,9433,'St.Andrae','AT','06604567890','1-17-1997','maennlich','LD','röm.-kath.')", con).ExecuteNonQuery();
                new SqlCommand("insert into Mitarbeiter values(3,'Maier','Michaela','Falschestraße',34,9470,'St.Paul','AT','06762144678','1-1-1861','weiblich','VH','röm.-kath.')", con).ExecuteNonQuery();


                new SqlCommand("insert into Person values('2016_001','IK','Abend','David','Pottendorfer Straße',61,7422,'Riedlingsdorf','06767702321','7-3-1992','röm.-kath.','KE gemeinsam','Arzt','LD','maennlich','AT','Der Klient scheint erfunden, dem ist auch so. Er wurde nur eingefuegt um zu testen, ob das Programm richtig funktioniert!')",con).ExecuteNonQuery();
                new SqlCommand("insert into Person values('2016_002','IK','Schaefer','Stefan','Griffen',70,3564,'Bad Eisenkappel','06602399941','1-1-2005','röm.-kath.',null,'Schueler','LD','maennlich','AT','Diese Person wurde nur eingefuegt um dem einzigen Klienten dieser Datenbank einen Vater zu geben')",con).ExecuteNonQuery();
                new SqlCommand("insert into Person values('2016_003','IK','Kleister','Maria','Völkermarkter Straße',59,4950,'Griffen','06688872352','3-24-2001','evan.',null,'Schueler','LD','weiblich','AT','Das ist die Mutter des Klienten.')",con).ExecuteNonQuery();
                new SqlCommand("insert into Person values('1','KV','Richter','Markus','Gasteiner strasse',70,3564,'Freischling','06802342341','7-23-1960','röm.-kath.',null,'Autoverkaeufer','VH','maennlich','AT','Diese Person wurde nur eingefuegt um dem einzigen Klienten dieser Datenbank einen Vater zu geben')",con).ExecuteNonQuery();
                new SqlCommand("insert into Person values('2','KM','Baecker','Lea','Wachauerstrasse',53,4950,'Weirading','06641312352','7-22-1959','röm.-kath.',null,'SQL Programmierer','VH','weiblich','AT','Das ist die Mutter des Klienten.')",con).ExecuteNonQuery();

                new SqlCommand("insert into Person_Bez values('2016_001','IK','1','KV')",con).ExecuteNonQuery();
                new SqlCommand("insert into Person_Bez values('2016_001','IK','2','KM')",con).ExecuteNonQuery();

                new SqlCommand("insert into BefKon values(1,'2016_001','IK','1-1-2015','1-6-2015','Diagnose','Medikation',1,2,18,null,null,'V 103','B 04','Auftrag','Symptomatik','Ziel','Obsorge')",con).ExecuteNonQuery();
                new SqlCommand("insert into BefKon values(2,'2016_002','IK','3-12-2014',null,'Diagnose','Medikation',2,3,11,null,null,'V 111','B 11','Auftrag','Symptomatik','Ziel','Obsorge')",con).ExecuteNonQuery();
                new SqlCommand("insert into BefKon values(3,'2016_003','IK','5-9-2016',null,'Diagnose','Medikation',3,1,9,null,null,'V 107','B 01','Auftrag','Symptomatik','Ziel','Obsorge')",con).ExecuteNonQuery();
                new SqlCommand("insert into BefKon values(4,'2016_001','IK','1-7-2015',null,'Diagnose','Medikation',1,2,16,null,null,'V 119','B 02','Auftrag','Symptomatik','Ziel','Obsorge')",con).ExecuteNonQuery();

                new SqlCommand("insert into Termin values(1,1,1,2,'2016-09-11','15:00','16:00',30,'Befassungsart','Angebotskategorie','IK;KM;','Akt')", con).ExecuteNonQuery();
                new SqlCommand("insert into Termin values(3,2,2,3,'2016-10-11','17:00','18:00',30,'Befassungsart','Angebotskategorie','Setting','Akt')", con).ExecuteNonQuery();
                new SqlCommand("insert into Termin values(1,3,3,1,'2016-03-02','13:00','14:00',30,'Befassungsart','Angebotskategorie','Setting','Akt')", con).ExecuteNonQuery();
                new SqlCommand("insert into Termin values(1,4,2,3,'2016-04-03','16:00','17:00',30,'Befassungsart','Angebotskategorie','Setting','Akt')", con).ExecuteNonQuery();
                new SqlCommand("insert into Termin values(1,5,1,2,'2016-05-04','14:00','15:00',30,'Befassungsart','Angebotskategorie','Setting','Akt')", con).ExecuteNonQuery();

                con.Close();
            }
            catch (Exception ex)
            {
                if (con.State == System.Data.ConnectionState.Open)
                 con.Close(); 
                throw new Exception(ex.Message);
            }
        }
    }
}
