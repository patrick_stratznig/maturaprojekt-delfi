﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maturaprojekt_Vorschau
{
    class UserData
    {
        private int _MNr;
        private string _nachname;
        public string _vorname;
        private string _geburtsdatum;

        public UserData(int mNr, string nachname, string vorname, string Geburtsdatum)
        {
            _MNr = mNr;
            _nachname = nachname;
            _vorname = vorname;
            _geburtsdatum = Geburtsdatum;

        }

        public int GetMNr => this._MNr;
        public string GetVorname => _vorname;
        public string GetGeburtsdatum => _geburtsdatum;
        public string GetNachname => _nachname;
    }
}
