﻿namespace Maturaprojekt_Vorschau
{
    partial class Anmeldedialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Anmeldedialog));
            this.label1 = new System.Windows.Forms.Label();
            this.txtBenutzername = new System.Windows.Forms.TextBox();
            this.txtPasswort = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmdAbbrechen = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cmdAnmelden = new System.Windows.Forms.Button();
            this.lblInfo = new System.Windows.Forms.Label();
            this.cmdSettings = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(159, 45);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Benutzername";
            // 
            // txtBenutzername
            // 
            this.txtBenutzername.Location = new System.Drawing.Point(254, 42);
            this.txtBenutzername.Margin = new System.Windows.Forms.Padding(2);
            this.txtBenutzername.Name = "txtBenutzername";
            this.txtBenutzername.Size = new System.Drawing.Size(154, 21);
            this.txtBenutzername.TabIndex = 1;
            // 
            // txtPasswort
            // 
            this.txtPasswort.Location = new System.Drawing.Point(254, 78);
            this.txtPasswort.Margin = new System.Windows.Forms.Padding(2);
            this.txtPasswort.Name = "txtPasswort";
            this.txtPasswort.PasswordChar = '●';
            this.txtPasswort.Size = new System.Drawing.Size(154, 21);
            this.txtPasswort.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(159, 80);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Passwort";
            // 
            // cmdAbbrechen
            // 
            this.cmdAbbrechen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdAbbrechen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAbbrechen.Location = new System.Drawing.Point(243, 137);
            this.cmdAbbrechen.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAbbrechen.Name = "cmdAbbrechen";
            this.cmdAbbrechen.Size = new System.Drawing.Size(80, 25);
            this.cmdAbbrechen.TabIndex = 4;
            this.cmdAbbrechen.Text = "Abbrechen";
            this.cmdAbbrechen.UseVisualStyleBackColor = true;
            this.cmdAbbrechen.Click += new System.EventHandler(this.cmdAbbrechen_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Maturaprojekt_Vorschau.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(7, 15);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(149, 105);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // cmdAnmelden
            // 
            this.cmdAnmelden.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdAnmelden.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnmelden.Location = new System.Drawing.Point(326, 137);
            this.cmdAnmelden.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnmelden.Name = "cmdAnmelden";
            this.cmdAnmelden.Size = new System.Drawing.Size(80, 25);
            this.cmdAnmelden.TabIndex = 5;
            this.cmdAnmelden.Text = "Anmelden";
            this.cmdAnmelden.UseVisualStyleBackColor = true;
            this.cmdAnmelden.Click += new System.EventHandler(this.cmdAnmelden_Click);
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.ForeColor = System.Drawing.Color.Red;
            this.lblInfo.Location = new System.Drawing.Point(162, 15);
            this.lblInfo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(0, 15);
            this.lblInfo.TabIndex = 6;
            // 
            // cmdSettings
            // 
            this.cmdSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSettings.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSettings.Location = new System.Drawing.Point(7, 138);
            this.cmdSettings.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSettings.Name = "cmdSettings";
            this.cmdSettings.Size = new System.Drawing.Size(93, 25);
            this.cmdSettings.TabIndex = 7;
            this.cmdSettings.Text = "Einstellungen";
            this.cmdSettings.UseVisualStyleBackColor = true;
            this.cmdSettings.Click += new System.EventHandler(this.cmdSettings_Click);
            // 
            // Anmeldedialog
            // 
            this.AcceptButton = this.cmdAnmelden;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.AntiqueWhite;
            this.ClientSize = new System.Drawing.Size(415, 170);
            this.Controls.Add(this.cmdSettings);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.cmdAnmelden);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cmdAbbrechen);
            this.Controls.Add(this.txtPasswort);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBenutzername);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Anmeldedialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Anmelden";
            this.Load += new System.EventHandler(this.Anmeldedialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBenutzername;
        private System.Windows.Forms.TextBox txtPasswort;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button cmdAbbrechen;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button cmdAnmelden;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Button cmdSettings;
    }
}