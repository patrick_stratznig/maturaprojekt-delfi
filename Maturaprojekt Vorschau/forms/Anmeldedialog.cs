﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using Maturaprojekt_Vorschau.Properties;
using Tools;


namespace Maturaprojekt_Vorschau
{
    public partial class Anmeldedialog : Form
    {
        private SqlConnection _con;
        private Configuration _conf;

        public SqlConnection GetCon => _con;
        public Configuration GetConfig => _conf;
        public string GetUser => txtBenutzername.Text;

        public Anmeldedialog()
        {
            InitializeComponent();
        }

        private void Anmeldedialog_Load(object sender, EventArgs e)
        {
            try
            {
                _conf = new Configuration(Resources.ConfigurationPath);
                Font = new Font("Microsoft Sans Serif", _conf.getInt("text-size"));

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        private void cmdAnmelden_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBenutzername.Text) && !string.IsNullOrEmpty(txtPasswort.Text))
                Connect();
            else
                MessageBox.Show("Benutzername und Passwort müssen ausgefüllt sein!", "Hinweis");
        }
        private void Connect()
        {
            txtBenutzername.SelectAll();
            txtPasswort.SelectAll();

            _con = new SqlConnection(@"data source=" + _conf.getString("data-source") + ";Initial Catalog=" +
                                  _conf.getString("initial-catalog") + ";UID= '" + txtBenutzername.Text + "';PWD= '" +
                                  txtPasswort.Text + "'");
            try
            {
                _con.Open();
                _con.Close();
                DialogResult = DialogResult.OK;
                this.Close();
            }
            catch
            {
                lblInfo.Text = "Benutzername oder Passwort falsch!";
            }
        }
        
        private void cmdSettings_Click(object sender, EventArgs e)
        {
            try
            {
                Einstellungen settings = new Einstellungen(_con);
                settings.ShowDialog();

            }catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void cmdAbbrechen_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
