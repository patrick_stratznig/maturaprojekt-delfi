﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Maturaprojekt_Vorschau.Properties;
using NLog;
using Tools;

namespace Maturaprojekt_Vorschau
{
    public partial class Auswahl : Form
    {
        #region Variablen        
        private string _typ = "";
        private BindingSource _bs;
        private SqlConnection _con;
        private FilterTextBox _txt;
        private SqlDataReader _reader;
        private Logger _logger;
        private DataTable _dt;
        #endregion


        public Auswahl()
        {
            InitializeComponent();
        }
        public Auswahl(BindingSource bs, SqlConnection con)
        {
            InitializeComponent();
            _bs = bs;
            _con = con;
            _typ = "";
        }
        public Auswahl(BindingSource bs, SqlConnection con, string typ)
        {
            InitializeComponent();
            _bs = bs;
            _con = con;
            _typ = typ;
        }

        public Auswahl(SqlConnection con, SqlDataReader reader, string typ) : this()
        {
            _reader = reader;
            _con = con;
            _typ = typ;
        }

        public string GetId { get { return dgvAuswahl.Rows[dgvAuswahl.SelectedCells[0].RowIndex].Cells[0].Value.ToString(); } }
        public new string GetType { get { return dgvAuswahl.Rows[dgvAuswahl.SelectedCells[0].RowIndex].Cells[1].Value.ToString(); } }



        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAuswahl.CurrentCell == null)
                {
                    MessageBox.Show(@"Bitte wählen Sie den Datensatz aus!", Resources.MessageBoxHeaderHinweis);
                }
                else
                    DialogResult = DialogResult.OK;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool LoadReader()
        {
            try
            {
                if (_reader != null)
                {
                    _dt = new DataTable();
                    _dt.Load(_reader);
                    dgvAuswahl.DataSource = _dt;
                    return true;
                }
                else
                    return false;


            }
catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage, Resources.MessageBoxHeaderHinweis);
                return false;

            }
        }
        private void new_Auswahl_Load(object sender, EventArgs e)
        {
            try
            {
                var conf = new Configuration(Resources.ConfigurationPath);
                this.Font = new Font("Microsoft Sans Serif", conf.getInt("text-size"));

                _logger = LogManager.GetCurrentClassLogger();

                if (!LoadReader())
                    dgvAuswahl.DataSource = _bs;

                switch (_typ)
                {

                    case "Person":
                        KlientAnzeige();
                        break;
                    case "Client":
                        KlientAnzeige();
                        break;
                    case "Prozess":
                        KlientAnzeige();
                        break;
                    case "Intervision":
                        KlientAnzeige();
                        break;
                    case "Sonstige":
                        KlientAnzeige();
                        break;
                    case "Telefon":
                        KlientAnzeige();
                        break;

                    case "Termin":
                        dgvAuswahl.Columns[3].Visible = false;
                        dgvAuswahl.Columns[8].Visible = false;
                        dgvAuswahl.Columns[9].Visible = false;
                        dgvAuswahl.Columns[10].Visible = false;
                        break;

                    case "Mitarbeiter":
                        dgvAuswahl.Columns[3].Visible = false;
                        dgvAuswahl.Columns[4].Visible = false;
                        dgvAuswahl.Columns[7].Visible = false;
                        dgvAuswahl.Columns[8].Visible = false;
                        dgvAuswahl.Columns[10].Visible = false;
                        dgvAuswahl.Columns[11].Visible = false;
                        dgvAuswahl.Columns[12].Visible = false;
                        dgvAuswahl.Columns[13].Visible = false;
                        dgvAuswahl.Columns[14].Visible = false;
                        break;

                    case "INST":
                        dgvAuswahl.Columns[0].Visible = false;
                        dgvAuswahl.Columns[4].Visible = false;
                        dgvAuswahl.Columns[5].Visible = false;
                        dgvAuswahl.Columns[6].Visible = false;
                        dgvAuswahl.Columns[7].Visible = false;
                        dgvAuswahl.Columns[8].Visible = false;
                        dgvAuswahl.Columns[12].Visible = false;
                        dgvAuswahl.Columns[13].Visible = false;
                        break;

                    case "Event":
                        dgvAuswahl.Columns[0].Visible = false;
                        dgvAuswahl.Columns[5].Visible = false;
                        dgvAuswahl.Columns[6].Visible = false;
                        dgvAuswahl.Columns[7].Visible = false;
                        dgvAuswahl.Columns[8].Visible = false;
                        dgvAuswahl.Columns[9].Visible = false;
                        break;
                }

                _txt = new FilterTextBox(dgvAuswahl, this);
                _txt.ValChangedHandler += txt_TextChanged;

                if (dgvAuswahl.RowCount != 0)
                    dgvAuswahl.Rows[0].Selected = true;

                int sum = 0;
                for (int i = 0; i < dgvAuswahl.Columns.Count; i++)
                {
                    if (dgvAuswahl.Columns[i].Visible)
                        sum += dgvAuswahl.Columns[i].Width;
                }

                dgvAuswahl.Width = sum + dgvAuswahl.RowHeadersWidth + 2;

                this.Width = dgvAuswahl.Width + 33;

                
                dgvAuswahl.Location = new Point(9, 50);
                dgvAuswahl.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom;              

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage, Resources.MessageBoxHeaderHinweis);
            }

        }

        private void KlientAnzeige()
        {
            dgvAuswahl.Columns[4].Visible = false;
            dgvAuswahl.Columns[5].Visible = false;
            dgvAuswahl.Columns[8].Visible = false;
            dgvAuswahl.Columns[9].Visible = false;
            dgvAuswahl.Columns[11].Visible = false;
            dgvAuswahl.Columns[12].Visible = false;
            dgvAuswahl.Columns[13].Visible = false;
            dgvAuswahl.Columns[14].Visible = false;
            dgvAuswahl.Columns[15].Visible = false;
            dgvAuswahl.Columns[16].Visible = false;
            dgvAuswahl.Columns[17].Visible = false;
        }
        private void txt_TextChanged(object sender, FilterTextBox.TextChangeEventArgs e)
        {
            try
            {
                string filter = "";

                for (int i = 0; i < dgvAuswahl.Columns.Count; i++)
                {
                    if (_txt[i] != "")
                    {
                        if (dgvAuswahl.Columns[i].ValueType.Name.Contains("Int"))
                            if (filter != "")
                                filter += " AND " + dgvAuswahl.Columns[i].HeaderText + " >= '" + _txt[i].ToString() + "'";
                            else
                                filter = dgvAuswahl.Columns[i].HeaderText + " >= '" + _txt[i].ToString() + "'";

                        //else if (dgvAuswahl.Columns[i].ValueType.Name.Contains("Date"))
                        //    if (filter != "")
                        //        filter += " AND " + dgvAuswahl.Columns[i].HeaderText + " Like '*" + DateTime.Parse(_txt[i].ToString()).Date.ToString("yyyy-MM-dd") + " 00:00:00 *'";
                        //    else
                        //        filter = dgvAuswahl.Columns[i].HeaderText + " Like '*" + DateTime.Parse(_txt[i].ToString()).Date.ToString("yyyy-MM-dd") + " 00:00:00 *'";
                        else
                        if (filter != "")
                            filter += " AND " + dgvAuswahl.Columns[i].HeaderText + " like '*" + _txt[i].ToString() + "*'";
                        else
                            filter = dgvAuswahl.Columns[i].HeaderText + " like '*" + _txt[i].ToString() + "*'";
                    }
                }
                if (_dt == null)
                    _bs.Filter = filter;
                else
                    _dt.DefaultView.RowFilter = filter;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void dgvAuswahl_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            try
            {
                if (_txt != null)
                    _txt.resizeColumns(dgvAuswahl);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void dgvAuswahl_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnOK_Click(null, null);
        }
    }
}
