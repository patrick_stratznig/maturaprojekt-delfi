﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using Maturaprojekt_Vorschau.Properties;
using NLog;
using Tools;

namespace Maturaprojekt_Vorschau
{

    public partial class BefKon : Form
    {
        #region Variablen

        private Logger _logger;

        private DataTable _dtBefKon;
        private int _BefKonNr;
        private string _PNr;
        private BindingSource _bs;
        private SqlConnection _con;

        private List<ownTextbox> _boxes = new List<ownTextbox>();
        private List<string> _files = new List<string>();

        private string _typ;
        private string _uberweiser;
        private string _uberweiserTyp;

        private MethodRepository _rep;
        private string getBefKonClientId;
        private string v;
        private SqlDataAdapter _adaptBefKon;

        private Configuration _conf;
        private SqlDataReader reader;

        #endregion

        public BefKon()
        {
            InitializeComponent();
        }
        class Person
        {
            public string PNr;
            public string PTyp;

            public Person(string PNr, string PTyp)
            {
                this.PNr = PNr;
                this.PTyp = PTyp;
            }
        }

        public BefKon(SqlConnection con, string Nummer, string typ) : this()
        {
            _con = con;
            _typ = typ;

            if (typ == "new")
                _PNr = Nummer;
            else
                _BefKonNr = int.Parse(Nummer);
        }

        private void new_BefKon_Load(object sender, EventArgs e)
        {
            if (_con.State != ConnectionState.Open)
                _con.Open();

            dgvFiles.Columns[0].ReadOnly = true;
            dgvFiles.Columns[1].ReadOnly = true;
            dgvFiles.Columns[2].ReadOnly = false;
            //Damit ein Kommentar bei einer hinzugefügten Datei angelegt werden kann

            _boxes.Add(anamFami);
            _boxes.Add(anamEntw);
            _boxes.Add(anamEltern);
            _boxes.Add(anamUmgang);

            lblFallfuehrender.Text = "";
            lblFallfuehrend2.Text = "";
            lblDiverses.Text = "";
            lblBehandlungsanlass.Text = "";
            lblInterventionskategorie.Text = "";

            _conf = new Configuration(Resources.ConfigurationPath);
            this.Font = new Font("Microsoft Sans Serif", _conf.getInt("text-size"));

            _logger = LogManager.GetCurrentClassLogger();

            _rep = new MethodRepository(_con);

            _adaptBefKon = _rep.AdaptBefKon;

            #region Textboxen fuellen

            if (_typ == "new")
            {
                newBefKonNr();
                lblClient.Text = _PNr;
            }
            else
            {
                var InitialCommand = new SqlCommand("Select * from BefKon where BefKonNr = @nr", _con);
                InitialCommand.Parameters.Add("@nr", SqlDbType.Int, 10000).Value = _BefKonNr;

                reader = InitialCommand.ExecuteReader();

                reader.Read();

                lblBefKonNr.Text = reader[0].ToString();
                lblClient.Text = reader[1].ToString();
                _PNr = reader[1].ToString();
                dtpBeginn.Text = reader[3].ToString();
                dtpEnde.Text = reader[4].ToString();
                lblFallfuehrender.Text = reader[5].ToString();
                lblFallfuehrend2.Text = reader[6].ToString();
                dtpAnmeldedatum.Text = reader[7].ToString();
                lblUberweiser.Text = _uberweiser = reader[8].ToString();
                _uberweiserTyp = reader[9].ToString();
                lblBehandlungsanlass.Text = reader[10].ToString();
                lblInterventionskategorie.Text = reader[11].ToString();
                allgAuftrag.Text = reader[12].ToString();
                allgSymptomatik.Text = reader[13].ToString();
                allgZiel.Text = reader[14].ToString();
                allgDiagnose.Text = reader[15].ToString();
                allgMedikation.Text = reader[16].ToString();
                txtAnmerkungen2.Text = reader[17].ToString();

                reader.Close();
            }

            #endregion




            dgvContextMenu cmInst = new dgvContextMenu(dgvINST);
            dgvContextMenu cmPers = new dgvContextMenu(dgvPERS);


            try
            {
                SqlCommand command =
                    new SqlCommand("Select Nachname,Vorname,Geburtsdatum,TelNr,Geschlecht from Person where PNr = @nr",
                        _con);
                command.Parameters.Add("@nr", SqlDbType.VarChar, 8, "PNr").Value = _PNr;


                reader = command.ExecuteReader();
                DateTime birthday = System.DateTime.Now;
                //alle Labels fuellen außer Alter

                reader.Read();
                if (!string.IsNullOrEmpty(reader[2].ToString()))
                {
                    birthday = Convert.ToDateTime(reader[2]);
                    lblBirthday.Text = birthday.ToShortDateString();
                }
                else
                {
                    lblBirthday.Text = "";
                }
                lblFirstName.Text = reader[1].ToString();
                lblGender.Text = reader[4].ToString();
                lblLastName.Text = reader[0].ToString();
                lblTelefone.Text = reader[3].ToString();

                reader.Close();



                if (_typ == "new")
                {
                    #region Alter berechnen

                    //Weil beim Anlegen eines neuen BefKons kein Anmeldedatum eingetragen ist, wird einfach das aktuelle Jahr genommen
                    int years = DateTime.Now.Year - Convert.ToDateTime(birthday).Year;
                    birthday = birthday.AddYears(years);
                    if (DateTime.Now.CompareTo(birthday) < 0)
                        years--;
                    if (years != 0)
                        lblAge.Text = years.ToString();
                    else
                    {
                        lblAge.Text = "";
                    }

                    #endregion
                                   }
                else
                {
                    if (new SqlCommand("select Datum_Ende from BefKon where BefKonNr =" + _BefKonNr, _con).ExecuteScalar().ToString() != "")
                        dtpEnde.Format = DateTimePickerFormat.Long;
                    else
                        dtpBeginn.Format = DateTimePickerFormat.Custom;

                    if (new SqlCommand("select Datum_Beginn from BefKon where BefKonNr =" + _BefKonNr, _con).ExecuteScalar().ToString() != "")
                        dtpBeginn.Format = DateTimePickerFormat.Long;
                    else
                        dtpBeginn.Format = DateTimePickerFormat.Custom;

                   

                    #region Alter berechnen

                    DateTime anmeldung = DateTime.Parse(dtpAnmeldedatum.Value.ToShortDateString());
                    int years = 0;

                    if (!string.IsNullOrEmpty(dtpAnmeldedatum.Value.ToString()) &&
                        !string.IsNullOrEmpty(lblBirthday.Text))
                    {
                        years = anmeldung.Year - birthday.Year;
                    }
                    else
                    {
                        years = anmeldung.Year - DateTime.Now.Year;
                    }

                    birthday = birthday.AddYears(years);
                    if (anmeldung.CompareTo(birthday) < 0)
                        years--;

                    if (years != 0)
                        lblAge.Text = years.ToString();
                    else
                    {
                        lblAge.Text = "";
                    }


                    #endregion
                    
                    #region dgvPers eintragen

                    SqlCommand cmd =
                        new SqlCommand("select Bezug_Nr, Bezug_Typ from BefKon_Bez_Person where BefKonNr = @BefKonNr",
                            _con);
                    cmd.Parameters.Add("@BefKonNr", SqlDbType.Int, 4, "BefKonNr").Value = _BefKonNr;

                    reader = cmd.ExecuteReader();
                    List<Person> p = new List<Person>();

                    while (reader.Read())
                    {
                        p.Add(new Person(reader[0].ToString(), reader[1].ToString()));
                    }
                    reader.Close();

                    foreach (Person x in p)
                        DgvPersInsert(x.PNr, x.PTyp);
                    #endregion

                    #region dgvINST eintragen

                    reader = new SqlCommand("select Bezug_Nr, Bezug_Typ from BefKon_Bez_Institution where BefKonNr = " + _BefKonNr, _con).ExecuteReader();
                    List<string> nummer = new List<string>();

                    while (reader.Read())
                    {
                        nummer.Add(reader[0].ToString());
                    }
                    reader.Close();

                    foreach(string s in nummer)
                        DgvInstInsert(s);
                    #endregion


                    #region Anamnesen/Allgemeines eintragen

                    List<ownTextbox> text = new List<ownTextbox>() { anamFami, anamEntw, anamEltern, anamUmgang };

                    reader = new SqlCommand("select * from BefKon_txt where BefKonNr=" + _BefKonNr, _con).ExecuteReader();
                    int count = 0;
                    while (reader.Read())
                    {
                        if (count < _boxes.Count)
                        {
                            _boxes[count].Text = reader[3].ToString();
                            count++;
                        }
                        else
                        {
                            _boxes.Add(new ownTextbox(reader[2].ToString()));
                            tabAnamnese.Controls.Add(_boxes[_boxes.Count - 1]);
                            _boxes[_boxes.Count - 1].Location = new Point(_boxes[_boxes.Count - 2].Location.X,
                            _boxes[_boxes.Count - 2].Location.Y + (_boxes[_boxes.Count - 2].Location.Y - _boxes[_boxes.Count - 3].Location.Y));
                            _boxes[_boxes.Count - 1].Size = _boxes[_boxes.Count - 2].Size;
                            _boxes[_boxes.Count - 1].TabIndex = _boxes[_boxes.Count - 2].TabIndex + 1;
                            _boxes[_boxes.Count - 1].Text = reader[3].ToString();
                            count++;
                        }
                    }
                    reader.Close();

                    #endregion

                    #region TextBoxen mit Werten füllen
                    if (!string.IsNullOrEmpty(lblInterventionskategorie.Text))
                        txtInterventionskategorie.Text = new SqlCommand("select Kategorie from Interventionskategorien where Kennzahl ='" + lblInterventionskategorie.Text + "'",_con).ExecuteScalar().ToString();
                    if (!string.IsNullOrEmpty(lblBehandlungsanlass.Text))
                        txtBehandlungsanlass.Text = new SqlCommand("select Grund from Behandlungsanlaesse where Kennzahl ='" + lblBehandlungsanlass.Text + "'", _con).ExecuteScalar().ToString();
                    if(!string.IsNullOrEmpty(lblFallfuehrender.Text))
                    txtFall1.Text = new SqlCommand("select Nachname from Mitarbeiter where MNr =" + lblFallfuehrender.Text, _con).ExecuteScalar().ToString();
                    if(!string.IsNullOrEmpty(lblFallfuehrend2.Text))
                    txtFall2.Text = new SqlCommand("select Nachname from Mitarbeiter where MNr =" + lblFallfuehrend2.Text, _con).ExecuteScalar().ToString();

                    #endregion

                    #region dgvFiles füllen

                    reader = new SqlCommand("Select * from BefKon_Dateien where BefKonNr = " + _BefKonNr, _con).ExecuteReader();

                    while (reader.Read())
                    {
                        object[] temp =
                        {
                            reader[2].ToString(), reader[2].ToString().Substring(reader[2].ToString().LastIndexOf('\\') + 1,
                                reader[2].ToString().Length - reader[2].ToString().LastIndexOf('\\') - 1),
                            reader[4].ToString()
                        };

                        dgvFiles.Rows.Add(temp);
                    }
                    reader.Close();

                    #endregion

                    if (_uberweiser != "")
                        txtUeberweiser.Text =
                            new SqlCommand("Select Bezeichnung from Institution where INr = " + _uberweiser + " AND ITyp ='" + _uberweiserTyp + "'", _con)
                                .ExecuteScalar().ToString();

                }
                lblFallfuehrender.Visible = false;
                lblFallfuehrend2.Visible = false;
                lblInterventionskategorie.Visible = false;
                lblBehandlungsanlass.Visible = false;
                
                #region Setting eintragen

                reader = new SqlCommand("select * from BefKon_Diverses where BefKonNr = " + _BefKonNr, _con).ExecuteReader();
                while(reader.Read())
                {
                    B_FU.Checked = Convert.ToBoolean(reader[1].ToString());
                    B_Gefaehrdungsmeldung.Checked = Convert.ToBoolean(reader[2].ToString());
                    B_Anzeige.Checked = Convert.ToBoolean(reader[3].ToString());
                }
                reader.Close();

                #endregion
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
                this.Close();
            }
        }

        private void newBefKonNr()
        {
            // BefKonNr um eins erhöhen und in den Label schreiben
            SqlCommand cmd = new SqlCommand("select max(BefKonNr) from BefKon", _con);
            var temp = "";
            try
            {
                temp = (Convert.ToInt32(cmd.ExecuteScalar()) + 1).ToString();
            }
            catch (Exception ex) { temp = "1"; }
            lblBefKonNr.Text = temp;
            _BefKonNr = int.Parse(temp);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPERS.Rows.Count != 0)
                {
                    var NR = dgvPERS.SelectedRows[0].Cells[0].Value.ToString();
                    //var adpt = new SqlDataAdapter("select * from Person where PNr = @PNr and PTyp = @PTyp", _con);
                    //var adaptPers = _rep.AdaptPerson;
                    //adpt.SelectCommand.Parameters.Add("@PNr", SqlDbType.VarChar, 8, "PNr").Value = NR;
                    //adpt.SelectCommand.Parameters.Add("@PTyp", SqlDbType.VarChar, 8, "PTyp").Value = dgvPERS.SelectedRows[0].Cells[1].Value;

                    var dt = new DataTable();
                    _rep.GetAdaptPerson(NR, dgvPERS.SelectedRows[0].Cells[1].Value.ToString()).Fill(dt);

                    BindingSource bs = new BindingSource();
                    bs.DataSource = dt;

                    Client c = new Client(bs, _con, "Client", ref dt);
                    if (c.ShowDialog() == DialogResult.OK)
                    {
                        dgvPERS.Rows.RemoveAt(dgvPERS.SelectedRows[0].Index);
                        DgvPersInsert(c.PNr,c.PTyp);
                    }
                }
                else
                {
                    MessageBox.Show("Bitte wählen Sie eine Person aus der Tabelle aus!");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }

        }

        private void btnData_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Multiselect = true;
                ofd.Title = "Dateien anfügen";
                ofd.InitialDirectory = @"Desktop";

                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    var selectedFiles = ofd.FileNames;

                    var temp = new List<string>();
                    foreach (DataGridViewRow dgvFilesRow in dgvFiles.Rows)
                        temp.Add(dgvFilesRow.Cells[1].Value.ToString());
                    
                    var error = "Folgende Dateien wurden schon einmal hinzugefügt: " + Environment.NewLine + Environment.NewLine;
                    var showError = false;

                    foreach (var selectedFile in selectedFiles)
                    {
                        var fileName = selectedFile.Substring(selectedFile.LastIndexOf('\\') + 1,
                            selectedFile.Length - selectedFile.LastIndexOf('\\') - 1);

                        if (!temp.Contains(fileName))
                            dgvFiles.Rows.Add(selectedFile, fileName, "");
                        else
                        {
                            error += fileName + Environment.NewLine;
                            showError = true;
                        }
                    }
                    if (showError)
                        MessageBox.Show(
                            error + Environment.NewLine + "Oben genannte Dateien wurden nicht hinzugefügt!",Resources.MessageBoxHeaderHinweis);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                Textfeld tf = new Textfeld();
                if (tf.ShowDialog() == DialogResult.OK)
                {
                    _boxes.Add(new ownTextbox(tf.Eingabe));
                    tabAnamnese.Controls.Add(_boxes[_boxes.Count - 1]);
                    _boxes[_boxes.Count - 1].Location = new Point(_boxes[_boxes.Count - 2].Location.X,
                    _boxes[_boxes.Count - 2].Location.Y + (_boxes[_boxes.Count - 2].Location.Y - _boxes[_boxes.Count - 3].Location.Y));
                    _boxes[_boxes.Count - 1].Size = _boxes[_boxes.Count - 2].Size;
                    _boxes[_boxes.Count - 1].TabIndex = _boxes[_boxes.Count - 2].TabIndex + 1;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void MakeSetting()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("insert into BefKon_Diverses values(@BefKonNr, @F, @G, @A)", _con);
                cmd.Parameters.Add("@BefKonNr", SqlDbType.Int, 5000, "BefKonNr").Value = _BefKonNr;
                cmd.Parameters.Add("@F", SqlDbType.Bit, 1, "FU").Value = B_FU.Checked;
                cmd.Parameters.Add("@G", SqlDbType.Bit, 1, "Gefährdungsmeldung").Value = B_Gefaehrdungsmeldung.Checked;
                cmd.Parameters.Add("@A", SqlDbType.Bit, 1, "Anzeige").Value = B_Anzeige.Checked;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        public void Save()
        {
            try
            {
                if(_typ == "new")
                newBefKonNr();
                SaveText();
                SaveFiles();

                SqlCommand cmdTexte = new SqlCommand(
                    "insert into BefKon_txt values(@befkonnr, @TxtNr, @texttyp, @text)",
                    _con);
                SqlCommand cmdInst =
                    new SqlCommand("insert into BefKon_Bez_Institution values(@BefKonNr, @BezugNr, @BezugTyp)", _con);
                SqlCommand cmdPers =
                    new SqlCommand("insert into BefKon_Bez_Person values(@BefKonNr, @BezugNr, @BezugTyp)",
                        _con);


                new SqlCommand("delete from BefKon_Diverses where BefKonNr = " + _BefKonNr, _con).ExecuteNonQuery();
                new SqlCommand("delete from BefKon_txt where BefKonNr =" + _BefKonNr, _con).ExecuteNonQuery();
                new SqlCommand("delete from BefKon_Bez_Person where BefKonNr =" + _BefKonNr, _con).ExecuteNonQuery();
                new SqlCommand("delete from BefKon_Bez_Institution where BefKonNr =" + _BefKonNr, _con).ExecuteNonQuery();

                MakeSetting();

                foreach (DataGridViewRow row in dgvINST.Rows)
                {
                    cmdInst.Parameters.Clear();

                    cmdInst.Parameters.Add("@BefKonNr", SqlDbType.Int, 8, "BefKonNr").Value = lblBefKonNr.Text;
                    cmdInst.Parameters.Add("@BezugNr", SqlDbType.VarChar, 8, "Bezug_Nr").Value = row.Cells[0].Value;
                    cmdInst.Parameters.Add("@BezugTyp", SqlDbType.VarChar, 4, "Bezug_Typ").Value =
                        new SqlCommand("select ITyp from Institution where INr =" + row.Cells[0].Value.ToString(), _con)
                            .ExecuteScalar();
                    cmdInst.ExecuteNonQuery();
                }

                foreach (DataGridViewRow row in dgvPERS.Rows)
                {
                    cmdPers.Parameters.Clear();

                    cmdPers.Parameters.Add("@BefKonNr", SqlDbType.Int, 8, "BefKonNr").Value = lblBefKonNr.Text;
                    cmdPers.Parameters.Add("@BezugNr", SqlDbType.VarChar, 8, "Bezug_Nr").Value =
                        row.Cells[0].Value;
                    cmdPers.Parameters.Add("@BezugTyp", SqlDbType.VarChar, 4, "Bezug_Typ").Value =
                        new SqlCommand(
                            "select PTyp from Person where PNr ='" + row.Cells[0].Value.ToString() + "'",
                            _con).ExecuteScalar();
                    cmdPers.ExecuteNonQuery();
                }

                int count = 0;

                foreach (ownTextbox t in _boxes)
                {
                    cmdTexte.Parameters.Clear();

                    cmdTexte.Parameters.Add("@befkonnr", SqlDbType.Int, 8, "BefKonNr").Value = lblBefKonNr.Text;
                    cmdTexte.Parameters.Add("@TxtNr", SqlDbType.Int, 8, "TxtNr").Value = count;
                    cmdTexte.Parameters.Add("@texttyp", SqlDbType.VarChar, 255, "BefKonTxtTyp").Value = t.editTitle;
                    cmdTexte.Parameters.Add("@text", SqlDbType.VarChar, 500000, "Text").Value = t.Text;
                    cmdTexte.ExecuteNonQuery();

                    count++;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void SaveText()
        {
            try
            {
              string fehler =
                    "Es gibt einen oder mehrere Probleme beim Speichern. Bitte lösen Sie diese bevor Sie fortfahren: " +
                    Environment.NewLine;
                if (lblFallfuehrender.Text == "")
                    fehler += "\t Bitte geben Sie einen Fallführenden Mitarbeiter an!" + Environment.NewLine;

                if (_typ == "new")
                {
                    var InsertCommand =
                        new SqlCommand(
                            "insert into BefKon values(@BefKonNr, @PNr, @Ptyp,nullif(@BeginDate,''),nullif(@EndDate,''), nullif(@MA1,''), nullif(@MA2,''), nullif(@Anmeld,''), nullif(@Ueberw,''),nullif(@Typ_Ueberw,''), nullif(@Grund,''), nullif(@IKategorie,''), nullif(@Auftrag,''), nullif(@Symptomatik,''), nullif(@Ziel,''),nullif(@Dia,''), nullif(@Medic,''), nullif(@Anmerk,''))",
                            _con);
                    if (_uberweiser == null)
                    {
                        _uberweiser = "";
                        _uberweiserTyp = "";
                    }
                    InsertCommand.Parameters.Add("@BefKonNr", SqlDbType.Int, 10000).Value = new SqlCommand("select ISNULL(max(BefKonNr)+1,1) from BefKon", _con).ExecuteScalar().ToString();
                    InsertCommand.Parameters.Add("@PNr", SqlDbType.VarChar, 8).Value = _PNr;
                    InsertCommand.Parameters.Add("@Ptyp", SqlDbType.VarChar, 4).Value = "IK";


                    if (dtpEnde.Format == DateTimePickerFormat.Custom)
                        InsertCommand.Parameters.AddWithValue("@EndDate", "");
                    else
                        InsertCommand.Parameters.AddWithValue("@EndDate", dtpEnde.Value);

                    if (dtpBeginn.Format == DateTimePickerFormat.Custom)
                        InsertCommand.Parameters.AddWithValue("@BeginDate", "");
                    else
                        InsertCommand.Parameters.AddWithValue("@BeginDate", dtpBeginn.Value);

                    InsertCommand.Parameters.AddWithValue("@Anmeld", dtpAnmeldedatum.Value);
                    InsertCommand.Parameters.AddWithValue("@MA1", lblFallfuehrender.Text);
                    InsertCommand.Parameters.AddWithValue("@MA2", lblFallfuehrend2.Text);
                    InsertCommand.Parameters.AddWithValue("@Ueberw", _uberweiser);
                    InsertCommand.Parameters.AddWithValue("@Typ_Ueberw", _uberweiserTyp);
                    InsertCommand.Parameters.AddWithValue("@Grund", lblBehandlungsanlass.Text);
                    InsertCommand.Parameters.AddWithValue("@IKategorie", lblInterventionskategorie.Text);
                    InsertCommand.Parameters.AddWithValue("@Auftrag", allgAuftrag.Text);
                    InsertCommand.Parameters.AddWithValue("@Symptomatik", allgSymptomatik.Text);
                    InsertCommand.Parameters.AddWithValue("@Ziel", allgZiel.Text);
                    InsertCommand.Parameters.AddWithValue("@Dia", allgDiagnose.Text);
                    InsertCommand.Parameters.AddWithValue("@Medic", allgMedikation.Text);
                    InsertCommand.Parameters.AddWithValue("@Anmerk", txtAnmerkungen2.Text);

                    InsertCommand.ExecuteNonQuery();
                }
                else
                {
                    var UpdateCommand =
                        new SqlCommand(
                            "update BefKon set Datum_Beginn=nullif(@BeginDate,''), Datum_Ende=nullif(@EndDate,''),Fallfuehrender=nullif(@MA1,''), Fallfuehrender2=nullif(@MA2,''),   Anmeldedatum = nullif(@Anmeld,''), Ueberweiser=nullif(@Ueberw,''), ITyp_Ueberweiser=nullif(@Typ_Ueberw,''), Behandlungsanlass = nullif(@Grund,''), Interventionskategorie=nullif(@IKategorie,''), Auftrag=nullif(@Auftrag,''), Symptomatik=nullif(@Symptomatik,''), Ziel=nullif(@Ziel,''), Diagnose=nullif(@Dia,''), Medikation=nullif(@Medic,''), Anmerkungen =nullif(@Anmerk,'')  where BefKonNr = @BefKonNr",
                            _con);

                    if (_uberweiser == null)
                    {
                        _uberweiser = "";
                        _uberweiserTyp = "";
                    }
                    UpdateCommand.Parameters.Add("@BefKonNr", SqlDbType.Int, 10000).Value = _BefKonNr;
                    UpdateCommand.Parameters.Add("@PNr", SqlDbType.VarChar, 8).Value = _PNr;
                    UpdateCommand.Parameters.Add("@Ptyp", SqlDbType.VarChar, 4).Value = "IK";                    

                    if (dtpEnde.Format == DateTimePickerFormat.Custom)
                        UpdateCommand.Parameters.AddWithValue("@EndDate", "");
                    else
                        UpdateCommand.Parameters.AddWithValue("@EndDate", dtpEnde.Value);

                    if (dtpBeginn.Format == DateTimePickerFormat.Custom)
                        UpdateCommand.Parameters.AddWithValue("@BeginDate", "");
                    else
                        UpdateCommand.Parameters.AddWithValue("@BeginDate", dtpBeginn.Value);

                    UpdateCommand.Parameters.AddWithValue("@Anmeld", dtpAnmeldedatum.Value);
                    UpdateCommand.Parameters.AddWithValue("@MA1", lblFallfuehrender.Text);
                    UpdateCommand.Parameters.AddWithValue("@MA2", lblFallfuehrend2.Text);
                    UpdateCommand.Parameters.AddWithValue("@Ueberw", _uberweiser);
                    UpdateCommand.Parameters.AddWithValue("@Typ_Ueberw", _uberweiserTyp);
                    UpdateCommand.Parameters.AddWithValue("@Grund", lblBehandlungsanlass.Text);
                    UpdateCommand.Parameters.AddWithValue("@IKategorie", lblInterventionskategorie.Text);
                    UpdateCommand.Parameters.AddWithValue("@Auftrag", allgAuftrag.Text);
                    UpdateCommand.Parameters.AddWithValue("@Symptomatik", allgSymptomatik.Text);
                    UpdateCommand.Parameters.AddWithValue("@Ziel", allgZiel.Text);
                    UpdateCommand.Parameters.AddWithValue("@Dia", allgDiagnose.Text);
                    UpdateCommand.Parameters.AddWithValue("@Medic", allgMedikation.Text);
                    UpdateCommand.Parameters.AddWithValue("@Anmerk", txtAnmerkungen2.Text);

                    UpdateCommand.ExecuteNonQuery();
                }
               
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void SaveFiles()
        {
            try
            {
               var folderPath = Path.Combine(_conf.getString("BefKonFolderPath"), _PNr, "BefKonNr" + _BefKonNr);           //Zuerst den Pfad zusammenstellen indem die Dateien später gespeichert werden
                                                                                                                            //der Basispfad wird aus der COnfiguration.xml entnommen, dann wird ein Ordner mit der Nummer des Klienten hinzugefügt und in dem wird zu jedem BefKon ein eigener Ordner erstellt
                if (!Directory.Exists(folderPath))                                                                          //hier wird überprüft ob der folderPath schon existiert
                    Directory.CreateDirectory(folderPath);                                                                  //er existiert nicht, also wird er erstellt
                else
                {
                    var command = new SqlCommand("Delete from BefKon_Dateien where BefKonNr = @nr", _con);                         //er existiert, also werden alle Dateien die in der Datenbank gespeichert werden gelöscht
                    command.Parameters.AddWithValue("@nr", _BefKonNr);
                    command.ExecuteNonQuery();
                    
                }

                var fileNumber = 1;
                foreach (DataGridViewRow dgvDataRow in dgvFiles.Rows)
                {
                    string sourcePath = dgvDataRow.Cells[0].Value.ToString();
                    var destinationPath = Path.Combine(folderPath, dgvDataRow.Cells[1].Value.ToString());

                    if (sourcePath != destinationPath)                                                                       //hier wird überprüft ob die Datei schon im Ordner ist, trifft das zu, wird die Datei nur mehr in der Datenbank vermerkt
                    {
                        File.Copy(sourcePath, destinationPath, true);                                                        //trifft das nicht zu, wird die Datei in den Ordner kopiert und dann in der Datenbank mit dem neuen Pfad vermerkt
                    }

                    InsertDateien(dgvDataRow, fileNumber++, destinationPath,dgvDataRow.Cells[1].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void InsertDateien(DataGridViewRow dgvDataRow, int fileNumber, string destinationPath, string fileName)
        {
            var command =
                new SqlCommand("insert into BefKon_Dateien values(@BefKonNr, @DateiNr, @DName, @DTyp, @Kommentar)",
                    _con);

            command.Parameters.Add("@BefKonNr", SqlDbType.Int, 8).Value = _BefKonNr;
            command.Parameters.Add("@DateiNr", SqlDbType.Int, 8).Value = fileNumber;
            command.Parameters.Add("@DName", SqlDbType.VarChar, 255).Value = destinationPath;
            command.Parameters.Add("@DTyp", SqlDbType.VarChar, 50).Value =
                fileName.Substring(fileName.LastIndexOf("."), fileName.Length - fileName.LastIndexOf("."));
            command.Parameters.Add("@Kommentar", SqlDbType.VarChar, 50000).Value =
                dgvDataRow.Cells[2].Value.ToString();

            command.ExecuteNonQuery();
            
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                
                Save();
                _typ = "old";
                MessageBox.Show("Der Datensatz wurde erfolgreich gespeichert!", Resources.MessageBoxHeaderHinweis);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();    
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var dtClient = new DataTable();                
                _rep.AdaptClient.Fill(dtClient);

                var bs = new BindingSource();
                bs.DataSource = dtClient;

                bs.AddNew();

                Client client = new Client(bs, _con, "Angehöriger",ref dtClient);
                client.ShowDialog();

                if (client.DialogResult != DialogResult.Cancel)
                {
                    if (MessageBox.Show("Wollen Sie diese Person gleich hinzufügen?", Resources.MessageBoxHeaderHinweis, MessageBoxButtons.YesNo) == DialogResult.Yes)
                        DgvPersInsert(client.PNr, client.PTyp);
                }
                client.Dispose();    
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);

            }
        }


        private void DgvPersInsert(string nummer, string typ)
        {
            try
            {
             
                bool temp = false;
                foreach (DataGridViewRow dgvPersRow in dgvPERS.Rows)
                {
                    if (dgvPersRow.Cells[0].Value.ToString() == nummer && dgvPersRow.Cells[2].Value.ToString() == typ)
                        temp = true;
                }
                if (temp)
                    MessageBox.Show("Die von Ihnen ausgewählte Person wurde bereits hinzugefügt!",
                        Resources.MessageBoxHeaderHinweis);
                else
                {
                    var command =
                        new SqlCommand(
                            "Select PNr, PTyp, Vorname, Nachname, Geburtsdatum from Person where PNr = '" + nummer + "' and PTyp = '" + typ + "'",
                            _con).ExecuteReader();

                    try
                    {
                        command.Read();
                        dgvPERS.Rows.Add(command[0].ToString(), command[1].ToString(), command[2].ToString(),
                            command[3].ToString(),
                            command[4].ToString());
                    }
                    finally
                    {
                        command.Close();
                    }
                }
            }catch(Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void DgvInstInsert(string nummer)
        {
           bool temp = false;
            foreach (DataGridViewRow dgvINSTRow in dgvINST.Rows)
            {
                if (dgvINSTRow.Cells[0].Value.ToString() == nummer)
                    temp = true;
            }
            if (temp)
                MessageBox.Show("Die von Ihnen ausgewählte Institution wurde bereits hinzugefügt!",
                    Resources.MessageBoxHeaderHinweis);
            else
            {
                reader =
                    new SqlCommand(
                        "select Bezeichnung, b.Kategorie, Ansprechperson from Institution i, ITyp_Bezeichnung b where i.ITyp = b.ITyp AND INr = '" +
                        nummer + "'",
                        _con).ExecuteReader();
                try
                {
                    reader.Read();
                    dgvINST.Rows.Add(nummer, reader[0].ToString(), reader[1].ToString(), reader[2].ToString());
                }
                finally
                {
                    reader.Close();
                }
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                var adapter = _rep.AdaptInst;

                var dtInst = new DataTable();
                adapter.Fill(dtInst);

                var bsInst = new BindingSource();
                bsInst.DataSource = dtInst;

                bsInst.AddNew();

                Institution inst = new Institution(_con, bsInst,ref dtInst);
                inst.ShowDialog();
                

                if (MessageBox.Show("Wollen Sie diese Institution gleich hinzufügen?", Resources.MessageBoxHeaderHinweis, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    adapter.Update(dtInst);
                    DgvInstInsert(inst.INr);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                var command = new SqlCommand("Select * from Person where PTyp != 'IK' and PTyp != 'Pb' and PTyp != 'TB' and PTyp != 'ImF' and PTyp != 'SB'", _con).ExecuteReader();
                               
                Auswahl a = new Auswahl(_con, command, "Client");
                if (a.ShowDialog() == DialogResult.OK)
                    DgvPersInsert(a.GetId, a.GetType);
                command.Close();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void btnUpdateIns_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvINST.Rows.Count != 0)
                {
                    string key = dgvINST.SelectedRows[0].Cells[0].Value as string;

                    var adpt = new SqlDataAdapter("select * from Institution where INr = @INr", _con);
                    var adaptInst = _rep.AdaptInst;

                    adpt.SelectCommand.Parameters.Add("@INr", SqlDbType.VarChar, 8, "INr").Value = key;

                    var dt = new DataTable();
                    adpt.Fill(dt);

                    BindingSource bs = new BindingSource();
                    bs.DataSource = dt;

                    Institution i = new Institution(_con, bs, ref dt);
                    i.ShowDialog();

                    adaptInst.Update(dt);

                    dgvINST.Rows.RemoveAt(dgvINST.SelectedRows[0].Index);

                    DgvInstInsert(key);
                }
                else
                    MessageBox.Show("Bitte wählen Sie eine Person aus der Tabelle aus!");

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void btnAddIns_Click(object sender, EventArgs e)
        {
            try
            {
                var adpt = _rep.AdaptInst;

                var dt = new DataTable();
                adpt.Fill(dt);

                BindingSource bs = new BindingSource();
                bs.DataSource = dt;

                Auswahl a = new Auswahl(bs, _con, "INST");
                if (a.ShowDialog() == DialogResult.OK)
                    DgvInstInsert(a.GetId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                var adpt = _rep.AdaptInst;
                
                var _dt = new DataTable();
                adpt.Fill(_dt);

                _bs = new BindingSource();
                _bs.DataSource = _dt;

                Auswahl a = new Auswahl(_bs, _con, "INST");
                a.ShowDialog();

                if (a.DialogResult != DialogResult.Cancel)
                {
                    lblUberweiser.Text = _uberweiser = a.GetId;
                    _uberweiserTyp = a.GetType;
                    txtUeberweiser.Text =
                        new SqlCommand("Select Bezeichnung from Institution where INr = " + a.GetId + " AND ITyp ='" + a.GetType + "'", _con).ExecuteScalar()
                            .ToString();
                }
                a.Dispose();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }
      
        private void BefKon_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (e.CloseReason == CloseReason.UserClosing)
                {
                    DialogResult box = MessageBox.Show("Wollen Sie die Änderungen speichern?",
                        Resources.MessageBoxHeaderHinweis, MessageBoxButtons.YesNoCancel);

                    if (box == DialogResult.Yes)
                    {
                        Save();
                        DialogResult = DialogResult.OK;
                    }
                    else if(box == DialogResult.No)
                        DialogResult = DialogResult.OK;
                    else
                        e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }

        }

        private void dgvData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void öffnenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(dgvFiles.Rows[dgvFiles.SelectedCells[0].RowIndex].Cells[0].Value.ToString()))
            {
                Process.Start(dgvFiles.Rows[dgvFiles.SelectedCells[0].RowIndex].Cells[0].Value.ToString());
            }
        }

        private void löschenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var folderPath = Path.Combine(_conf.getString("BefKonFolderPath"), _PNr, "BefKonNr" + _BefKonNr);

            if (Directory.Exists(folderPath))                                                                                           //in dieser Methode wird überprüft ob die Datei die gelöscht werden soll bereits im Ordner ist oder nicht
            {                                                                                                                           //ist sie im Ordner, wird gefragt ob man sie wirklich löschen will, wenn ja wird die ausgewählte Datei aus dem Ordner und aus der dgv gelöscht
                if (Path.GetDirectoryName(dgvFiles.Rows[dgvFiles.SelectedCells[0].RowIndex].Cells[0].Value.ToString()) ==
                    folderPath)
                {
                    if (MessageBox.Show(
                            "Wollen Sie diese Datei wirklich entgültig von diesem Befassungskontext entfernen?",
                            Resources.MessageBoxHeaderHinweis, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        File.Delete(dgvFiles.Rows[dgvFiles.SelectedCells[0].RowIndex].Cells[0].Value.ToString());
                        dgvFiles.Rows.RemoveAt(dgvFiles.SelectedRows[0].Index);
                    }
                }
                else
                dgvFiles.Rows.RemoveAt(dgvFiles.SelectedRows[0].Index);
            }
            else
            dgvFiles.Rows.RemoveAt(dgvFiles.SelectedRows[0].Index);
            
        }

        private void dtpEnde_ValueChanged(object sender, EventArgs e)
        {
            dtpEnde.Format = DateTimePickerFormat.Long;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            dtpEnde.Format = DateTimePickerFormat.Custom;
            dtpEnde.CustomFormat = " ";
            
        }

        private void dtpBeginn_ValueChanged(object sender, EventArgs e)
        {
            dtpBeginn.Format = DateTimePickerFormat.Long;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            dtpBeginn.Format = DateTimePickerFormat.Custom;
            dtpBeginn.CustomFormat = " ";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Load(new SqlCommand("select * from Behandlungsanlaesse",_con).ExecuteReader());
            BindingSource bs = new BindingSource();
            bs.DataSource = dt;
            Auswahl a = new Auswahl(bs, _con);
            if (a.ShowDialog() == DialogResult.OK)
            {
                lblBehandlungsanlass.Text = a.GetId;
                txtBehandlungsanlass.Text = a.GetType;
            }
            a.Dispose();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Load(new SqlCommand("select * from Interventionskategorien", _con).ExecuteReader());
            BindingSource bs = new BindingSource();
            bs.DataSource = dt;
            Auswahl a = new Auswahl(bs, _con);
            if (a.ShowDialog() == DialogResult.OK)
            {
                lblInterventionskategorie.Text = a.GetId;
                txtInterventionskategorie.Text = a.GetType;
            }
            a.Dispose();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Load(new SqlCommand("select * from Mitarbeiter where MNr > 0", _con).ExecuteReader());
            BindingSource bs = new BindingSource();
            bs.DataSource = dt;
            Auswahl a = new Auswahl(bs, _con, "Mitarbeiter");
            if (a.ShowDialog() == DialogResult.OK)
            {
                lblFallfuehrender.Text = a.GetId;
                txtFall1.Text = a.GetType;
            }
            a.Dispose();
        }

        private void button8_Click_1(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Load(new SqlCommand("select * from Mitarbeiter where MNr > 0", _con).ExecuteReader());
            BindingSource bs = new BindingSource();
            bs.DataSource = dt;
            Auswahl a = new Auswahl(bs, _con, "Mitarbeiter");
            if (a.ShowDialog() == DialogResult.OK)
            {
                lblFallfuehrend2.Text = a.GetId;
                txtFall2.Text = a.GetType;
            }
            a.Dispose();
        }
    }
}
