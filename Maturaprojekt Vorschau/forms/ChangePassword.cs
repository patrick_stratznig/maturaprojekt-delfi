﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tools;
using Maturaprojekt_Vorschau.Properties;
using NLog;
using System.Data.SqlClient;
using Excel = Microsoft.Office.Interop.Excel;

namespace Maturaprojekt_Vorschau
{
    public partial class ChangePassword : Form
    {
        private string _MNr;
        private SqlConnection _con;
        private Logger _logger;

        public ChangePassword()
        {
            InitializeComponent();
        }
        public ChangePassword(string MNr, SqlConnection con):this()
        {
            _MNr = MNr;
            _con = con;
        }

        private void ChangePassword_Load(object sender, EventArgs e)
        {
            try
            {
                var conf = new Configuration(Resources.ConfigurationPath);
                this.Font = new Font("Microsoft Sans Serif", conf.getInt("text-size"));

                _logger = LogManager.GetCurrentClassLogger();

                if (_con.State == ConnectionState.Closed)
                    _con.Open();

                if (_MNr != null)
                {
                    #region Labels füllen
                    var reader = new SqlCommand("Select m.Nachname, m.Vorname, m.Geburtsdatum, u.Username from Mitarbeiter m join user_login u on(m.MNr = u.MNr) where m.MNr = " + _MNr, _con).ExecuteReader();

                    while (reader.Read())
                    {
                        lblMNr.Text = _MNr;
                        lblNachname.Text = reader[0].ToString();
                        lblVorname.Text = reader[1].ToString();
                        lblGeburt.Text = reader[2].ToString();
                        txtUser.Text = reader[3].ToString();
                    }

                    reader.Close();
                    #endregion

                }
            }catch(Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);             
            }
        }

        private void textBox2_Leave(object sender, EventArgs e)
        {
            if (txtPwd.Text != txtPwdRepeat.Text)
                lblWarning.Visible = true;
            else
                lblWarning.Visible = false;
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPwd.Text != txtPwdRepeat.Text)
                    MessageBox.Show(lblWarning.Text, Resources.MessageBoxHeaderHinweis, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    new SqlCommand("ALTER LOGIN [" + txtUser.Text + "] WITH PASSWORD=N'" + txtPwd.Text + "'", _con).ExecuteNonQuery();
                    MessageBox.Show("Das Passwort wurde erfolgreich geändert!", Resources.MessageBoxHeaderHinweis);

                    if(MessageBox.Show("Wollen Sie das neue Passwort ausdrucken?",Resources.MessageBoxHeaderHinweis,MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        var ExcelApp = new Excel.Application();
                        ExcelApp.Visible = true;
                        ExcelApp.WindowState = Excel.XlWindowState.xlMaximized;
                        ExcelApp.Workbooks.Add();
                        Excel._Worksheet workSheet = (Excel.Worksheet)ExcelApp.ActiveSheet;   

                        workSheet.Name = "neues Passwort";
                        workSheet.Cells[1, "A"] = "Benutzername:";
                        workSheet.Cells[2, "A"] = "Passwort:";

                        workSheet.Cells[1, "B"] = txtUser.Text;
                        workSheet.Cells[2, "B"] = txtPwd.Text;
                    }
                }
            }catch(Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ChangePassword_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                var box = MessageBox.Show("Wollen Sie wirklich abbrechen?",
                Resources.MessageBoxHeaderHinweis, MessageBoxButtons.YesNo);

                if (box == DialogResult.Yes)                                 
                    DialogResult = DialogResult.OK;                
                else                
                    e.Cancel = true;
            }
        }
    }
}
