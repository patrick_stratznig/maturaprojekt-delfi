﻿namespace Maturaprojekt_Vorschau
{
    partial class Client
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtPNr = new System.Windows.Forms.TextBox();
            this.txtStraße = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtLcd = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtPLZ = new System.Windows.Forms.TextBox();
            this.txtHnr = new System.Windows.Forms.TextBox();
            this.txtOrt = new System.Windows.Forms.TextBox();
            this.cmdSave = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTelNr = new System.Windows.Forms.TextBox();
            this.cBGeschlecht = new System.Windows.Forms.ComboBox();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.lblPTyp = new System.Windows.Forms.Label();
            this.cbPtyp = new System.Windows.Forms.ComboBox();
            this.lblTyp = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cBObsorge = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtBeruf = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cBFamilienstand = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cmdSearch = new System.Windows.Forms.Button();
            this.mTxtGebd = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.txtStaatsbuergerschaft = new System.Windows.Forms.TextBox();
            this.txtReligion = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblWarnung = new System.Windows.Forms.Label();
            this.txtAnmerkungen = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nummer";
            // 
            // txtPNr
            // 
            this.txtPNr.Location = new System.Drawing.Point(97, 17);
            this.txtPNr.Margin = new System.Windows.Forms.Padding(2);
            this.txtPNr.Name = "txtPNr";
            this.txtPNr.ReadOnly = true;
            this.txtPNr.Size = new System.Drawing.Size(109, 24);
            this.txtPNr.TabIndex = 100;
            this.txtPNr.TabStop = false;
            // 
            // txtStraße
            // 
            this.txtStraße.Location = new System.Drawing.Point(181, 313);
            this.txtStraße.Margin = new System.Windows.Forms.Padding(2);
            this.txtStraße.Name = "txtStraße";
            this.txtStraße.Size = new System.Drawing.Size(206, 24);
            this.txtStraße.TabIndex = 7;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(27, 316);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(114, 18);
            this.label14.TabIndex = 26;
            this.label14.Text = "Straße/Nummer";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(27, 274);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 18);
            this.label10.TabIndex = 34;
            this.label10.Text = "Land";
            // 
            // txtLcd
            // 
            this.txtLcd.Location = new System.Drawing.Point(181, 271);
            this.txtLcd.Margin = new System.Windows.Forms.Padding(2);
            this.txtLcd.MaxLength = 2;
            this.txtLcd.Name = "txtLcd";
            this.txtLcd.ReadOnly = true;
            this.txtLcd.Size = new System.Drawing.Size(42, 24);
            this.txtLcd.TabIndex = 33;
            this.txtLcd.TabStop = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(27, 358);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(60, 18);
            this.label18.TabIndex = 32;
            this.label18.Text = "PLZ/Ort";
            // 
            // txtPLZ
            // 
            this.txtPLZ.Location = new System.Drawing.Point(181, 355);
            this.txtPLZ.Margin = new System.Windows.Forms.Padding(2);
            this.txtPLZ.Name = "txtPLZ";
            this.txtPLZ.Size = new System.Drawing.Size(74, 24);
            this.txtPLZ.TabIndex = 9;
            this.txtPLZ.Leave += new System.EventHandler(this.txtPLZ_Leave);
            // 
            // txtHnr
            // 
            this.txtHnr.Location = new System.Drawing.Point(391, 313);
            this.txtHnr.Margin = new System.Windows.Forms.Padding(2);
            this.txtHnr.Name = "txtHnr";
            this.txtHnr.Size = new System.Drawing.Size(57, 24);
            this.txtHnr.TabIndex = 8;
            // 
            // txtOrt
            // 
            this.txtOrt.Location = new System.Drawing.Point(259, 355);
            this.txtOrt.Margin = new System.Windows.Forms.Padding(2);
            this.txtOrt.Name = "txtOrt";
            this.txtOrt.Size = new System.Drawing.Size(189, 24);
            this.txtOrt.TabIndex = 10;
            this.txtOrt.Leave += new System.EventHandler(this.txtOrt_Leave);
            // 
            // cmdSave
            // 
            this.cmdSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSave.Location = new System.Drawing.Point(894, 523);
            this.cmdSave.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(101, 34);
            this.cmdSave.TabIndex = 20;
            this.cmdSave.Text = "Speichern";
            this.cmdSave.UseVisualStyleBackColor = true;
            this.cmdSave.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(514, 225);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 18);
            this.label5.TabIndex = 35;
            this.label5.Text = "Religion";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(27, 182);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 18);
            this.label9.TabIndex = 45;
            this.label9.Text = "Geschlecht";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 145);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 18);
            this.label4.TabIndex = 33;
            this.label4.Text = "Geburtsdatum";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(181, 104);
            this.txtFirstName.Margin = new System.Windows.Forms.Padding(2);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(173, 24);
            this.txtFirstName.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(27, 108);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(68, 18);
            this.label15.TabIndex = 28;
            this.label15.Text = "Vorname";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 219);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 18);
            this.label3.TabIndex = 31;
            this.label3.Text = "TelNr.";
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(181, 68);
            this.txtLastName.Margin = new System.Windows.Forms.Padding(2);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(173, 24);
            this.txtLastName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 71);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nachname";
            // 
            // txtTelNr
            // 
            this.txtTelNr.Location = new System.Drawing.Point(181, 214);
            this.txtTelNr.Margin = new System.Windows.Forms.Padding(2);
            this.txtTelNr.Name = "txtTelNr";
            this.txtTelNr.Size = new System.Drawing.Size(169, 24);
            this.txtTelNr.TabIndex = 5;
            // 
            // cBGeschlecht
            // 
            this.cBGeschlecht.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBGeschlecht.FormattingEnabled = true;
            this.cBGeschlecht.Items.AddRange(new object[] {
            "männlich",
            "weiblich"});
            this.cBGeschlecht.Location = new System.Drawing.Point(181, 176);
            this.cBGeschlecht.Margin = new System.Windows.Forms.Padding(2);
            this.cBGeschlecht.Name = "cBGeschlecht";
            this.cBGeschlecht.Size = new System.Drawing.Size(131, 26);
            this.cBGeschlecht.TabIndex = 4;
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(790, 523);
            this.cmdCancel.Margin = new System.Windows.Forms.Padding(2);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(100, 34);
            this.cmdCancel.TabIndex = 21;
            this.cmdCancel.Text = "Schließen";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // lblPTyp
            // 
            this.lblPTyp.AutoSize = true;
            this.lblPTyp.Location = new System.Drawing.Point(224, 20);
            this.lblPTyp.Name = "lblPTyp";
            this.lblPTyp.Size = new System.Drawing.Size(32, 18);
            this.lblPTyp.TabIndex = 62;
            this.lblPTyp.Text = "Typ";
            // 
            // cbPtyp
            // 
            this.cbPtyp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPtyp.FormattingEnabled = true;
            this.cbPtyp.Location = new System.Drawing.Point(311, 17);
            this.cbPtyp.Name = "cbPtyp";
            this.cbPtyp.Size = new System.Drawing.Size(205, 26);
            this.cbPtyp.TabIndex = 1;
            this.cbPtyp.SelectedIndexChanged += new System.EventHandler(this.cbPtyp_SelectedIndexChanged);
            // 
            // lblTyp
            // 
            this.lblTyp.AutoSize = true;
            this.lblTyp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTyp.Location = new System.Drawing.Point(273, 20);
            this.lblTyp.Name = "lblTyp";
            this.lblTyp.Size = new System.Drawing.Size(23, 18);
            this.lblTyp.TabIndex = 64;
            this.lblTyp.Text = "IK";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(514, 183);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 18);
            this.label6.TabIndex = 37;
            this.label6.Text = "Obsorge";
            // 
            // cBObsorge
            // 
            this.cBObsorge.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBObsorge.FormattingEnabled = true;
            this.cBObsorge.Items.AddRange(new object[] {
            "KM",
            "KV",
            "KE gemeinsam",
            "Jugendamt",
            "KHJT",
            "andere"});
            this.cBObsorge.Location = new System.Drawing.Point(668, 179);
            this.cBObsorge.Margin = new System.Windows.Forms.Padding(2);
            this.cBObsorge.Name = "cBObsorge";
            this.cBObsorge.Size = new System.Drawing.Size(266, 26);
            this.cBObsorge.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(514, 145);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 18);
            this.label7.TabIndex = 40;
            this.label7.Text = "Beruf";
            // 
            // txtBeruf
            // 
            this.txtBeruf.Location = new System.Drawing.Point(668, 142);
            this.txtBeruf.Margin = new System.Windows.Forms.Padding(2);
            this.txtBeruf.Name = "txtBeruf";
            this.txtBeruf.Size = new System.Drawing.Size(266, 24);
            this.txtBeruf.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(514, 108);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 18);
            this.label8.TabIndex = 42;
            this.label8.Text = "Familienstand";
            // 
            // cBFamilienstand
            // 
            this.cBFamilienstand.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBFamilienstand.FormattingEnabled = true;
            this.cBFamilienstand.Items.AddRange(new object[] {
            "LD (ledig)",
            "VH (verheiratet)",
            "VW (verwitwet)",
            "GS (geschieden)",
            "EA (Ehe aufgehoben)",
            "LP (in eingetragener Lebenspartnerschaft)",
            "LV (durch Tod aufgelöste Lebenspartnerschaft)",
            "LA (aufgehobene Lebenspartnerschaft)",
            "LE (durch Todeserklaerung aufgelöste Lebenspartnerschaft)",
            "NB (nicht bekannt)"});
            this.cBFamilienstand.Location = new System.Drawing.Point(668, 105);
            this.cBFamilienstand.Margin = new System.Windows.Forms.Padding(2);
            this.cBFamilienstand.Name = "cBFamilienstand";
            this.cBFamilienstand.Size = new System.Drawing.Size(266, 26);
            this.cBFamilienstand.TabIndex = 12;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(514, 71);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(132, 18);
            this.label11.TabIndex = 49;
            this.label11.Text = "Staatsbürgerschaft";
            // 
            // cmdSearch
            // 
            this.cmdSearch.BackgroundImage = global::Maturaprojekt_Vorschau.Properties.Resources.search_icon;
            this.cmdSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cmdSearch.Location = new System.Drawing.Point(228, 266);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(36, 34);
            this.cmdSearch.TabIndex = 6;
            this.cmdSearch.UseVisualStyleBackColor = true;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // mTxtGebd
            // 
            this.mTxtGebd.CustomFormat = " dd.MMMM, yyyy";
            this.mTxtGebd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.mTxtGebd.Location = new System.Drawing.Point(181, 140);
            this.mTxtGebd.Name = "mTxtGebd";
            this.mTxtGebd.Size = new System.Drawing.Size(131, 24);
            this.mTxtGebd.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::Maturaprojekt_Vorschau.Properties.Resources.search_icon;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.Location = new System.Drawing.Point(715, 63);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(36, 34);
            this.button1.TabIndex = 11;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // txtStaatsbuergerschaft
            // 
            this.txtStaatsbuergerschaft.Location = new System.Drawing.Point(668, 68);
            this.txtStaatsbuergerschaft.Margin = new System.Windows.Forms.Padding(2);
            this.txtStaatsbuergerschaft.MaxLength = 2;
            this.txtStaatsbuergerschaft.Name = "txtStaatsbuergerschaft";
            this.txtStaatsbuergerschaft.ReadOnly = true;
            this.txtStaatsbuergerschaft.Size = new System.Drawing.Size(42, 24);
            this.txtStaatsbuergerschaft.TabIndex = 102;
            this.txtStaatsbuergerschaft.TabStop = false;
            // 
            // txtReligion
            // 
            this.txtReligion.FormattingEnabled = true;
            this.txtReligion.Location = new System.Drawing.Point(668, 222);
            this.txtReligion.Margin = new System.Windows.Forms.Padding(2);
            this.txtReligion.Name = "txtReligion";
            this.txtReligion.Size = new System.Drawing.Size(266, 26);
            this.txtReligion.TabIndex = 15;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblWarnung);
            this.groupBox1.Controls.Add(this.txtAnmerkungen);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(30, 403);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(755, 157);
            this.groupBox1.TabIndex = 103;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Anmerkungen";
            // 
            // lblWarnung
            // 
            this.lblWarnung.AutoSize = true;
            this.lblWarnung.ForeColor = System.Drawing.Color.Red;
            this.lblWarnung.Location = new System.Drawing.Point(296, 8);
            this.lblWarnung.Name = "lblWarnung";
            this.lblWarnung.Size = new System.Drawing.Size(0, 18);
            this.lblWarnung.TabIndex = 79;
            // 
            // txtAnmerkungen
            // 
            this.txtAnmerkungen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAnmerkungen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnmerkungen.Location = new System.Drawing.Point(3, 20);
            this.txtAnmerkungen.Multiline = true;
            this.txtAnmerkungen.Name = "txtAnmerkungen";
            this.txtAnmerkungen.Size = new System.Drawing.Size(749, 134);
            this.txtAnmerkungen.TabIndex = 95;
            // 
            // Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AntiqueWhite;
            this.CancelButton = this.cmdCancel;
            this.ClientSize = new System.Drawing.Size(1006, 572);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtReligion);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtStaatsbuergerschaft);
            this.Controls.Add(this.mTxtGebd);
            this.Controls.Add(this.cmdSearch);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtLcd);
            this.Controls.Add(this.txtTelNr);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtPLZ);
            this.Controls.Add(this.cBFamilienstand);
            this.Controls.Add(this.txtOrt);
            this.Controls.Add(this.lblTyp);
            this.Controls.Add(this.txtHnr);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cBGeschlecht);
            this.Controls.Add(this.txtBeruf);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtStraße);
            this.Controls.Add(this.cbPtyp);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cBObsorge);
            this.Controls.Add(this.lblPTyp);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtLastName);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtFirstName);
            this.Controls.Add(this.cmdSave);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtPNr);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Client";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "neuen Angehörigen erstellen";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Client_FormClosing);
            this.Load += new System.EventHandler(this.new_Client_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPNr;
        private System.Windows.Forms.TextBox txtStraße;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtPLZ;
        private System.Windows.Forms.TextBox txtHnr;
        private System.Windows.Forms.TextBox txtOrt;
        private System.Windows.Forms.Button cmdSave;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.ComboBox cBGeschlecht;
        private System.Windows.Forms.TextBox txtTelNr;
        private System.Windows.Forms.Label lblPTyp;
        private System.Windows.Forms.ComboBox cbPtyp;
        private System.Windows.Forms.Label lblTyp;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtLcd;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cBObsorge;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtBeruf;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cBFamilienstand;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button cmdSearch;
        private System.Windows.Forms.DateTimePicker mTxtGebd;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtStaatsbuergerschaft;
        private System.Windows.Forms.ComboBox txtReligion;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblWarnung;
        private System.Windows.Forms.TextBox txtAnmerkungen;
    }
}