﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using Maturaprojekt_Vorschau.Properties;
using Tools;
using NLog;


namespace Maturaprojekt_Vorschau
{
    public partial class Client : Form
    {
        #region Variablen

        private readonly MethodRepository _adapt;
        private readonly BindingSource _bs;
        private readonly SqlConnection _con;
        private readonly string _typ;
        private List<NumberString> typ;
        private Configuration _conf;
        private DataTable _dt;
        private Logger _logger;
        private bool _check = true;
        private string _state;
        

        #endregion
        
        public Client(BindingSource bs, SqlConnection con, string typ, ref DataTable dt)
        {
            _typ = typ;
            _bs = bs;
            _con = con;
            _dt = dt;
            _adapt = new MethodRepository(_con);

            InitializeComponent();
        }

        public string PNr => txtPNr.Text;
        public string PTyp => lblTyp.Text;

        private void new_Client_Load(object sender, EventArgs e)
        {
            try
            {
                _logger = LogManager.GetCurrentClassLogger();

                _conf = new Configuration(Resources.ConfigurationPath);
                Font = new Font("Microsoft Sans Serif", _conf.getInt("text-size"));

                #region DataBindings

                txtPNr.DataBindings.Add("Text", _bs, "PNr");
                lblTyp.DataBindings.Add("Text", _bs, "PTyp");
                txtLastName.DataBindings.Add("Text", _bs, "Nachname");
                txtFirstName.DataBindings.Add("Text", _bs, "Vorname");
                mTxtGebd.DataBindings.Add("Text", _bs, "Geburtsdatum");
                cBGeschlecht.DataBindings.Add("Text", _bs, "Geschlecht");
                txtTelNr.DataBindings.Add("Text", _bs, "TelNr");
                txtReligion.DataBindings.Add("Text", _bs, "Religion");
                txtHnr.DataBindings.Add("Text", _bs, "Hausnummer");
                txtStraße.DataBindings.Add("Text", _bs, "Straße");
                txtPLZ.DataBindings.Add("Text", _bs, "PLZ");
                txtOrt.DataBindings.Add("Text", _bs, "Ort");
                txtStaatsbuergerschaft.DataBindings.Add("Text", _bs, "Staatsbuergerschaft");
                cBFamilienstand.DataBindings.Add("Text", _bs, "Familienstand");
                txtBeruf.DataBindings.Add("Text", _bs, "Beruf");
                cBObsorge.DataBindings.Add("Text", _bs, "Obsorge");
                txtAnmerkungen.DataBindings.Add("Text", _bs, "Anmerkungen");
                txtLcd.DataBindings.Add("Text", _bs, "Landcode");
                #endregion

                if (_con.State == ConnectionState.Closed)
                    _con.Open();

                if (txtPNr.Text == "")
                {
                    _state = "new";
                    _check = false;
                    switch (_typ)
                    {
                        case "Client":
                            Text = "neuen Klienten anlegen";
                            txtPNr.Text = NewPNr(_typ);
                            lblTyp.Text = "IK";
                            cbPtyp.Enabled = false;
                            break;

                        case "Angehöriger":
                            Text = "neue Person anlegen";
                            txtPNr.Text = NewPNr(_typ);
                            lblTyp.Text = "KM";
                            break;

                        case "Prozessbegleitung":
                            Text = "neue Prozessbegleitung anlegen";
                            txtPNr.Text = NewPNr(_typ);
                            lblTyp.Text = "Pb";
                            cbPtyp.Enabled = false;
                            break;

                        case "Intervision":
                            Text = "neue Intervision mit Fachwissen anlegen";
                            txtPNr.Text = NewPNr(_typ);
                            lblTyp.Text = "ImF";
                            cbPtyp.Enabled = false;
                            break;

                        case "Telefon":
                            Text = "neue telefonische Befassung erfassen";
                            txtPNr.Text = NewPNr(_typ);
                            lblTyp.Text = "TB";
                            cbPtyp.Enabled = false;
                            break;

                        case "Sonstige":
                            Text = "neue sonstige Befassung erfassen";
                            txtPNr.Text = NewPNr(_typ);
                            lblTyp.Text = "SB";
                            cbPtyp.Enabled = false;
                            break;
                    }
                }
                else
                {
                    _state = "old";
                    switch (_typ)
                    {
                        case "Client":
                            Text = "Klient bearbeiten";
                            cbPtyp.Enabled = false;
                            break;

                        case "Angehöriger":
                            Text = "Person bearbeiten";
                            break;

                        case "Prozessbegleitung":
                            Text = "Prozessbegleitung bearbeiten";
                            cbPtyp.Enabled = false;
                            break;

                        case "Intervision":
                            Text = "Intervision mit Fachwissen bearbeiten";
                            cbPtyp.Enabled = false;
                            break;

                        case "Telefon":
                            Text = "Telefonische Befassung bearbeiten";
                            cbPtyp.Enabled = false;
                            break;

                        case "Sonstige":
                            Text = "Sonstige Befassung bearbeiten";
                            cbPtyp.Enabled = false;
                            break;
                    }
                }

                typ = new List<NumberString>();
                var command = new SqlCommand("Select * from PTyp_Bezeichnung", _con);

                var reader = command.ExecuteReader();

                while (reader.Read())
                    if (_typ == "Angehöriger")
                    {
                        if (reader[0].ToString() != "IK" && reader[0].ToString() != "Pb" &&
                            reader[0].ToString() != "ImF" && reader[0].ToString() != "TB" &&
                            reader[0].ToString() != "SB" && reader[0].ToString() != "KOOP")
                        {
                            typ.Add(new NumberString(reader[0].ToString(), reader[1].ToString()));
                            cbPtyp.Items.Add(reader[1]);
                        }
                    }
                    else
                    {
                        typ.Add(new NumberString(reader[0].ToString(), reader[1].ToString()));
                        cbPtyp.Items.Add(reader[1]);
                    }
                reader.Close();
                cbPtyp.Text = new NumberString("", "").findName(lblTyp.Text, typ);

                reader = new SqlCommand("Select distinct(Religion) from Person", _con).ExecuteReader();
                while (reader.Read())
                    txtReligion.Items.Add(reader[0].ToString());
                reader.Close();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }


        public string NewPNr(string typ)
        {
            try
            {
                if (typ == "Client")
                    return GetIKNr();
                                               
                var command =
                    new SqlCommand("select max(PNr) from Person where PTyp = '"+lblTyp.Text+"'", _con).ExecuteScalar().ToString();
                
                if (command == "")
                    return "1";

                return (int.Parse(command) + 1).ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private string GetIKNr()
        {
            try
            {
                var nummer = "";

                var command = new SqlCommand("select max(PNr) from Person where PTyp = 'IK'", _con);          

                nummer = command.ExecuteScalar().ToString();

                if (nummer.Substring(0, 4) == DateTime.Now.Year.ToString()) //Wenn ein neues Jahr beginnt
                {
                    var newNumber = (Convert.ToInt32(nummer.Substring(5, 3)) + 1).ToString();
                    //WICHTIG: Wir gehen davon aus das max 999 Klienten (also von 1 - 999) pro Jahr neu hinzukommen

                    newNumber = newNumber.PadLeft(3, '0'); //Damit die Nummer immer 3 Stellen lang ist             

                    return DateTime.Now.Year + "_" + newNumber;
                    //Die fertige Nummer wird zurückgegeben im Format: "Jahr + _ + Nummer"
                }
                return DateTime.Now.Year + "_" + "001";
                //Die fertige Nummer wird zurückgegeben im Format: "Jahr + _ + Nummer"
            }
            catch
            {
                return DateTime.Now.Year + "_" + "001";
            }
            // Hier ist es auch sehr wichtig, dass der Computer, auf dem dieses Programm läuft, die richtige Zeit und Datum eingestellt hat
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Save())
            {
                _state = "old";
                MessageBox.Show(Resources.SaveSuccessful, Resources.MessageBoxHeaderHinweis);
            }
        }

        private bool Save()
        {
            try
            {
                if (string.IsNullOrEmpty(cbPtyp.Text))
                {
                    MessageBox.Show(Resources.ClientWarnungNichtAusgefuellt, Resources.MessageBoxHeaderWarnung);
                    return false;
                }
                else
                {
                    if(_state == "new" && _typ != "Angehöriger")
                    txtPNr.Text = this.NewPNr(_typ);

                    _bs.EndEdit();                  
                    _adapt.AdaptClient.Update(_dt);
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
                return false;
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Client_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
               if (e.CloseReason == CloseReason.UserClosing)
                {
                    var box = MessageBox.Show("Wollen Sie die Änderungen speichern?",
                    Resources.MessageBoxHeaderHinweis, MessageBoxButtons.YesNoCancel);

                    if (box == DialogResult.Yes)
                    {
                        Save();
                        DialogResult = DialogResult.OK;
                    }
                    else if (box == DialogResult.No)
                    {
                        _bs.CancelEdit();
                        DialogResult = DialogResult.Cancel;
                    }
                    else
                        e.Cancel = true;
                }               
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void cbPtyp_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblTyp.Text = new NumberString("", "").findID(cbPtyp.Text, typ);
            if(lblTyp.Text != "IK" && !_check)
                txtPNr.Text = new SqlCommand("select ISNULL(max(PNr),0) + 1 from Person where PTyp like('" + lblTyp.Text + "')", _con).ExecuteScalar().ToString();
            
        }

        private void txtPLZ_Leave(object sender, EventArgs e)
        {
            try
            {
                if (_conf.getString("autoFillPLZ") == "true" && txtLcd.Text.ToUpper() == "AT")
                    txtOrt.Text =
                        new SqlCommand("Select Ort from plz_ort where PLZ = '" + txtPLZ.Text + "'", _con).ExecuteScalar().ToString();
            }
            catch
            {
            }
        }

        private void txtOrt_Leave(object sender, EventArgs e)
        {
            try
            {
                if (_conf.getString("autoFillPLZ") == "true" && txtLcd.Text.ToUpper() == "AT")
                    txtPLZ.Text =
                        new SqlCommand("Select PLZ from plz_ort where Ort = '" + txtOrt.Text + "'", _con).ExecuteScalar().ToString();
            }
            catch
            {
            }
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {
            var command = new SqlCommand("Select * from lcd", _con);
            var auswahl = new Auswahl(_con, command.ExecuteReader(), "Landcode");

            if (auswahl.ShowDialog() == DialogResult.OK)
            {
                txtLcd.Text = auswahl.GetId;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var command = new SqlCommand("Select * from lcd", _con);
            var auswahl = new Auswahl(_con, command.ExecuteReader(), "Landcode");

            if (auswahl.ShowDialog() == DialogResult.OK)
            {
                txtStaatsbuergerschaft.Text = auswahl.GetId;
            }
        }
    }
}
