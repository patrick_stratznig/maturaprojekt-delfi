﻿namespace Maturaprojekt_Vorschau
{
    partial class NewCommandBuilder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvShow = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.cbTable = new System.Windows.Forms.ComboBox();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdSave = new System.Windows.Forms.Button();
            this.gBSpalten = new System.Windows.Forms.GroupBox();
            this.cmdAbwaehlen = new System.Windows.Forms.Button();
            this.cmdAuswaehlen = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShow)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvShow
            // 
            this.dgvShow.AllowUserToAddRows = false;
            this.dgvShow.AllowUserToDeleteRows = false;
            this.dgvShow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvShow.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvShow.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvShow.GridColor = System.Drawing.Color.Black;
            this.dgvShow.Location = new System.Drawing.Point(11, 278);
            this.dgvShow.Margin = new System.Windows.Forms.Padding(2);
            this.dgvShow.Name = "dgvShow";
            this.dgvShow.ReadOnly = true;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvShow.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvShow.RowTemplate.Height = 33;
            this.dgvShow.Size = new System.Drawing.Size(634, 253);
            this.dgvShow.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 10);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "Tabelle auswählen";
            // 
            // cbTable
            // 
            this.cbTable.FormattingEnabled = true;
            this.cbTable.Location = new System.Drawing.Point(15, 41);
            this.cbTable.Margin = new System.Windows.Forms.Padding(2);
            this.cbTable.Name = "cbTable";
            this.cbTable.Size = new System.Drawing.Size(157, 26);
            this.cbTable.TabIndex = 7;
            this.cbTable.SelectedIndexChanged += new System.EventHandler(this.cbTable_SelectedIndexChanged);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(362, 546);
            this.cmdCancel.Margin = new System.Windows.Forms.Padding(1);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(109, 32);
            this.cmdCancel.TabIndex = 81;
            this.cmdCancel.Text = "Schließen";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSave.Location = new System.Drawing.Point(486, 546);
            this.cmdSave.Margin = new System.Windows.Forms.Padding(1);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(160, 32);
            this.cmdSave.TabIndex = 80;
            this.cmdSave.Text = "Speichern als Excel";
            this.cmdSave.UseVisualStyleBackColor = true;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // gBSpalten
            // 
            this.gBSpalten.Location = new System.Drawing.Point(11, 88);
            this.gBSpalten.Name = "gBSpalten";
            this.gBSpalten.Size = new System.Drawing.Size(635, 185);
            this.gBSpalten.TabIndex = 82;
            this.gBSpalten.TabStop = false;
            this.gBSpalten.Text = "Spalten auswählen";
            // 
            // cmdAbwaehlen
            // 
            this.cmdAbwaehlen.Location = new System.Drawing.Point(244, 40);
            this.cmdAbwaehlen.Name = "cmdAbwaehlen";
            this.cmdAbwaehlen.Size = new System.Drawing.Size(109, 27);
            this.cmdAbwaehlen.TabIndex = 83;
            this.cmdAbwaehlen.Text = "alle abwählen";
            this.cmdAbwaehlen.UseVisualStyleBackColor = true;
            this.cmdAbwaehlen.Click += new System.EventHandler(this.cmdAbwaehlen_Click);
            // 
            // cmdAuswaehlen
            // 
            this.cmdAuswaehlen.Location = new System.Drawing.Point(362, 40);
            this.cmdAuswaehlen.Name = "cmdAuswaehlen";
            this.cmdAuswaehlen.Size = new System.Drawing.Size(120, 27);
            this.cmdAuswaehlen.TabIndex = 84;
            this.cmdAuswaehlen.Text = "alle auswählen";
            this.cmdAuswaehlen.UseVisualStyleBackColor = true;
            this.cmdAuswaehlen.Click += new System.EventHandler(this.cmdAuswaehlen_Click);
            // 
            // NewCommandBuilder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AntiqueWhite;
            this.ClientSize = new System.Drawing.Size(656, 588);
            this.Controls.Add(this.cmdAuswaehlen);
            this.Controls.Add(this.cmdAbwaehlen);
            this.Controls.Add(this.gBSpalten);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdSave);
            this.Controls.Add(this.cbTable);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvShow);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "NewCommandBuilder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tabellen auslesen";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NewCommandBuilder_FormClosing);
            this.Load += new System.EventHandler(this.new_CommandBuilder_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvShow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvShow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbTable;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmdSave;
        private System.Windows.Forms.GroupBox gBSpalten;
        private System.Windows.Forms.Button cmdAbwaehlen;
        private System.Windows.Forms.Button cmdAuswaehlen;
    }
}