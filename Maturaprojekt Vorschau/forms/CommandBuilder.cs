﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.Diagnostics;
using System.Data.SqlClient;
using Maturaprojekt_Vorschau.Properties;
using Tools;
using NLog;

namespace Maturaprojekt_Vorschau
{
    public partial class NewCommandBuilder : Form
    {
        #region Variablen
        private SqlConnection _con;
        private string _table = "";
        private string _column;
        private List<CheckBox> _cbList;
        private Logger _logger;

        #endregion
        public NewCommandBuilder()
        {
            InitializeComponent();
        }

        public NewCommandBuilder(SqlConnection con)
        {
            this._con = con;
            InitializeComponent();
        }

        private void new_CommandBuilder_Load(object sender, EventArgs e)
        {
            try
            {
                var conf = new Configuration(Resources.ConfigurationPath);
                this.Font = new Font("Microsoft Sans Serif", conf.getInt("text-size"));

                _cbList = new List<CheckBox>();

                SqlCommand command = new SqlCommand(@"SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES", _con);

                if (_con.State == ConnectionState.Closed)
                    _con.Open();

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if(reader[0].ToString() != "Termin_txt" && reader[0].ToString() != "user_login")
                    cbTable.Items.Add(reader[0].ToString());
                }
                reader.Close();

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage, Resources.MessageBoxHeaderHinweis);
            }
        }   

        private void cbTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                    _table = cbTable.Text;
                    SqlCommand command = new SqlCommand(@"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='" + _table +"'", _con);
                    SqlDataReader reader = command.ExecuteReader();

                gBSpalten.Controls.Clear();
                _cbList.Clear();

                while (reader.Read())
                {
                    _cbList.Add(new CheckBox());
                    _cbList[_cbList.Count - 1].Text = reader[0].ToString();
                }

                reader.Close();
                CheckBox lastBox = null;
                int xPos = 6;
                int yPos = 34;

                //Unterschied zwischen Zeilen ist 35, zwischen Spalten ist er 25

                foreach(var checkBox in _cbList)
                {
                    gBSpalten.Controls.Add(checkBox);
                    checkBox.Text = checkBox.Text;
                    checkBox.AutoSize = true;
                    checkBox.Name = "cB" + checkBox.Text;
                    checkBox.CheckState = CheckState.Checked;
                    checkBox.CheckStateChanged += CheckBox_CheckStateChanged;
                    if (lastBox == null)
                        checkBox.Location = new Point(xPos, yPos);
                    else
                    {
                        xPos += lastBox.Width + 25;
                        if (xPos + checkBox.Width > gBSpalten.Size.Width + gBSpalten.Location.X)
                        {
                            xPos = 6; yPos += 35;
                        }
                        checkBox.Location = new Point(xPos, yPos);
                    }
                    lastBox = checkBox;
                }

                show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void CheckBox_CheckStateChanged(object sender, EventArgs e)
        {
            show();
        }

        private void show()
        {
            try
            {
                dgvShow.Rows.Clear();
                dgvShow.Columns.Clear();
                _column = "";

                foreach (var CheckBox in _cbList)
                {
                    if (CheckBox.CheckState == CheckState.Checked)
                    {
                        if (_column == "")
                            _column += CheckBox.Text;
                        else
                            _column += "," + CheckBox.Text;
                        dgvShow.Columns.Add("dgvShowColumn" + CheckBox.Text, CheckBox.Text);
                    }
                }
                if (_column != "")
                {
                    SqlCommand command = new SqlCommand(@"Select " + _column + " from " + _table, _con);
                    SqlDataReader reader = command.ExecuteReader();


                    string[] temp = new string[reader.FieldCount];

                    while (reader.Read())
                    {
                        for (int i = 0; i < temp.Length; i++)
                            temp[i] = reader[i].ToString();

                        dgvShow.Rows.Add(temp);
                        temp = new string[reader.FieldCount];
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            var ExcelApp = new Excel.Application();
            ExcelApp.Visible = true;
            ExcelApp.WindowState = Excel.XlWindowState.xlMaximized;
            ExcelApp.Workbooks.Add();
            Excel._Worksheet workSheet = (Excel.Worksheet)ExcelApp.ActiveSheet;

            char[] buchstaben = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
           for(int i = 0; i < dgvShow.Columns.Count;i++)
            {
                workSheet.Cells[1,buchstaben[i].ToString()] = dgvShow.Columns[i].HeaderText;
            }
            var row = 1;
            for (int j = 0; j < dgvShow.Rows.Count; j++)
            {
                row++;
                for (int i = 0; i < dgvShow.Columns.Count; i++)
                {
                    workSheet.Cells[row, buchstaben[i].ToString()] = dgvShow[i, j].Value;
                }
            }  
            
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void NewCommandBuilder_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Sind Sie sich sicher, dass Sie schließen wollen?", Resources.MessageBoxHeaderHinweis, MessageBoxButtons.YesNo) == DialogResult.No)
                    e.Cancel = true;
                else
                    DialogResult = DialogResult.OK;
            }
        }

        private void cmdAbwaehlen_Click(object sender, EventArgs e)
        {
            foreach(var temp in _cbList)            
                temp.Checked = false;
            
        }

        private void cmdAuswaehlen_Click(object sender, EventArgs e)
        {
            foreach (var temp in _cbList)
                temp.Checked = true;
        }
    }
}
