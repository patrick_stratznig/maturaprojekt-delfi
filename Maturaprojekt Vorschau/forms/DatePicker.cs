﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tools;
using Maturaprojekt_Vorschau.Properties;

namespace Maturaprojekt_Vorschau
{
    public partial class DatePicker : Form
    {
        public DateTime Beginn;
        public DateTime Ende;
        private Configuration _conf;

        public DatePicker()
        {
            InitializeComponent();
        }

        private void DatePicker_Load(object sender, EventArgs e)
        {
            _conf = new Configuration(Resources.ConfigurationPath);
            Font = new Font("Microsoft Sans Serif", _conf.getInt("text-size"));
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Beginn = dtpBeginn.Value;
            Ende = dtpEnde.Value;
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Wollen Sie wirklich abbrechen?", "Hinweis", MessageBoxButtons.YesNo) == DialogResult.Yes)
                DialogResult = DialogResult.Cancel;
        }
    }
}
