﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using Maturaprojekt_Vorschau.Properties;
using Tools;
using NLog;


namespace Maturaprojekt_Vorschau
{
    public partial class Einstellungen : Form
    {
        private SqlConnection _con;
        private Logger _logger;
        private Configuration _conf;

        public Einstellungen(SqlConnection con)
        {
            ownTextbox txt = new ownTextbox();
            _con = con;
            InitializeComponent();
        }
        

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                bool restart = false;
                if (txtServer.Text != _conf.getString("data-source"))
                {
                    _conf.setString("data-source", txtServer.Text);
                    restart = true;
                }
                if (txtDatenbank.Text != _conf.getString("initial-catalog"))
                {
                    _conf.setString("initial-catalog", txtDatenbank.Text);
                    restart = true;
                }
                if (txtFolder.Text != _conf.getString("BefKonFolderPath"))
                    _conf.setString("BefKonFolderPath", txtFolder.Text);

                if (txtSize.Text != _conf.getInt("text-size").ToString())
                {
                    _conf.setInt("text-size", int.Parse(txtSize.Text));
                    restart = true;
                }

                if(txtBackup.Text != _conf.getString("backup-path").ToString())
                {
                    _conf.setString("backup-path", txtBackup.Text);
                    restart = true;
                }

                if (cbAutoPLZ.CheckState == CheckState.Checked)      //CheckBox für automatisches Befüllen von PLZ oder Ort
                    _conf.setString("autoFillPLZ", "true");
                else
                    _conf.setString("autoFillPLZ", "false");

                _conf.saveConfig();

                if(restart)
                MessageBox.Show("Manche Änderungen werden erst wirksam, wenn Sie das Programm schließen und neu starten", Resources.MessageBoxHeaderHinweis);

                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        

        private void Einstellungen_Load(object sender, EventArgs e)
        {
            try
            {
                _conf = new Configuration(Resources.ConfigurationPath);
                this.Font = new Font("Microsoft Sans Serif", _conf.getInt("text-size"));

                _logger = LogManager.GetCurrentClassLogger();

                txtServer.Text = _conf.getString("data-source");
                txtDatenbank.Text = _conf.getString("initial-catalog");          
                cbAutoPLZ.Checked = bool.Parse(_conf.getString("autoFillPLZ"));          //CheckBox für automatisches Befüllen von PLZ oder Ort
                txtFolder.Text = _conf.getString("BefKonFolderPath");
                txtSize.Text = _conf.getInt("text-size").ToString();
                txtBackup.Text = _conf.getString("backup-path").ToString();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }
    }
}
