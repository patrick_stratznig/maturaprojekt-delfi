﻿namespace Maturaprojekt_Vorschau
{
    partial class Event
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdSave = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cmdEditMA = new System.Windows.Forms.Button();
            this.dgvMitarbeiter = new System.Windows.Forms.DataGridView();
            this.clmMNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmGebDat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmdNewMA = new System.Windows.Forms.Button();
            this.cmdAddMA = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dtpEnde = new System.Windows.Forms.DateTimePicker();
            this.dtpBeginn = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmdSearch = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.txtLcd = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtPLZ = new System.Windows.Forms.TextBox();
            this.txtOrt = new System.Windows.Forms.TextBox();
            this.txtHnr = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtStraße = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTyp = new System.Windows.Forms.TextBox();
            this.txtENr = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.lblEventTyp = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblWarnung = new System.Windows.Forms.Label();
            this.txtAnmerkungen = new System.Windows.Forms.TextBox();
            this.lblTyp = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMitarbeiter)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 15);
            this.label1.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 72);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 15);
            this.label7.TabIndex = 59;
            this.label7.Text = "Ende";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 35);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 15);
            this.label5.TabIndex = 58;
            this.label5.Text = "Beginn";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(603, 685);
            this.cmdCancel.Margin = new System.Windows.Forms.Padding(1);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(100, 32);
            this.cmdCancel.TabIndex = 14;
            this.cmdCancel.Text = "Schließen";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSave.Location = new System.Drawing.Point(714, 685);
            this.cmdSave.Margin = new System.Windows.Forms.Padding(1);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(100, 32);
            this.cmdSave.TabIndex = 13;
            this.cmdSave.Text = "Speichern";
            this.cmdSave.UseVisualStyleBackColor = true;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cmdEditMA);
            this.groupBox3.Controls.Add(this.dgvMitarbeiter);
            this.groupBox3.Controls.Add(this.cmdNewMA);
            this.groupBox3.Controls.Add(this.cmdAddMA);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(12, 238);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(660, 220);
            this.groupBox3.TabIndex = 80;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Mitarbeiter";
            // 
            // cmdEditMA
            // 
            this.cmdEditMA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdEditMA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdEditMA.Location = new System.Drawing.Point(459, 32);
            this.cmdEditMA.Margin = new System.Windows.Forms.Padding(2);
            this.cmdEditMA.Name = "cmdEditMA";
            this.cmdEditMA.Size = new System.Drawing.Size(191, 30);
            this.cmdEditMA.TabIndex = 11;
            this.cmdEditMA.Text = "Mitarbeiter bearbeiten";
            this.cmdEditMA.UseVisualStyleBackColor = true;
            this.cmdEditMA.Visible = false;
            this.cmdEditMA.Click += new System.EventHandler(this.cmdEditMA_Click);
            // 
            // dgvMitarbeiter
            // 
            this.dgvMitarbeiter.AllowUserToAddRows = false;
            this.dgvMitarbeiter.AllowUserToDeleteRows = false;
            this.dgvMitarbeiter.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dgvMitarbeiter.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvMitarbeiter.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMitarbeiter.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvMitarbeiter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMitarbeiter.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmMNr,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.clmGebDat});
            this.dgvMitarbeiter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvMitarbeiter.GridColor = System.Drawing.Color.Black;
            this.dgvMitarbeiter.Location = new System.Drawing.Point(2, 81);
            this.dgvMitarbeiter.Margin = new System.Windows.Forms.Padding(2);
            this.dgvMitarbeiter.Name = "dgvMitarbeiter";
            this.dgvMitarbeiter.ReadOnly = true;
            this.dgvMitarbeiter.RowTemplate.Height = 24;
            this.dgvMitarbeiter.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvMitarbeiter.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMitarbeiter.Size = new System.Drawing.Size(656, 137);
            this.dgvMitarbeiter.TabIndex = 2;
            this.dgvMitarbeiter.TabStop = false;
            // 
            // clmMNr
            // 
            this.clmMNr.HeaderText = "MNr";
            this.clmMNr.Name = "clmMNr";
            this.clmMNr.ReadOnly = true;
            this.clmMNr.Width = 59;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "Nachname";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 101;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "Vorname";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 89;
            // 
            // clmGebDat
            // 
            this.clmGebDat.HeaderText = "Geburtsdatum";
            this.clmGebDat.Name = "clmGebDat";
            this.clmGebDat.ReadOnly = true;
            this.clmGebDat.Width = 122;
            // 
            // cmdNewMA
            // 
            this.cmdNewMA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdNewMA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdNewMA.Location = new System.Drawing.Point(223, 32);
            this.cmdNewMA.Margin = new System.Windows.Forms.Padding(2);
            this.cmdNewMA.Name = "cmdNewMA";
            this.cmdNewMA.Size = new System.Drawing.Size(232, 30);
            this.cmdNewMA.TabIndex = 10;
            this.cmdNewMA.Text = "neuen Mitarbeiter hinzufügen";
            this.cmdNewMA.UseVisualStyleBackColor = true;
            this.cmdNewMA.Click += new System.EventHandler(this.cmdNewMA_Click);
            // 
            // cmdAddMA
            // 
            this.cmdAddMA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAddMA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAddMA.Location = new System.Drawing.Point(8, 32);
            this.cmdAddMA.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAddMA.Name = "cmdAddMA";
            this.cmdAddMA.Size = new System.Drawing.Size(210, 30);
            this.cmdAddMA.TabIndex = 9;
            this.cmdAddMA.Text = "Mitarbeiter hinzufügen";
            this.cmdAddMA.UseVisualStyleBackColor = true;
            this.cmdAddMA.Click += new System.EventHandler(this.cmdAddMA_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dtpEnde);
            this.groupBox1.Controls.Add(this.dtpBeginn);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(512, 64);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(295, 112);
            this.groupBox1.TabIndex = 81;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Zeiterfassung";
            // 
            // dtpEnde
            // 
            this.dtpEnde.CustomFormat = " dd/MM/yyyy  \'um\'  HH:mm";
            this.dtpEnde.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEnde.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEnde.Location = new System.Drawing.Point(70, 67);
            this.dtpEnde.Name = "dtpEnde";
            this.dtpEnde.Size = new System.Drawing.Size(213, 21);
            this.dtpEnde.TabIndex = 8;
            this.dtpEnde.Value = new System.DateTime(2017, 2, 1, 14, 58, 0, 0);
            // 
            // dtpBeginn
            // 
            this.dtpBeginn.CustomFormat = " dd/MM/yyyy  \'um\'  HH:mm";
            this.dtpBeginn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpBeginn.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBeginn.Location = new System.Drawing.Point(70, 30);
            this.dtpBeginn.Name = "dtpBeginn";
            this.dtpBeginn.Size = new System.Drawing.Size(213, 21);
            this.dtpBeginn.TabIndex = 7;
            this.dtpBeginn.Value = new System.DateTime(2017, 2, 1, 14, 58, 0, 0);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmdSearch);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtLcd);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.txtPLZ);
            this.groupBox2.Controls.Add(this.txtOrt);
            this.groupBox2.Controls.Add(this.txtHnr);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtStraße);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(20, 64);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(467, 158);
            this.groupBox2.TabIndex = 82;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Adresse";
            // 
            // cmdSearch
            // 
            this.cmdSearch.BackgroundImage = global::Maturaprojekt_Vorschau.Properties.Resources.search_icon;
            this.cmdSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cmdSearch.Location = new System.Drawing.Point(224, 23);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(36, 34);
            this.cmdSearch.TabIndex = 2;
            this.cmdSearch.UseVisualStyleBackColor = true;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(23, 31);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 15);
            this.label10.TabIndex = 52;
            this.label10.Text = "Land";
            // 
            // txtLcd
            // 
            this.txtLcd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLcd.Location = new System.Drawing.Point(177, 28);
            this.txtLcd.Margin = new System.Windows.Forms.Padding(2);
            this.txtLcd.MaxLength = 2;
            this.txtLcd.Name = "txtLcd";
            this.txtLcd.ReadOnly = true;
            this.txtLcd.Size = new System.Drawing.Size(42, 21);
            this.txtLcd.TabIndex = 51;
            this.txtLcd.TabStop = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(23, 115);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(48, 15);
            this.label18.TabIndex = 50;
            this.label18.Text = "PLZ/Ort";
            // 
            // txtPLZ
            // 
            this.txtPLZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPLZ.Location = new System.Drawing.Point(177, 112);
            this.txtPLZ.Margin = new System.Windows.Forms.Padding(2);
            this.txtPLZ.Name = "txtPLZ";
            this.txtPLZ.Size = new System.Drawing.Size(74, 21);
            this.txtPLZ.TabIndex = 5;
            this.txtPLZ.Leave += new System.EventHandler(this.txtPLZ_Leave);
            // 
            // txtOrt
            // 
            this.txtOrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrt.Location = new System.Drawing.Point(255, 112);
            this.txtOrt.Margin = new System.Windows.Forms.Padding(2);
            this.txtOrt.Name = "txtOrt";
            this.txtOrt.Size = new System.Drawing.Size(188, 21);
            this.txtOrt.TabIndex = 6;
            this.txtOrt.Leave += new System.EventHandler(this.txtOrt_Leave);
            // 
            // txtHnr
            // 
            this.txtHnr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHnr.Location = new System.Drawing.Point(387, 70);
            this.txtHnr.Margin = new System.Windows.Forms.Padding(2);
            this.txtHnr.Name = "txtHnr";
            this.txtHnr.Size = new System.Drawing.Size(57, 21);
            this.txtHnr.TabIndex = 4;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(23, 73);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 15);
            this.label14.TabIndex = 49;
            this.label14.Text = "Straße/Nummer";
            // 
            // txtStraße
            // 
            this.txtStraße.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStraße.Location = new System.Drawing.Point(177, 70);
            this.txtStraße.Margin = new System.Windows.Forms.Padding(2);
            this.txtStraße.Name = "txtStraße";
            this.txtStraße.Size = new System.Drawing.Size(206, 21);
            this.txtStraße.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(224, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 15);
            this.label2.TabIndex = 83;
            this.label2.Text = "Art der Fortbildung";
            // 
            // txtTyp
            // 
            this.txtTyp.Location = new System.Drawing.Point(386, 15);
            this.txtTyp.Name = "txtTyp";
            this.txtTyp.ReadOnly = true;
            this.txtTyp.Size = new System.Drawing.Size(202, 21);
            this.txtTyp.TabIndex = 84;
            this.txtTyp.TabStop = false;
            // 
            // txtENr
            // 
            this.txtENr.Location = new System.Drawing.Point(96, 15);
            this.txtENr.Name = "txtENr";
            this.txtENr.ReadOnly = true;
            this.txtENr.Size = new System.Drawing.Size(80, 21);
            this.txtENr.TabIndex = 87;
            this.txtENr.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 15);
            this.label3.TabIndex = 86;
            this.label3.Text = "Nummer";
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::Maturaprojekt_Vorschau.Properties.Resources.search_icon;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.Location = new System.Drawing.Point(594, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(36, 34);
            this.button1.TabIndex = 1;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblEventTyp
            // 
            this.lblEventTyp.AutoSize = true;
            this.lblEventTyp.Location = new System.Drawing.Point(689, 21);
            this.lblEventTyp.Name = "lblEventTyp";
            this.lblEventTyp.Size = new System.Drawing.Size(0, 15);
            this.lblEventTyp.TabIndex = 88;
            this.lblEventTyp.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblWarnung);
            this.groupBox4.Controls.Add(this.txtAnmerkungen);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(14, 488);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(658, 178);
            this.groupBox4.TabIndex = 104;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Anmerkungen";
            // 
            // lblWarnung
            // 
            this.lblWarnung.AutoSize = true;
            this.lblWarnung.ForeColor = System.Drawing.Color.Red;
            this.lblWarnung.Location = new System.Drawing.Point(296, 8);
            this.lblWarnung.Name = "lblWarnung";
            this.lblWarnung.Size = new System.Drawing.Size(0, 15);
            this.lblWarnung.TabIndex = 79;
            // 
            // txtAnmerkungen
            // 
            this.txtAnmerkungen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAnmerkungen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnmerkungen.Location = new System.Drawing.Point(3, 17);
            this.txtAnmerkungen.Multiline = true;
            this.txtAnmerkungen.Name = "txtAnmerkungen";
            this.txtAnmerkungen.Size = new System.Drawing.Size(652, 158);
            this.txtAnmerkungen.TabIndex = 12;
            // 
            // lblTyp
            // 
            this.lblTyp.AutoSize = true;
            this.lblTyp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTyp.Location = new System.Drawing.Point(337, 18);
            this.lblTyp.Name = "lblTyp";
            this.lblTyp.Size = new System.Drawing.Size(12, 15);
            this.lblTyp.TabIndex = 105;
            this.lblTyp.Text = "-";
            // 
            // Event
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AntiqueWhite;
            this.ClientSize = new System.Drawing.Size(833, 730);
            this.Controls.Add(this.lblTyp);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.lblEventTyp);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtENr);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtTyp);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdSave);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Event";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fortbildung";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Event_FormClosing);
            this.Load += new System.EventHandler(this.new_Event_Load);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMitarbeiter)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmdSave;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button cmdEditMA;
        private System.Windows.Forms.DataGridView dgvMitarbeiter;
        private System.Windows.Forms.Button cmdNewMA;
        private System.Windows.Forms.Button cmdAddMA;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmMNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmGebDat;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtpEnde;
        private System.Windows.Forms.DateTimePicker dtpBeginn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTyp;
        private System.Windows.Forms.TextBox txtENr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button cmdSearch;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtLcd;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtPLZ;
        private System.Windows.Forms.TextBox txtOrt;
        private System.Windows.Forms.TextBox txtHnr;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtStraße;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblEventTyp;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblWarnung;
        private System.Windows.Forms.TextBox txtAnmerkungen;
        private System.Windows.Forms.Label lblTyp;
    }
}