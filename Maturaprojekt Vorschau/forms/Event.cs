﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Maturaprojekt_Vorschau.Properties;
using NLog;
using Tools;

namespace Maturaprojekt_Vorschau
{
    public partial class Event : Form
    {
        #region Variablen

        private MethodRepository _rep;
        private Logger _logger = LogManager.GetCurrentClassLogger();
        private SqlConnection _con;
        private BindingSource _bs;        
        private DataTable _dt;
        private Configuration _conf;
        private bool _isUserAdmin;
        private string _typ = "old";
        #endregion
        public Event()
        {
            InitializeComponent();
        }
        public Event(SqlConnection con, BindingSource bs, ref DataTable dt, bool isUserAdmin)
        {
            _con = con;
            _bs = bs;
            _dt = dt;
            _isUserAdmin = isUserAdmin;
            InitializeComponent();
        }

        private void new_Event_Load(object sender, EventArgs e)
        {
            try
            {
                _conf = new Configuration(Resources.ConfigurationPath);
                this.Font = new Font("Microsoft Sans Serif", _conf.getInt("text-size"));

                _rep = new MethodRepository(_con);

                txtAnmerkungen.DataBindings.Add("Text", _bs, "Kommentar");
                txtHnr.DataBindings.Add("Text", _bs, "Hausnummer");
                txtOrt.DataBindings.Add("Text", _bs, "Ort");
                txtPLZ.DataBindings.Add("Text", _bs, "PLZ");
                txtStraße.DataBindings.Add("Text", _bs, "Straße");
                lblTyp.DataBindings.Add("Text", _bs, "ETyp");
                txtLcd.DataBindings.Add("Text", _bs, "Landcode");
                dtpBeginn.DataBindings.Add("Text", _bs, "Datum_Beginn");
                dtpEnde.DataBindings.Add("Text", _bs, "Datum_Ende");
                txtENr.DataBindings.Add("Text", _bs, "ENr");

                if (_con.State != ConnectionState.Open)
                    _con.Open();

                dgvContextMenu cm = new dgvContextMenu(dgvMitarbeiter);

                if(txtENr.Text == "")
                {
                    _typ = "new";
                    newNr();
                }
                else
                {
                    // Teilnehmer der Fortbildung aus der Tabelle auslesen und in die dgvAuswahl schreiben

                    var command = new SqlCommand("Select * from _Events_Teilnehmer where ENr = @enr",_con);
                    command.Parameters.Add("@enr", SqlDbType.Int, 10000).Value = txtENr.Text;

                    var reader = command.ExecuteReader();

                    var includedMa = new List<string>();
                    while (reader.Read())
                        includedMa.Add(reader[1].ToString());

                    reader.Close();

                    foreach (var ma in includedMa)
                        DgvAuswahlInsert(ma);
                                        
                    reader.Close();

                    if(!string.IsNullOrEmpty(lblTyp.Text))
                    {
                        txtTyp.Text = new SqlCommand("Select Kategorie from _Events_Typ where ETyp = '" + lblTyp.Text + "'", _con).ExecuteScalar().ToString();
                    }
                }

                if(!_isUserAdmin)                
                    cmdNewMA.Enabled = cmdEditMA.Enabled = false;
                   
                
                                
            }catch(Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }
        private void newNr()
        {
            var temp = new SqlCommand("Select max(ENr) from _Events", _con).ExecuteScalar().ToString();

            if (!string.IsNullOrWhiteSpace(temp))
            {
                txtENr.Text = (int.Parse(temp) + 1).ToString();
            }
            else
            {
                txtENr.Text = "1";
            }
        }
        public void SaveMa()
        {
            if (_con.State == ConnectionState.Closed)
                _con.Open();

            SqlTransaction trans = _con.BeginTransaction();

            try
            {
                SqlCommand command = new SqlCommand("delete from _Events_Teilnehmer where ENr = " + txtENr.Text, _con)
                {
                    Transaction = trans
                };
                command.ExecuteNonQuery();

                foreach (DataGridViewRow row in dgvMitarbeiter.Rows)
                {
                    command =new SqlCommand("insert into _Events_Teilnehmer values(" + txtENr.Text + ", " + row.Cells[0].Value + ")", _con)
                        {
                            Transaction = trans
                        };
                    command.ExecuteNonQuery();
                }
                trans.Commit();

                
            }
            catch (Exception ex)
            {
                trans.Rollback();
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);

            }
        }
            
        private bool Save()
        {
            try
            {
                if (string.IsNullOrEmpty(dtpBeginn.Text) || string.IsNullOrEmpty(dtpEnde.Text))
                {
                    MessageBox.Show("Bitte Beginn- und Enddatum ausfüllen!", Resources.MessageBoxHeaderHinweis);
                    return false;
                }
                else if (string.IsNullOrEmpty(txtTyp.Text))
                {
                    MessageBox.Show("Bitte zuerst einen Typ auswählen!", Resources.MessageBoxHeaderHinweis);
                    return false;
                }
                else
                {
                    if (_typ == "new")
                        newNr();
                    _bs.EndEdit();
                    _rep.AdaptEvent.Update(_dt);
                    SaveMa();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
                return false;
            }
        }
        private void cmdSave_Click(object sender, EventArgs e)
        {
            if (Save())
            {
                _typ = "old";
                MessageBox.Show(Resources._EventSave, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Event_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (e.CloseReason == CloseReason.UserClosing)
                {
                    var box = MessageBox.Show("Wollen Sie die Änderungen speichern?",
                    Resources.MessageBoxHeaderHinweis, MessageBoxButtons.YesNoCancel);

                    if (box == DialogResult.Yes)
                    {
                        Save();
                        DialogResult = DialogResult.OK;
                    }
                    else if (box == DialogResult.No)
                    {
                        _bs.CancelEdit();
                        DialogResult = DialogResult.Cancel;
                    }
                    else
                        e.Cancel = true;

                }
          
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void cmdAddMA_Click(object sender, EventArgs e)
        {
            try
            {
                var auswahl = new Auswahl(_con, new SqlCommand("Select * from Mitarbeiter where geloescht = 0", _con).ExecuteReader(), "Mitarbeiter");

                if(auswahl.ShowDialog() == DialogResult.OK)                
                    DgvAuswahlInsert(auswahl.GetId);                
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void DgvAuswahlInsert(string text)
        {
            var read = new SqlCommand(@"select MNr,Vorname, Nachname, Geburtsdatum from Mitarbeiter where MNr = '" + text + "'", _con).ExecuteReader();
            read.Read();
            dgvMitarbeiter.Rows.Add(read[0], read[1], read[2], read[3].ToString());
            read.Close();

        }

        private void cmdNewMA_Click(object sender, EventArgs e)
        {
            try
            {
                var dt = new DataTable();
                _rep.AdaptMa.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;

                _bs.AddNew();

                Mitarbeiter ma = new Mitarbeiter(_con, _bs, ref dt);
                ma.ShowDialog();

                if(MessageBox.Show("Wollen Sie diesen Mitarbeiter gleich hinzufügen?",Resources.MessageBoxHeaderHinweis,MessageBoxButtons.YesNo) == DialogResult.Yes)
                DgvAuswahlInsert(ma.GetMNr);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void txtPLZ_Leave(object sender, EventArgs e)
        {
            try
            {
                if (_conf.getString("autoFillPLZ") == "true" && txtLcd.Text.ToUpper() == "AT")
                    txtOrt.Text = new SqlCommand("Select Ort from plz_ort where PLZ = '" + txtPLZ.Text + "'", _con).ExecuteScalar().ToString();
            }
            catch
            {

            }
        }

        private void txtOrt_Leave(object sender, EventArgs e)
        {
            try
            {
                if (_conf.getString("autoFillPLZ") == "true" && txtLcd.Text.ToUpper() == "AT")
                    txtPLZ.Text = new SqlCommand("Select PLZ from plz_ort where Ort = '" + txtOrt.Text + "'", _con).ExecuteScalar().ToString();
            }
            catch
            {

            }
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {
            var command = new SqlCommand("Select * from lcd", _con);
            var auswahl = new Auswahl(_con, command.ExecuteReader(), "Landcode");

            if (auswahl.ShowDialog() == DialogResult.OK)
            {
                txtLcd.Text = auswahl.GetId;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var command = new SqlCommand("select * from _Events_Typ", _con).ExecuteReader();
            var auswahl = new Auswahl(_con, command,"Ev_typ");

            if(auswahl.ShowDialog() == DialogResult.OK)
            {
                txtTyp.Text = new SqlCommand("select Kategorie from _Events_Typ where ETyp = '" + auswahl.GetId + "'",_con).ExecuteScalar().ToString();
                lblTyp.Text = auswahl.GetId;
            }
            command.Close();
        }

        private void cmdEditMA_Click(object sender, EventArgs e)
        {

        }
    }
}
