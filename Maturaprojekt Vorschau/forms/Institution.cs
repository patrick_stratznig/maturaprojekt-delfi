﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Maturaprojekt_Vorschau.Properties;
using Tools;

namespace Maturaprojekt_Vorschau
{
    public partial class Institution : Form
    {
        #region Variablen
        private BindingSource _bs;
        private SqlConnection _con;
        private List<NumberString> _ma;
  
        private string _INr;
        private Configuration _conf;
        private DataTable _dt;
        private MethodRepository _rep;
        private string _typ = "old";

        public string INr => _INr;
        #endregion
        public Institution()
        {
            InitializeComponent();
        }

        public Institution(SqlConnection con, BindingSource bs, ref DataTable dt)
        {
            _bs = bs;
            _con = con;
            _dt = dt;
            InitializeComponent();
        }
  

        private void cmdSave_Click(object sender, EventArgs e)
        {
            if(Save())
            {
                _typ = "old";
                MessageBox.Show(Resources.InstitutionSave, Resources.MessageBoxHeaderHinweis);
            }
        }

        public bool Save()
        {
            if (!string.IsNullOrEmpty(cBKategorie.Text))
            {
                if (_typ == "new")
                    newNr();

                _bs.EndEdit();
                _rep.AdaptInst.Update(_dt);
                return true;
            }
            else
            {
                MessageBox.Show(Resources.InstitutionNoKategorie, Resources.MessageBoxHeaderHinweis);
                return false;
            }
        }
        private void Institution_Load(object sender, EventArgs e)
        {
            try
            {
                _conf = new Configuration(Resources.ConfigurationPath);
                this.Font = new Font("Microsoft Sans Serif", _conf.getInt("text-size"));

                txtINr.DataBindings.Add("Text", _bs, "INr");
                lblITyp.DataBindings.Add("Text", _bs, "ITyp");
                txtPerson.DataBindings.Add("Text", _bs, "Ansprechperson");
                txtName.DataBindings.Add("Text", _bs, "Bezeichnung");
                txtHnr.DataBindings.Add("Text", _bs, "Hausnummer");
                txtPLZ.DataBindings.Add("Text", _bs, "PLZ");
                txtStraße.DataBindings.Add("Text", _bs, "Straße");
                txtLcd.DataBindings.Add("Text", _bs, "Landcode");
                txtTelNr.DataBindings.Add("Text", _bs, "TelNr");
                txtAnmerkungen.DataBindings.Add("Text", _bs, "Anmerkungen");
                txtTown.DataBindings.Add("Text", _bs, "Ort");
                txtEMail.DataBindings.Add("Text", _bs, "Email");
                txtTelNr2.DataBindings.Add("Text", _bs, "TelNr2");

                lblITyp.Visible = false;
        

                if (_con.State != ConnectionState.Open)
                    _con.Open();

                _rep = new MethodRepository(_con);

                if (txtINr.Text == "")
                {
                    _typ = "new";
                    newNr();
                }

                _INr = txtINr.Text;

                SqlCommand comm = new SqlCommand("Select ITyp, Kategorie from ITyp_Bezeichnung", _con);
                SqlDataReader reader = comm.ExecuteReader();
                _ma = new List<NumberString>();

                while(reader.Read())
                {
                    cBKategorie.Items.Add(reader[1].ToString());                    

                    NumberString temp = new NumberString(reader[0].ToString(), reader[1].ToString());
                    _ma.Add(temp);                    
                }
                cBKategorie.Text = new NumberString("", "").findName(lblITyp.Text, _ma);

                reader.Close();

                if(cBKategorie.Text == "Person auswaehlen")
                {
                    cBKategorie.Text = "Kategorie auswaehlen";
                }


                cBKategorie.SelectedIndex = 0;
            }catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void newNr()
        {
            SqlCommand command = new SqlCommand("Select max(INr) from Institution", _con);
            string nummer = command.ExecuteScalar().ToString();

            int temp = 0;

            if (nummer == "")
                temp = 1;
            else            
                temp = int.Parse(nummer)+1;                
            
            txtINr.Text = temp.ToString();
        }
        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Institution_FormClosing(object sender, FormClosingEventArgs e)
        {  
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult box = MessageBox.Show("Wollen Sie die Änderungen speichern?",
                    Resources.MessageBoxHeaderHinweis, MessageBoxButtons.YesNoCancel);

                if (box == DialogResult.Yes)
                {
                    Save();
                    DialogResult = DialogResult.OK;
                }
                else if (box == DialogResult.No)
                {
                    _bs.CancelEdit();
                    DialogResult = DialogResult.Cancel;
                }
                else
                    e.Cancel = true;
            }
                    
        }
       

        private void cBKategorie_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblITyp.Visible = true;
            lblITyp.Text = new NumberString("", "").findID(cBKategorie.Text, _ma);
            lblITyp.Visible = false;
        }

        private void txtTown_Leave(object sender, EventArgs e)
        {
            try
            {
                if (_conf.getString("autoFillPLZ") == "true" && txtLcd.Text == "AT")
                    txtPLZ.Text = new SqlCommand("Select PLZ from plz_ort where Ort = '" + txtTown.Text + "'", _con).ExecuteScalar().ToString();
            }
            catch
            {
            }
        }

        private void txtPLZ_Leave(object sender, EventArgs e)
        {
            try
            {
                if (_conf.getString("autoFillPLZ") == "true" && txtLcd.Text == "AT")
                    txtTown.Text = new SqlCommand("Select Ort from plz_ort where PLZ = '" + txtPLZ.Text + "'", _con).ExecuteScalar().ToString();
            }
            catch
            {
            }
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {
            var command = new SqlCommand("Select * from lcd", _con);
            var auswahl = new Auswahl(_con, command.ExecuteReader(), "Landcode");

            if (auswahl.ShowDialog() == DialogResult.OK)
            {
                txtLcd.Text = auswahl.GetId;
            }
        }
    }
}
