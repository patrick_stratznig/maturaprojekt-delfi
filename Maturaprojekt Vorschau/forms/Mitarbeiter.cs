﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing;
using Maturaprojekt_Vorschau.Properties;
using Tools;
using NLog;

namespace Maturaprojekt_Vorschau
{
    public partial class Mitarbeiter : Form
    {
        #region Variablen
        private SqlConnection _con;
        private BindingSource _bs;
        private Configuration _conf;
        private DataTable _dt;
        private MethodRepository _rep;
        private SqlDataAdapter _adapt;
        private string _typ = "new";
        private bool _userError = false;
        private bool _pwdError = false;
        private Logger _logger;
        #endregion

        public string GetMNr { get { return txtMNr.Text; } }
        public Mitarbeiter()
        {
            InitializeComponent();
        }
        public Mitarbeiter(SqlConnection con, BindingSource bs, ref DataTable dt)
        {
            InitializeComponent();
            _con = con;
            _bs = bs;
            _dt = dt;
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_userError && !_pwdError && !string.IsNullOrEmpty(txtPassword.Text) && !string.IsNullOrEmpty(txtUser.Text))
                {
                   
                    if (_typ == "new")
                        newNr();

                    _bs.EndEdit();
                    _adapt.Update(_dt);

                    if (_typ == "new")
                    {
                        SaveUser();
                        _typ = "old";
                        txtUser.Enabled = false;
                        txtPassword.Enabled = false;
                        cbIsAdmin.Enabled = false;
                        lblNoChange.Visible = false;
                    }
                    MessageBox.Show("Der Mitarbeiter wurde erfolgreich gespeichert!", Resources.MessageBoxHeaderHinweis);
                }
                else
                {
                    MessageBox.Show("Benutzername und Passwort müssen korrekt eingegeben werden!", Resources.MessageBoxHeaderHinweis);
                }
            }catch(Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void SaveUser()
        {
            SqlTransaction tran = _con.BeginTransaction();
            try
            {
                var command = new SqlCommand("Insert into user_login values(@mnr,@user,@admin)", _con);
                command.Parameters.Add("@mnr", SqlDbType.Int).Value = txtMNr.Text;
                command.Parameters.Add("@user", SqlDbType.VarChar).Value = txtUser.Text;
                command.Parameters.Add("@admin", SqlDbType.Bit).Value = cbIsAdmin.Checked;
                command.Transaction = tran;
                command.ExecuteNonQuery();

                command = new SqlCommand("create login [" + txtUser.Text + "] with Password = '" + txtPassword.Text + "',CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF", _con);
                command.Transaction = tran;
                command.ExecuteNonQuery();

                command = new SqlCommand("create user [" + txtUser.Text + "] for login [" + txtUser.Text + "] ", _con);
                command.Transaction = tran;
                command.ExecuteNonQuery();

                if (!cbIsAdmin.Checked)
                {
                    command = new SqlCommand("alter role [db_datareader] add member [" + txtUser.Text + "];", _con);
                    command.Transaction = tran;
                    command.ExecuteNonQuery();

                    command = new SqlCommand("alter role [db_datawriter] add member [" + txtUser.Text + "];", _con);
                    command.Transaction = tran;
                    command.ExecuteNonQuery();
                }
                else
                {
                    command = new SqlCommand("alter server role [sysadmin] add member [" + txtUser.Text + "];", _con);
                    command.Transaction = tran;
                    command.ExecuteNonQuery();
                }

                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }
        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void new_Mitarbeiter_Load(object sender, EventArgs e)
        {
            try
            {
                _conf = new Configuration(Resources.ConfigurationPath);
                Font = new Font("Microsoft Sans Serif", _conf.getInt("text-size"));

                _logger = LogManager.GetCurrentClassLogger();

                if (_con.State == ConnectionState.Closed)
                    _con.Open();

                _rep = new MethodRepository(_con);
                _adapt = _rep.AdaptMa;

                txtMNr.DataBindings.Add("Text", _bs, "MNr");
                txtFirstName.DataBindings.Add("Text", _bs, "Vorname");
                txtLastName.DataBindings.Add("Text", _bs, "Nachname");
                txt_Straße.DataBindings.Add("Text", _bs, "Straße");
                txt_Hnr.DataBindings.Add("Text", _bs, "Hausnummer");
                txtPLZ.DataBindings.Add("Text", _bs, "PLZ");
                txtOrt.DataBindings.Add("Text", _bs, "Ort");
                txtLcd.DataBindings.Add("Text", _bs, "Landcode");
                txtTelNr.DataBindings.Add("Text", _bs, "TelNr");
                txtGeburt.DataBindings.Add("Text", _bs, "Geburtsdatum");
                cbFamilienstand.DataBindings.Add("Text", _bs, "Familienstand");
                cbGeschlecht.DataBindings.Add("Text", _bs, "Geschlecht");
                txtReligion.DataBindings.Add("Text", _bs, "Religion");

                if(txtMNr.Text == "")
                {
                    _typ = "new";
                    newNr();
                }
                else
                {
                    _typ = "old";
                    var command = new SqlCommand("Select Username from user_login where MNr = @mnr",_con);
                    command.Parameters.Add("@mnr", SqlDbType.VarChar).Value = txtMNr.Text;

                    txtUser.Text = command.ExecuteScalar().ToString();
                    txtUser.Enabled = false;
                    txtPassword.Enabled = false;
                    cbIsAdmin.Enabled = false;
                }

            }catch(Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void newNr()
        {
            var command = new SqlCommand("Select max(MNr) from Mitarbeiter", _con).ExecuteScalar().ToString();

            if (!string.IsNullOrEmpty(command))
            {
                string temp = (Convert.ToInt32(command) + 1).ToString();
                txtMNr.Text = temp;
            }
            else
            {
                txtMNr.Text = "1";
            }
        }
        private void new_Mitarbeiter_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (e.CloseReason == CloseReason.UserClosing)
                {
                    DialogResult box = MessageBox.Show("Wollen Sie die Änderungen speichern?",
                        Resources.MessageBoxHeaderHinweis, MessageBoxButtons.YesNoCancel);

                    if (box == DialogResult.Yes)
                    {
                        if (_typ == "new")
                            newNr();
                        _bs.EndEdit();
                        _adapt.Update(_dt);
                        DialogResult = DialogResult.OK;
                    }
                    else if (box == DialogResult.No)
                        _bs.CancelEdit();
                    else
                        e.Cancel = true;
                }
            }catch(Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void txt_PLZ_Leave(object sender, EventArgs e)
        {
            try
            {
                if (_conf.getString("autoFillPLZ") == "true" && txtLcd.Text == "AT")
                    txtOrt.Text = new SqlCommand("Select Ort from plz_ort where PLZ = '" + txtPLZ.Text + "'", _con).ExecuteScalar() 
                        .ToString();
            }
            catch
            {
            }
        }

        private void txt_Ort_Leave(object sender, EventArgs e)
        {
            try
            {
                if (_conf.getString("autoFillPLZ") == "true" && txtLcd.Text == "AT")
                    txtPLZ.Text = new SqlCommand("Select PLZ from plz_ort where Ort = '" + txtOrt.Text + "'", _con).ExecuteScalar() 
                        .ToString();
            }
            catch
            {
            }
        }

        private void txtUser_Enter(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFirstName.Text) && !string.IsNullOrEmpty(txtLastName.Text))
            {
                txtUser.Text = txtLastName.Text.ToLower() + "." + txtFirstName.Text.ToLower().Split(' ')[0];
            }
        }

        
        private void txtUser_Leave(object sender, EventArgs e)
        {
            var command = new SqlCommand("Select count(*) from user_login where Username = @user",_con);
            command.Parameters.Add("@user", SqlDbType.VarChar).Value = txtUser.Text;

            
            var erg = int.Parse(command.ExecuteScalar().ToString());
           
            if (erg >= 1)
            {
                lblError.Visible = true;
                _userError = true;
            }
            else
            {
                lblError.Visible = false;
                _userError = false;
            }
        }

        private void txtPassword_Leave(object sender, EventArgs e)
        {
            if (txtPassword.Text.Length < 4)
            {
                lblPwdError.Visible = true;
                _pwdError = true;
            }
            else
            {
                lblPwdError.Visible = false;
                _pwdError = false;
            }
        }

        private void cbIsAdmin_CheckedChanged(object sender, EventArgs e)
        {
            if(cbIsAdmin.Checked == true)
            {
                if (MessageBox.Show("Wenn Sie jetzt speichern, bekommt dieser Mitarbeiter alle Rechte in der Datenbank. Das heißt er kann Tabellen löschen und Daten ändern, die, falls sie geändert werden, die komplette Datenbank unbrauchbar machen würden." + Environment.NewLine + Environment.NewLine +
                    "Sind Sie sich sicher, dass Sie diesem Mitarbeiter Adminrechte geben wollen?", "Warnung", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    cbIsAdmin.Checked = false;
            }
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {
            try
            {
                var command = new SqlCommand("Select * from lcd", _con);
                var auswahl = new Auswahl(_con, command.ExecuteReader(), "Landcode");

                if (auswahl.ShowDialog() == DialogResult.OK)
                {
                    txtLcd.Text = auswahl.GetId;
                }
            }catch(Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var command = new SqlCommand("Select * from lcd", _con);
                var auswahl = new Auswahl(_con, command.ExecuteReader(), "Landcode");

                if (auswahl.ShowDialog() == DialogResult.OK)
                {
                    txtLandcode.Text = auswahl.GetId;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }
    }
}
