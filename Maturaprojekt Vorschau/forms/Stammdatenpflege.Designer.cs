﻿namespace Maturaprojekt_Vorschau
{
    partial class Stammdatenpflege
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cBAuswahl = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblColumn1 = new System.Windows.Forms.Label();
            this.lblColumn2 = new System.Windows.Forms.Label();
            this.txtColumn1 = new System.Windows.Forms.TextBox();
            this.txtColumn2 = new System.Windows.Forms.TextBox();
            this.dgvStammdaten = new System.Windows.Forms.DataGridView();
            this.cmsDGV = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.neuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bearbeitenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.löschenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnEdit = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.lblHinweis = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStammdaten)).BeginInit();
            this.cmsDGV.SuspendLayout();
            this.SuspendLayout();
            // 
            // cBAuswahl
            // 
            this.cBAuswahl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBAuswahl.FormattingEnabled = true;
            this.cBAuswahl.Items.AddRange(new object[] {
            "PTyp_Bezeichnung",
            "ITyp_Bezeichnung",
            "Interventionskategorien",
            "Behandlungsanlaesse",
            "plz_ort",
            "lcd",
            "_Events_Typ",
            "Angebotskategorie",
            "Befassungsart"});
            this.cBAuswahl.Location = new System.Drawing.Point(24, 49);
            this.cBAuswahl.Name = "cBAuswahl";
            this.cBAuswahl.Size = new System.Drawing.Size(179, 23);
            this.cBAuswahl.TabIndex = 3;
            this.cBAuswahl.SelectedIndexChanged += new System.EventHandler(this.cB_Auswahl_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Tabelle auswählen:";
            // 
            // lblColumn1
            // 
            this.lblColumn1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColumn1.AutoSize = true;
            this.lblColumn1.Location = new System.Drawing.Point(215, 38);
            this.lblColumn1.Name = "lblColumn1";
            this.lblColumn1.Size = new System.Drawing.Size(11, 15);
            this.lblColumn1.TabIndex = 14;
            this.lblColumn1.Text = "-";
            // 
            // lblColumn2
            // 
            this.lblColumn2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblColumn2.AutoSize = true;
            this.lblColumn2.Location = new System.Drawing.Point(215, 68);
            this.lblColumn2.Name = "lblColumn2";
            this.lblColumn2.Size = new System.Drawing.Size(11, 15);
            this.lblColumn2.TabIndex = 15;
            this.lblColumn2.Text = "-";
            // 
            // txtColumn1
            // 
            this.txtColumn1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtColumn1.Location = new System.Drawing.Point(312, 35);
            this.txtColumn1.Name = "txtColumn1";
            this.txtColumn1.Size = new System.Drawing.Size(257, 21);
            this.txtColumn1.TabIndex = 16;
            this.txtColumn1.Visible = false;
            this.txtColumn1.Leave += new System.EventHandler(this.txtColumn1_TextChanged);
            // 
            // txtColumn2
            // 
            this.txtColumn2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtColumn2.Location = new System.Drawing.Point(312, 65);
            this.txtColumn2.Name = "txtColumn2";
            this.txtColumn2.Size = new System.Drawing.Size(257, 21);
            this.txtColumn2.TabIndex = 17;
            this.txtColumn2.Visible = false;
            // 
            // dgvStammdaten
            // 
            this.dgvStammdaten.AllowUserToAddRows = false;
            this.dgvStammdaten.AllowUserToDeleteRows = false;
            this.dgvStammdaten.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvStammdaten.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvStammdaten.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvStammdaten.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStammdaten.ContextMenuStrip = this.cmsDGV;
            this.dgvStammdaten.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvStammdaten.Location = new System.Drawing.Point(24, 134);
            this.dgvStammdaten.MultiSelect = false;
            this.dgvStammdaten.Name = "dgvStammdaten";
            this.dgvStammdaten.ReadOnly = true;
            this.dgvStammdaten.RowTemplate.Height = 33;
            this.dgvStammdaten.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvStammdaten.Size = new System.Drawing.Size(545, 422);
            this.dgvStammdaten.TabIndex = 18;
            // 
            // cmsDGV
            // 
            this.cmsDGV.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.cmsDGV.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.neuToolStripMenuItem,
            this.bearbeitenToolStripMenuItem,
            this.löschenToolStripMenuItem});
            this.cmsDGV.Name = "cmsDGV";
            this.cmsDGV.Size = new System.Drawing.Size(134, 70);
            // 
            // neuToolStripMenuItem
            // 
            this.neuToolStripMenuItem.Name = "neuToolStripMenuItem";
            this.neuToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.neuToolStripMenuItem.Text = "Neu";
            this.neuToolStripMenuItem.Click += new System.EventHandler(this.neuToolStripMenuItem_Click);
            // 
            // bearbeitenToolStripMenuItem
            // 
            this.bearbeitenToolStripMenuItem.Name = "bearbeitenToolStripMenuItem";
            this.bearbeitenToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.bearbeitenToolStripMenuItem.Text = "Bearbeiten ";
            this.bearbeitenToolStripMenuItem.Click += new System.EventHandler(this.bearbeitenToolStripMenuItem_Click);
            // 
            // löschenToolStripMenuItem
            // 
            this.löschenToolStripMenuItem.Name = "löschenToolStripMenuItem";
            this.löschenToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.löschenToolStripMenuItem.Text = "Löschen";
            this.löschenToolStripMenuItem.Click += new System.EventHandler(this.löschenToolStripMenuItem_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEdit.Location = new System.Drawing.Point(456, 94);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(113, 34);
            this.btnEdit.TabIndex = 20;
            this.btnEdit.Text = "Bestätigen";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.Location = new System.Drawing.Point(469, 561);
            this.cmdCancel.Margin = new System.Windows.Forms.Padding(2);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(100, 34);
            this.cmdCancel.TabIndex = 23;
            this.cmdCancel.Text = "Schließen";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblHinweis
            // 
            this.lblHinweis.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHinweis.AutoSize = true;
            this.lblHinweis.Location = new System.Drawing.Point(309, 9);
            this.lblHinweis.Name = "lblHinweis";
            this.lblHinweis.Size = new System.Drawing.Size(146, 15);
            this.lblHinweis.TabIndex = 24;
            this.lblHinweis.Text = "Bitte nun Daten eintragen";
            this.lblHinweis.Visible = false;
            // 
            // Stammdatenpflege
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AntiqueWhite;
            this.ClientSize = new System.Drawing.Size(596, 606);
            this.Controls.Add(this.lblHinweis);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.dgvStammdaten);
            this.Controls.Add(this.txtColumn2);
            this.Controls.Add(this.txtColumn1);
            this.Controls.Add(this.lblColumn2);
            this.Controls.Add(this.lblColumn1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cBAuswahl);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Stammdatenpflege";
            this.Text = "Stammdatenpflege";
            this.Load += new System.EventHandler(this.Stammdatenpflege_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStammdaten)).EndInit();
            this.cmsDGV.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cBAuswahl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblColumn1;
        private System.Windows.Forms.Label lblColumn2;
        private System.Windows.Forms.TextBox txtColumn1;
        private System.Windows.Forms.TextBox txtColumn2;
        private System.Windows.Forms.DataGridView dgvStammdaten;
        private System.Windows.Forms.ContextMenuStrip cmsDGV;
        private System.Windows.Forms.ToolStripMenuItem bearbeitenToolStripMenuItem;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.ToolStripMenuItem neuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem löschenToolStripMenuItem;
        private System.Windows.Forms.Label lblHinweis;
    }
}