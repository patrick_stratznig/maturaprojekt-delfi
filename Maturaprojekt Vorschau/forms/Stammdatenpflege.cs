﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using Maturaprojekt_Vorschau.Properties;
using Tools;
using NLog;

namespace Maturaprojekt_Vorschau
{
    public partial class Stammdatenpflege : Form
    {
        private SqlConnection _con;
        private string _typ;
        private Logger _logger;
        private string _column1;
        private string _column2;

        public Stammdatenpflege()
        {
            InitializeComponent();
        }
        public Stammdatenpflege(SqlConnection con)
        {
            InitializeComponent();
            _con = con;
        }

        private void Stammdatenpflege_Load(object sender, EventArgs e)
        {
            try
            {
                var conf = new Configuration(Resources.ConfigurationPath);
                this.Font = new Font("Microsoft Sans Serif", conf.getInt("text-size"));

                _logger = LogManager.GetCurrentClassLogger();

                if (_con.State != ConnectionState.Open)
                    _con.Open();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void IndexChanged()
        {
            try
            {
                dgvStammdaten.Columns.Clear();

                var selectedTable = cBAuswahl.Items[cBAuswahl.SelectedIndex];

                SqlDataReader reader = new SqlCommand(@"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='" + selectedTable + "'", _con).ExecuteReader();

                reader.Read();
                dgvStammdaten.Columns.Add("clm" + reader[0].ToString(), reader[0].ToString());
                _column1 = reader[0].ToString();
                lblColumn1.Text = _column1;

                reader.Read();
                dgvStammdaten.Columns.Add("clm" + reader[0].ToString(), reader[0].ToString());
                _column2 = reader[0].ToString();
                lblColumn2.Text = _column2;

                reader.Close();
                reader = new SqlCommand("Select * from " + selectedTable, _con).ExecuteReader();

                while (reader.Read())
                    dgvStammdaten.Rows.Add(reader[0].ToString(), reader[1].ToString());

                reader.Close();



            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }

        }
        private void cB_Auswahl_SelectedIndexChanged(object sender, EventArgs e)
        {           
            IndexChanged();

            SqlDataReader rdr = new SqlCommand("Select ISNULL(CHARACTER_MAXIMUM_LENGTH, 5000) from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = '" + cBAuswahl.Items[cBAuswahl.SelectedIndex] + "' ", _con).ExecuteReader();
            rdr.Read();
            txtColumn1.MaxLength = Convert.ToInt32(rdr[0].ToString());
            rdr.Read();
            txtColumn2.MaxLength = Convert.ToInt32(rdr[0].ToString());
            rdr.Close();
        }

        private void neuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                txtColumn1.Enabled = true;                
                txtColumn1.Text = txtColumn2.Text = "";
                txtColumn1.Visible = txtColumn2.Visible = true;
                _typ = "new";
                dgvStammdaten.Rows.Add();
                lblHinweis.Visible = true;
                txtColumn1.Focus();
            }
            catch(Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void bearbeitenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvStammdaten.CurrentCell != null)
            {
                txtColumn1.Visible = true;
                txtColumn2.Visible = true;
                txtColumn1.Enabled = false;
                txtColumn1.Text = dgvStammdaten.Rows[dgvStammdaten.SelectedRows[0].Index].Cells[0].Value.ToString();
                txtColumn2.Text = dgvStammdaten.Rows[dgvStammdaten.SelectedRows[0].Index].Cells[1].Value.ToString();
                _typ = "edit";
                txtColumn2.SelectAll();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(_typ))
                {
                    switch (_typ)
                    {
                        case "new":
                            {
                                SqlCommand cmdInsert = new SqlCommand("insert into " + cBAuswahl.Items[cBAuswahl.SelectedIndex] + " values(@index, @value)", _con);
                                cmdInsert.Parameters.AddWithValue("@index", txtColumn1.Text);
                                cmdInsert.Parameters.AddWithValue("@value", txtColumn2.Text);
                                cmdInsert.ExecuteNonQuery();

                                MessageBox.Show("Der Datensatz wurde erfolgreich hinzugefügt", Resources.MessageBoxHeaderHinweis);
                                lblHinweis.Visible = false;
                                txtColumn1.Text = txtColumn2.Text = "";
                            }
                            break;

                        case "edit":
                            {
                                SqlCommand cmdUpdate = new SqlCommand("update " + cBAuswahl.Items[cBAuswahl.SelectedIndex] + " set " + _column2 + " = '"
                                        + txtColumn2.Text + " 'where " + _column1 + "= @key", _con);
                                cmdUpdate.Parameters.AddWithValue("@key", txtColumn1.Text);
                                cmdUpdate.ExecuteNonQuery();

                                MessageBox.Show("Die Änderung wurde erfolgreich gespeichert!", Resources.MessageBoxHeaderHinweis);
                            }
                            break;
                    }
                    IndexChanged();
                    txtColumn2.Visible = txtColumn1.Visible = false;
                }

            }catch(Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }       

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Wollen Sie wirklich schließen?", Resources.MessageBoxHeaderHinweis, MessageBoxButtons.YesNo) ==
                DialogResult.Yes)
            {
                Close();                
            }
        }       

        private void txtColumn1_TextChanged(object sender, EventArgs e)
        {
            if (new SqlCommand("Select count(" + lblColumn2.Text + ") from " + cBAuswahl.Items[cBAuswahl.SelectedIndex] + " where " + lblColumn1.Text + " like('" + txtColumn1.Text + "')", _con).ExecuteScalar().ToString() != "0")
            {
                MessageBox.Show("Dieser Schlüssel existiert bereits! Bitte wählen Sie einen neuen");
                txtColumn1.Focus();
                txtColumn1.SelectAll();
            }
        }

        private void txtColumn2_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void löschenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                txtColumn1.Text = txtColumn2.Text = "";
                txtColumn1.Enabled = true;
                new SqlCommand("Delete from " + cBAuswahl.Items[cBAuswahl.SelectedIndex] + " where " + _column1 + " like ('" + dgvStammdaten.Rows[dgvStammdaten.SelectedRows[0].Index].Cells[0].Value.ToString() + "')", _con).ExecuteNonQuery();
                IndexChanged();
            }

            catch (Exception ex)
            {
                MessageBox.Show("Dieser Datensatz kann nicht gelöscht werden, da er verwendet wird!");
            }
        }
    }
}

/*
var integerKey = new int();
SqlCommand cmdUpdate;

if (int.TryParse(txtColumn1.Text, out integerKey))
{
    cmdUpdate = new SqlCommand("update " + cBAuswahl.Items[cBAuswahl.SelectedIndex] + " set " + lblColumn2.Text + "='" 
        + txtColumn2.Text + " 'where " + lblColumn1.Text + "=" + integerKey + "", _con);
}
else
{
    cmdUpdate = new SqlCommand("update " + cBAuswahl.Items[cBAuswahl.SelectedIndex] + " set " + lblColumn2.Text + "='" 
        + txtColumn2.Text + " 'where " + lblColumn1.Text + "='" + txtColumn1.Text + "'", _con);
}
_commandList.Add(cmdUpdate);
*/
