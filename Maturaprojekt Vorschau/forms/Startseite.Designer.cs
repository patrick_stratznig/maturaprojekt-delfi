﻿namespace Maturaprojekt_Vorschau
{
    partial class Startseite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvAnzeige = new System.Windows.Forms.DataGridView();
            this.cmstStartseite = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.befassungskontextToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.klientBearbeitenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.terminHinzufuegenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.täglichesBackupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monatlichesBackupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backupOrdnerÖffnenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.einstellungenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.beendenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bearbeitenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.neuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.befassungskontextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.terminToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.fortbildungToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sonstigeBefassungenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.telefonischToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.intervisionMitFachwissenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sonstigeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prozessToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bearbeitenToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.befassungskontextToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.terminToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.fortbildungToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.sonstigeBefassungenToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.telefonischToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.intervisionMitFachwissenToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sonstigeToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.prozessbegleitungToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stammdatenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.neuToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.klientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.msMaCreate = new System.Windows.Forms.ToolStripMenuItem();
            this.institutionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bearbeitenToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.klientToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.msMaEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.institutionToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.msLine = new System.Windows.Forms.ToolStripSeparator();
            this.msDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.mitarbeiterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.institutionToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.reaktivierenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mitarbeiterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.institutionToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.stammdatenpflegeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.passwortÄndernToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statistikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statistikAnzeigenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eigeneAbfrageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hilfeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.kontaktToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbKlienten = new System.Windows.Forms.CheckBox();
            this.btn_Klient = new System.Windows.Forms.Button();
            this.btn_BefKon = new System.Windows.Forms.Button();
            this.btn_Termin = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cBMitarbeiter = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblBackup = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnzeige)).BeginInit();
            this.cmstStartseite.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvAnzeige
            // 
            this.dgvAnzeige.AllowUserToAddRows = false;
            this.dgvAnzeige.AllowUserToDeleteRows = false;
            this.dgvAnzeige.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAnzeige.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvAnzeige.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAnzeige.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAnzeige.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAnzeige.ContextMenuStrip = this.cmstStartseite;
            this.dgvAnzeige.Location = new System.Drawing.Point(793, 203);
            this.dgvAnzeige.Margin = new System.Windows.Forms.Padding(4);
            this.dgvAnzeige.Name = "dgvAnzeige";
            this.dgvAnzeige.ReadOnly = true;
            this.dgvAnzeige.RowTemplate.Height = 24;
            this.dgvAnzeige.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAnzeige.Size = new System.Drawing.Size(542, 465);
            this.dgvAnzeige.TabIndex = 2;
            this.dgvAnzeige.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView1_ColumnWidthChanged);
            // 
            // cmstStartseite
            // 
            this.cmstStartseite.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmstStartseite.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.befassungskontextToolStripMenuItem1,
            this.klientBearbeitenToolStripMenuItem,
            this.terminHinzufuegenToolStripMenuItem});
            this.cmstStartseite.Name = "cmstStartseite";
            this.cmstStartseite.Size = new System.Drawing.Size(232, 70);
            // 
            // befassungskontextToolStripMenuItem1
            // 
            this.befassungskontextToolStripMenuItem1.Name = "befassungskontextToolStripMenuItem1";
            this.befassungskontextToolStripMenuItem1.Size = new System.Drawing.Size(231, 22);
            this.befassungskontextToolStripMenuItem1.Text = "Befassungskontext bearbeiten";
            this.befassungskontextToolStripMenuItem1.Click += new System.EventHandler(this.befassungskontextToolStripMenuItem1_Click);
            // 
            // klientBearbeitenToolStripMenuItem
            // 
            this.klientBearbeitenToolStripMenuItem.Name = "klientBearbeitenToolStripMenuItem";
            this.klientBearbeitenToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.klientBearbeitenToolStripMenuItem.Text = "Klient bearbeiten";
            this.klientBearbeitenToolStripMenuItem.Click += new System.EventHandler(this.klientBearbeitenToolStripMenuItem_Click);
            // 
            // terminHinzufuegenToolStripMenuItem
            // 
            this.terminHinzufuegenToolStripMenuItem.Name = "terminHinzufuegenToolStripMenuItem";
            this.terminHinzufuegenToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.terminHinzufuegenToolStripMenuItem.Text = "Termin hinzufügen";
            this.terminHinzufuegenToolStripMenuItem.Click += new System.EventHandler(this.terminHinzufuegenToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dateiToolStripMenuItem,
            this.bearbeitenToolStripMenuItem,
            this.stammdatenToolStripMenuItem,
            this.statistikToolStripMenuItem,
            this.hilfeToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(1348, 25);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dateiToolStripMenuItem
            // 
            this.dateiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backupToolStripMenuItem,
            this.einstellungenToolStripMenuItem,
            this.toolStripMenuItem3,
            this.beendenToolStripMenuItem});
            this.dateiToolStripMenuItem.Name = "dateiToolStripMenuItem";
            this.dateiToolStripMenuItem.Size = new System.Drawing.Size(46, 19);
            this.dateiToolStripMenuItem.Text = "Datei";
            // 
            // backupToolStripMenuItem
            // 
            this.backupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.täglichesBackupToolStripMenuItem,
            this.monatlichesBackupToolStripMenuItem,
            this.backupOrdnerÖffnenToolStripMenuItem});
            this.backupToolStripMenuItem.Name = "backupToolStripMenuItem";
            this.backupToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.backupToolStripMenuItem.Text = "Backup";
            this.backupToolStripMenuItem.Visible = false;
            // 
            // täglichesBackupToolStripMenuItem
            // 
            this.täglichesBackupToolStripMenuItem.Name = "täglichesBackupToolStripMenuItem";
            this.täglichesBackupToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.täglichesBackupToolStripMenuItem.Text = "tägliches Backup";
            this.täglichesBackupToolStripMenuItem.Click += new System.EventHandler(this.täglichesBackupToolStripMenuItem_Click);
            // 
            // monatlichesBackupToolStripMenuItem
            // 
            this.monatlichesBackupToolStripMenuItem.Name = "monatlichesBackupToolStripMenuItem";
            this.monatlichesBackupToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.monatlichesBackupToolStripMenuItem.Text = "monatliches Backup";
            // 
            // backupOrdnerÖffnenToolStripMenuItem
            // 
            this.backupOrdnerÖffnenToolStripMenuItem.Name = "backupOrdnerÖffnenToolStripMenuItem";
            this.backupOrdnerÖffnenToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.backupOrdnerÖffnenToolStripMenuItem.Text = "Backup-Ordner öffnen";
            this.backupOrdnerÖffnenToolStripMenuItem.Click += new System.EventHandler(this.backupOrdnerÖffnenToolStripMenuItem_Click);
            // 
            // einstellungenToolStripMenuItem
            // 
            this.einstellungenToolStripMenuItem.Name = "einstellungenToolStripMenuItem";
            this.einstellungenToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.einstellungenToolStripMenuItem.Text = "Einstellungen";
            this.einstellungenToolStripMenuItem.Click += new System.EventHandler(this.einstellungenToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(149, 6);
            // 
            // beendenToolStripMenuItem
            // 
            this.beendenToolStripMenuItem.Name = "beendenToolStripMenuItem";
            this.beendenToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.beendenToolStripMenuItem.Text = "Beenden";
            this.beendenToolStripMenuItem.Click += new System.EventHandler(this.beendenToolStripMenuItem_Click);
            // 
            // bearbeitenToolStripMenuItem
            // 
            this.bearbeitenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.neuToolStripMenuItem,
            this.bearbeitenToolStripMenuItem1});
            this.bearbeitenToolStripMenuItem.Name = "bearbeitenToolStripMenuItem";
            this.bearbeitenToolStripMenuItem.Size = new System.Drawing.Size(58, 19);
            this.bearbeitenToolStripMenuItem.Text = "Ändern";
            // 
            // neuToolStripMenuItem
            // 
            this.neuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.befassungskontextToolStripMenuItem,
            this.terminToolStripMenuItem,
            this.toolStripMenuItem4,
            this.fortbildungToolStripMenuItem1,
            this.sonstigeBefassungenToolStripMenuItem});
            this.neuToolStripMenuItem.Name = "neuToolStripMenuItem";
            this.neuToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.neuToolStripMenuItem.Text = "Neu";
            // 
            // befassungskontextToolStripMenuItem
            // 
            this.befassungskontextToolStripMenuItem.Name = "befassungskontextToolStripMenuItem";
            this.befassungskontextToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
            this.befassungskontextToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.befassungskontextToolStripMenuItem.Text = "Befassungskontext...";
            this.befassungskontextToolStripMenuItem.Click += new System.EventHandler(this.neuenBefassungskontextToolStripMenuItem_Click);
            // 
            // terminToolStripMenuItem
            // 
            this.terminToolStripMenuItem.Name = "terminToolStripMenuItem";
            this.terminToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.terminToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.terminToolStripMenuItem.Text = "Termin...";
            this.terminToolStripMenuItem.Click += new System.EventHandler(this.neuToolStripMenuItem2_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(221, 6);
            // 
            // fortbildungToolStripMenuItem1
            // 
            this.fortbildungToolStripMenuItem1.Name = "fortbildungToolStripMenuItem1";
            this.fortbildungToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.fortbildungToolStripMenuItem1.Size = new System.Drawing.Size(224, 22);
            this.fortbildungToolStripMenuItem1.Text = "Fortbildung";
            this.fortbildungToolStripMenuItem1.Click += new System.EventHandler(this.fortbildungEintragenToolStripMenuItem_Click);
            // 
            // sonstigeBefassungenToolStripMenuItem
            // 
            this.sonstigeBefassungenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.telefonischToolStripMenuItem1,
            this.intervisionMitFachwissenToolStripMenuItem,
            this.sonstigeToolStripMenuItem,
            this.prozessToolStripMenuItem});
            this.sonstigeBefassungenToolStripMenuItem.Name = "sonstigeBefassungenToolStripMenuItem";
            this.sonstigeBefassungenToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.sonstigeBefassungenToolStripMenuItem.Text = "sonstige Befassungen";
            // 
            // telefonischToolStripMenuItem1
            // 
            this.telefonischToolStripMenuItem1.Name = "telefonischToolStripMenuItem1";
            this.telefonischToolStripMenuItem1.Size = new System.Drawing.Size(213, 22);
            this.telefonischToolStripMenuItem1.Text = "Telefonisch";
            this.telefonischToolStripMenuItem1.Click += new System.EventHandler(this.telefonischToolStripMenuItem1_Click);
            // 
            // intervisionMitFachwissenToolStripMenuItem
            // 
            this.intervisionMitFachwissenToolStripMenuItem.Name = "intervisionMitFachwissenToolStripMenuItem";
            this.intervisionMitFachwissenToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.intervisionMitFachwissenToolStripMenuItem.Text = "Intervision mit Fachwissen";
            this.intervisionMitFachwissenToolStripMenuItem.Click += new System.EventHandler(this.intervisionMitFachwissenToolStripMenuItem_Click);
            // 
            // sonstigeToolStripMenuItem
            // 
            this.sonstigeToolStripMenuItem.Name = "sonstigeToolStripMenuItem";
            this.sonstigeToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.sonstigeToolStripMenuItem.Text = "Sonstige";
            this.sonstigeToolStripMenuItem.Click += new System.EventHandler(this.sonstigeToolStripMenuItem_Click);
            // 
            // prozessToolStripMenuItem
            // 
            this.prozessToolStripMenuItem.Name = "prozessToolStripMenuItem";
            this.prozessToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.prozessToolStripMenuItem.Text = "Prozessbegleitung";
            this.prozessToolStripMenuItem.Click += new System.EventHandler(this.prozessToolStripMenuItem_Click);
            // 
            // bearbeitenToolStripMenuItem1
            // 
            this.bearbeitenToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.befassungskontextToolStripMenuItem2,
            this.terminToolStripMenuItem2,
            this.toolStripMenuItem5,
            this.fortbildungToolStripMenuItem2,
            this.sonstigeBefassungenToolStripMenuItem1});
            this.bearbeitenToolStripMenuItem1.Name = "bearbeitenToolStripMenuItem1";
            this.bearbeitenToolStripMenuItem1.Size = new System.Drawing.Size(130, 22);
            this.bearbeitenToolStripMenuItem1.Text = "Bearbeiten";
            // 
            // befassungskontextToolStripMenuItem2
            // 
            this.befassungskontextToolStripMenuItem2.Name = "befassungskontextToolStripMenuItem2";
            this.befassungskontextToolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.B)));
            this.befassungskontextToolStripMenuItem2.Size = new System.Drawing.Size(247, 22);
            this.befassungskontextToolStripMenuItem2.Text = "Befassungskontext...";
            this.befassungskontextToolStripMenuItem2.Click += new System.EventHandler(this.befassungskontextBearbeitenToolStripMenuItem_Click);
            // 
            // terminToolStripMenuItem2
            // 
            this.terminToolStripMenuItem2.Name = "terminToolStripMenuItem2";
            this.terminToolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.T)));
            this.terminToolStripMenuItem2.Size = new System.Drawing.Size(247, 22);
            this.terminToolStripMenuItem2.Text = "Termin...";
            this.terminToolStripMenuItem2.Click += new System.EventHandler(this.bestehendenTerminBearbeitenToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(244, 6);
            // 
            // fortbildungToolStripMenuItem2
            // 
            this.fortbildungToolStripMenuItem2.Name = "fortbildungToolStripMenuItem2";
            this.fortbildungToolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.F)));
            this.fortbildungToolStripMenuItem2.Size = new System.Drawing.Size(247, 22);
            this.fortbildungToolStripMenuItem2.Text = "Fortbildung";
            this.fortbildungToolStripMenuItem2.Click += new System.EventHandler(this.fortbildungToolStripMenuItem2_Click);
            // 
            // sonstigeBefassungenToolStripMenuItem1
            // 
            this.sonstigeBefassungenToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.telefonischToolStripMenuItem2,
            this.intervisionMitFachwissenToolStripMenuItem1,
            this.sonstigeToolStripMenuItem2,
            this.prozessbegleitungToolStripMenuItem});
            this.sonstigeBefassungenToolStripMenuItem1.Name = "sonstigeBefassungenToolStripMenuItem1";
            this.sonstigeBefassungenToolStripMenuItem1.Size = new System.Drawing.Size(247, 22);
            this.sonstigeBefassungenToolStripMenuItem1.Text = "sonstige Befassungen";
            // 
            // telefonischToolStripMenuItem2
            // 
            this.telefonischToolStripMenuItem2.Name = "telefonischToolStripMenuItem2";
            this.telefonischToolStripMenuItem2.Size = new System.Drawing.Size(213, 22);
            this.telefonischToolStripMenuItem2.Text = "Telefonisch";
            this.telefonischToolStripMenuItem2.Click += new System.EventHandler(this.telefonischToolStripMenuItem2_Click);
            // 
            // intervisionMitFachwissenToolStripMenuItem1
            // 
            this.intervisionMitFachwissenToolStripMenuItem1.Name = "intervisionMitFachwissenToolStripMenuItem1";
            this.intervisionMitFachwissenToolStripMenuItem1.Size = new System.Drawing.Size(213, 22);
            this.intervisionMitFachwissenToolStripMenuItem1.Text = "Intervision mit Fachwissen";
            this.intervisionMitFachwissenToolStripMenuItem1.Click += new System.EventHandler(this.intervisionMitFachwissenToolStripMenuItem1_Click);
            // 
            // sonstigeToolStripMenuItem2
            // 
            this.sonstigeToolStripMenuItem2.Name = "sonstigeToolStripMenuItem2";
            this.sonstigeToolStripMenuItem2.Size = new System.Drawing.Size(213, 22);
            this.sonstigeToolStripMenuItem2.Text = "Sonstige";
            this.sonstigeToolStripMenuItem2.Click += new System.EventHandler(this.sonstigeToolStripMenuItem2_Click);
            // 
            // prozessbegleitungToolStripMenuItem
            // 
            this.prozessbegleitungToolStripMenuItem.Name = "prozessbegleitungToolStripMenuItem";
            this.prozessbegleitungToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.prozessbegleitungToolStripMenuItem.Text = "Prozessbegleitung";
            this.prozessbegleitungToolStripMenuItem.Click += new System.EventHandler(this.prozessbegleitungToolStripMenuItem_Click);
            // 
            // stammdatenToolStripMenuItem
            // 
            this.stammdatenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.neuToolStripMenuItem4,
            this.bearbeitenToolStripMenuItem7,
            this.msLine,
            this.msDelete,
            this.reaktivierenToolStripMenuItem,
            this.toolStripMenuItem6,
            this.stammdatenpflegeToolStripMenuItem,
            this.passwortÄndernToolStripMenuItem});
            this.stammdatenToolStripMenuItem.Name = "stammdatenToolStripMenuItem";
            this.stammdatenToolStripMenuItem.Size = new System.Drawing.Size(87, 19);
            this.stammdatenToolStripMenuItem.Text = "Stammdaten";
            // 
            // neuToolStripMenuItem4
            // 
            this.neuToolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.klientToolStripMenuItem,
            this.msMaCreate,
            this.institutionToolStripMenuItem});
            this.neuToolStripMenuItem4.Name = "neuToolStripMenuItem4";
            this.neuToolStripMenuItem4.Size = new System.Drawing.Size(175, 22);
            this.neuToolStripMenuItem4.Text = "Neu";
            // 
            // klientToolStripMenuItem
            // 
            this.klientToolStripMenuItem.Name = "klientToolStripMenuItem";
            this.klientToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.K)));
            this.klientToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.klientToolStripMenuItem.Text = "Klient...";
            this.klientToolStripMenuItem.Click += new System.EventHandler(this.neuerKlientToolStripMenuItem_Click);
            // 
            // msMaCreate
            // 
            this.msMaCreate.Name = "msMaCreate";
            this.msMaCreate.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this.msMaCreate.Size = new System.Drawing.Size(188, 22);
            this.msMaCreate.Text = "Mitarbeiter...";
            this.msMaCreate.Click += new System.EventHandler(this.neuerMitarbeiterToolStripMenuItem_Click);
            // 
            // institutionToolStripMenuItem
            // 
            this.institutionToolStripMenuItem.Name = "institutionToolStripMenuItem";
            this.institutionToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.institutionToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.institutionToolStripMenuItem.Text = "Institution...";
            this.institutionToolStripMenuItem.Click += new System.EventHandler(this.neuhinzufuegenToolStripMenuItem_Click);
            // 
            // bearbeitenToolStripMenuItem7
            // 
            this.bearbeitenToolStripMenuItem7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.klientToolStripMenuItem2,
            this.msMaEdit,
            this.institutionToolStripMenuItem2});
            this.bearbeitenToolStripMenuItem7.Name = "bearbeitenToolStripMenuItem7";
            this.bearbeitenToolStripMenuItem7.Size = new System.Drawing.Size(175, 22);
            this.bearbeitenToolStripMenuItem7.Text = "Bearbeiten";
            // 
            // klientToolStripMenuItem2
            // 
            this.klientToolStripMenuItem2.Name = "klientToolStripMenuItem2";
            this.klientToolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.K)));
            this.klientToolStripMenuItem2.Size = new System.Drawing.Size(211, 22);
            this.klientToolStripMenuItem2.Text = "Klient...";
            this.klientToolStripMenuItem2.Click += new System.EventHandler(this.klientenBearbeitenToolStripMenuItem_Click);
            // 
            // msMaEdit
            // 
            this.msMaEdit.Name = "msMaEdit";
            this.msMaEdit.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.M)));
            this.msMaEdit.Size = new System.Drawing.Size(211, 22);
            this.msMaEdit.Text = "Mitarbeiter...";
            this.msMaEdit.Click += new System.EventHandler(this.mitarbeiterBearbeitenToolStripMenuItem_Click);
            // 
            // institutionToolStripMenuItem2
            // 
            this.institutionToolStripMenuItem2.Name = "institutionToolStripMenuItem2";
            this.institutionToolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.I)));
            this.institutionToolStripMenuItem2.Size = new System.Drawing.Size(211, 22);
            this.institutionToolStripMenuItem2.Text = "Institution...";
            this.institutionToolStripMenuItem2.Click += new System.EventHandler(this.bearbeitenToolStripMenuItem5_Click);
            // 
            // msLine
            // 
            this.msLine.Name = "msLine";
            this.msLine.Size = new System.Drawing.Size(172, 6);
            // 
            // msDelete
            // 
            this.msDelete.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitarbeiterToolStripMenuItem,
            this.institutionToolStripMenuItem3});
            this.msDelete.Name = "msDelete";
            this.msDelete.Size = new System.Drawing.Size(175, 22);
            this.msDelete.Text = "Löschen";
            // 
            // mitarbeiterToolStripMenuItem
            // 
            this.mitarbeiterToolStripMenuItem.Name = "mitarbeiterToolStripMenuItem";
            this.mitarbeiterToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.mitarbeiterToolStripMenuItem.Text = "Mitarbeiter";
            this.mitarbeiterToolStripMenuItem.Click += new System.EventHandler(this.mitarbeiterToolStripMenuItem_Click);
            // 
            // institutionToolStripMenuItem3
            // 
            this.institutionToolStripMenuItem3.Name = "institutionToolStripMenuItem3";
            this.institutionToolStripMenuItem3.Size = new System.Drawing.Size(132, 22);
            this.institutionToolStripMenuItem3.Text = "Institution";
            this.institutionToolStripMenuItem3.Click += new System.EventHandler(this.institutionToolStripMenuItem3_Click);
            // 
            // reaktivierenToolStripMenuItem
            // 
            this.reaktivierenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitarbeiterToolStripMenuItem1,
            this.institutionToolStripMenuItem1});
            this.reaktivierenToolStripMenuItem.Name = "reaktivierenToolStripMenuItem";
            this.reaktivierenToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.reaktivierenToolStripMenuItem.Text = "Reaktivieren";
            // 
            // mitarbeiterToolStripMenuItem1
            // 
            this.mitarbeiterToolStripMenuItem1.Name = "mitarbeiterToolStripMenuItem1";
            this.mitarbeiterToolStripMenuItem1.Size = new System.Drawing.Size(132, 22);
            this.mitarbeiterToolStripMenuItem1.Text = "Mitarbeiter";
            this.mitarbeiterToolStripMenuItem1.Click += new System.EventHandler(this.mitarbeiterToolStripMenuItem1_Click);
            // 
            // institutionToolStripMenuItem1
            // 
            this.institutionToolStripMenuItem1.Name = "institutionToolStripMenuItem1";
            this.institutionToolStripMenuItem1.Size = new System.Drawing.Size(132, 22);
            this.institutionToolStripMenuItem1.Text = "Institution";
            this.institutionToolStripMenuItem1.Click += new System.EventHandler(this.institutionToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(172, 6);
            // 
            // stammdatenpflegeToolStripMenuItem
            // 
            this.stammdatenpflegeToolStripMenuItem.Name = "stammdatenpflegeToolStripMenuItem";
            this.stammdatenpflegeToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.stammdatenpflegeToolStripMenuItem.Text = "Stammdatenpflege";
            this.stammdatenpflegeToolStripMenuItem.Click += new System.EventHandler(this.stammdatenpflegeToolStripMenuItem_Click);
            // 
            // passwortÄndernToolStripMenuItem
            // 
            this.passwortÄndernToolStripMenuItem.Name = "passwortÄndernToolStripMenuItem";
            this.passwortÄndernToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.passwortÄndernToolStripMenuItem.Text = "Passwort ändern";
            this.passwortÄndernToolStripMenuItem.Click += new System.EventHandler(this.passwortÄndernToolStripMenuItem_Click);
            // 
            // statistikToolStripMenuItem
            // 
            this.statistikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statistikAnzeigenToolStripMenuItem,
            this.eigeneAbfrageToolStripMenuItem});
            this.statistikToolStripMenuItem.Name = "statistikToolStripMenuItem";
            this.statistikToolStripMenuItem.Size = new System.Drawing.Size(60, 19);
            this.statistikToolStripMenuItem.Text = "Statistik";
            // 
            // statistikAnzeigenToolStripMenuItem
            // 
            this.statistikAnzeigenToolStripMenuItem.Name = "statistikAnzeigenToolStripMenuItem";
            this.statistikAnzeigenToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.statistikAnzeigenToolStripMenuItem.Text = "Statistik erstellen";
            this.statistikAnzeigenToolStripMenuItem.Click += new System.EventHandler(this.statistikAnzeigenToolStripMenuItem_Click);
            // 
            // eigeneAbfrageToolStripMenuItem
            // 
            this.eigeneAbfrageToolStripMenuItem.Name = "eigeneAbfrageToolStripMenuItem";
            this.eigeneAbfrageToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.eigeneAbfrageToolStripMenuItem.Text = "eigene Abfrage";
            this.eigeneAbfrageToolStripMenuItem.Click += new System.EventHandler(this.eigeneAbfrageToolStripMenuItem_Click);
            // 
            // hilfeToolStripMenuItem1
            // 
            this.hilfeToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kontaktToolStripMenuItem});
            this.hilfeToolStripMenuItem1.Name = "hilfeToolStripMenuItem1";
            this.hilfeToolStripMenuItem1.Size = new System.Drawing.Size(44, 19);
            this.hilfeToolStripMenuItem1.Text = "Hilfe";
            // 
            // kontaktToolStripMenuItem
            // 
            this.kontaktToolStripMenuItem.Name = "kontaktToolStripMenuItem";
            this.kontaktToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.kontaktToolStripMenuItem.Text = "Kontakt";
            this.kontaktToolStripMenuItem.Click += new System.EventHandler(this.hilfeToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Rockwell", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(277, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(659, 43);
            this.label1.TabIndex = 5;
            this.label1.Text = "Willkommen in der Delfi - Datenbank";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(337, 288);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 18);
            this.label3.TabIndex = 7;
            this.label3.Text = "Schnellzugriff";
            // 
            // cbKlienten
            // 
            this.cbKlienten.AutoSize = true;
            this.cbKlienten.Location = new System.Drawing.Point(415, 368);
            this.cbKlienten.Name = "cbKlienten";
            this.cbKlienten.Size = new System.Drawing.Size(94, 19);
            this.cbKlienten.TabIndex = 12;
            this.cbKlienten.Text = "alle Klienten";
            this.cbKlienten.UseVisualStyleBackColor = true;
            this.cbKlienten.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // btn_Klient
            // 
            this.btn_Klient.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_Klient.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Klient.Location = new System.Drawing.Point(179, 330);
            this.btn_Klient.Name = "btn_Klient";
            this.btn_Klient.Size = new System.Drawing.Size(189, 27);
            this.btn_Klient.TabIndex = 17;
            this.btn_Klient.Text = "neuer Klient";
            this.btn_Klient.UseVisualStyleBackColor = false;
            this.btn_Klient.Click += new System.EventHandler(this.neuerKlientToolStripMenuItem_Click);
            // 
            // btn_BefKon
            // 
            this.btn_BefKon.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_BefKon.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_BefKon.Location = new System.Drawing.Point(179, 363);
            this.btn_BefKon.Name = "btn_BefKon";
            this.btn_BefKon.Size = new System.Drawing.Size(189, 27);
            this.btn_BefKon.TabIndex = 18;
            this.btn_BefKon.Text = "neuer Befassungskontext";
            this.btn_BefKon.UseVisualStyleBackColor = false;
            this.btn_BefKon.Click += new System.EventHandler(this.neuenBefassungskontextToolStripMenuItem_Click);
            // 
            // btn_Termin
            // 
            this.btn_Termin.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_Termin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Termin.Location = new System.Drawing.Point(179, 396);
            this.btn_Termin.Name = "btn_Termin";
            this.btn_Termin.Size = new System.Drawing.Size(189, 27);
            this.btn_Termin.TabIndex = 19;
            this.btn_Termin.Text = "neuer Termin";
            this.btn_Termin.UseVisualStyleBackColor = false;
            this.btn_Termin.Click += new System.EventHandler(this.neuToolStripMenuItem2_Click);
            // 
            // pictureBox1
            // 
            //this.pictureBox1.Image = global::Maturaprojekt_Vorschau.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(12, 49);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(242, 155);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // cBMitarbeiter
            // 
            this.cBMitarbeiter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBMitarbeiter.DropDownWidth = 200;
            this.cBMitarbeiter.FormattingEnabled = true;
            this.cBMitarbeiter.Items.AddRange(new object[] {
            "alle"});
            this.cBMitarbeiter.Location = new System.Drawing.Point(415, 333);
            this.cBMitarbeiter.Name = "cBMitarbeiter";
            this.cBMitarbeiter.Size = new System.Drawing.Size(194, 23);
            this.cBMitarbeiter.TabIndex = 21;
            this.cBMitarbeiter.SelectedIndexChanged += new System.EventHandler(this.cB_MA_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 692);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 15);
            this.label5.TabIndex = 20;
            this.label5.Text = "Version 1.0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(131, 452);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(490, 15);
            this.label4.TabIndex = 100;
            this.label4.Text = "_____________________________________________________________________";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(354, 487);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 18);
            this.label6.TabIndex = 102;
            this.label6.Text = "Instiution";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Location = new System.Drawing.Point(415, 531);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(225, 27);
            this.button2.TabIndex = 103;
            this.button2.Text = "Institution suchen/bearbeiten";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.bearbeitenToolStripMenuItem5_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Location = new System.Drawing.Point(179, 531);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(189, 27);
            this.button3.TabIndex = 104;
            this.button3.Text = "Institution hinzufügen";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.neuhinzufuegenToolStripMenuItem_Click);
            // 
            // lblWelcome
            // 
            this.lblWelcome.AutoSize = true;
            this.lblWelcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcome.Location = new System.Drawing.Point(281, 117);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(99, 24);
            this.lblWelcome.TabIndex = 105;
            this.lblWelcome.Text = "Guten Tag";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(111, 692);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 15);
            this.label2.TabIndex = 106;
            this.label2.Text = "Letztes Backup:";
            // 
            // lblBackup
            // 
            this.lblBackup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblBackup.AutoSize = true;
            this.lblBackup.Location = new System.Drawing.Point(210, 692);
            this.lblBackup.Name = "lblBackup";
            this.lblBackup.Size = new System.Drawing.Size(11, 15);
            this.lblBackup.TabIndex = 107;
            this.lblBackup.Text = "-";
            // 
            // Startseite
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.AntiqueWhite;
            this.ClientSize = new System.Drawing.Size(1348, 721);
            this.Controls.Add(this.lblBackup);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblWelcome);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cBMitarbeiter);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_Termin);
            this.Controls.Add(this.btn_BefKon);
            this.Controls.Add(this.btn_Klient);
            this.Controls.Add(this.cbKlienten);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.dgvAnzeige);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Startseite";
            this.Text = "Startseite";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Startseite_FormClosing);
            this.Load += new System.EventHandler(this.Startseite_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Startseite_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnzeige)).EndInit();
            this.cmstStartseite.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvAnzeige;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dateiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem einstellungenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem beendenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statistikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statistikAnzeigenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cbKlienten;
        private System.Windows.Forms.Button btn_Klient;
        private System.Windows.Forms.Button btn_BefKon;
        private System.Windows.Forms.Button btn_Termin;
        private System.Windows.Forms.ComboBox cBMitarbeiter;
        private System.Windows.Forms.ToolStripMenuItem bearbeitenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stammdatenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hilfeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem kontaktToolStripMenuItem;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ToolStripMenuItem neuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem befassungskontextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem terminToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem fortbildungToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sonstigeBefassungenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem telefonischToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem intervisionMitFachwissenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sonstigeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bearbeitenToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem befassungskontextToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem terminToolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem fortbildungToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem sonstigeBefassungenToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem telefonischToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem intervisionMitFachwissenToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sonstigeToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem neuToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem klientToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem msMaCreate;
        private System.Windows.Forms.ToolStripMenuItem institutionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bearbeitenToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem klientToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem msMaEdit;
        private System.Windows.Forms.ToolStripMenuItem institutionToolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator msLine;
        private System.Windows.Forms.ToolStripMenuItem msDelete;
        private System.Windows.Forms.ToolStripMenuItem mitarbeiterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem institutionToolStripMenuItem3;
        private System.Windows.Forms.Label lblWelcome;
        private System.Windows.Forms.ToolStripMenuItem eigeneAbfrageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prozessToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prozessbegleitungToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmstStartseite;
        private System.Windows.Forms.ToolStripMenuItem befassungskontextToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem klientBearbeitenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem terminHinzufuegenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem stammdatenpflegeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reaktivierenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mitarbeiterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem institutionToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem passwortÄndernToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblBackup;
        private System.Windows.Forms.ToolStripMenuItem backupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem täglichesBackupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monatlichesBackupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backupOrdnerÖffnenToolStripMenuItem;
    }
}