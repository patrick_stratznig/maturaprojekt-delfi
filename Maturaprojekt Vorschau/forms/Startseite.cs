﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Tools;
using System.Data.SqlClient;
using System.IO;
using Maturaprojekt_Vorschau.Properties;
using NLog;
using Excel = Microsoft.Office.Interop.Excel;
using System.Diagnostics;


namespace Maturaprojekt_Vorschau
{
    public partial class Startseite : Form
    {
        #region Variablen
        private BindingSource _bs;
        private UserData _currentUser;

        private SqlConnection _con;
        private Configuration _conf;

        private FilterTextBox _txt;
        private BindingSource _bsDgv;

        private MethodRepository _getAdapter;

        private Logger _logger;
        private SelectionBar _filterBar;

        private bool isUserAdmin;
        private string dgvUser = "%";
        private List<string> MitarbeiterAuswahl = new List<string>(); 
        #endregion

        public Startseite()
        {
            InitializeComponent();
        }

        private void einstellungenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Einstellungen ein = new Einstellungen(_con);
            ein.ShowDialog();
        }

        private void Startseite_Load(object sender, EventArgs e)
        {
            try
            {
                _logger = LogManager.GetCurrentClassLogger();

                var conf = new Configuration(Resources.ConfigurationPath);
                this.Font = new Font("Microsoft Sans Serif", conf.getInt("text-size"));

                Anmeldedialog anmelden = new Anmeldedialog();
                if (anmelden.ShowDialog() == DialogResult.OK)                   //Es wird überprüft ob im Anmeldedialog OK oder Abbrechen gedrückt wurde, falls OK gedrückt wurde dann geht das Porgramm weiter. Falls nicht, springt es ins else
                {
                    _con = anmelden.GetCon;             //Die Connection und die Configuration - Datei aus dem Anmeldedialog lesen
                    _conf = anmelden.GetConfig;

                    _getAdapter = new MethodRepository(_con);       //Um später im Programm auf die einzelnen Adapter zugreifen zu können wird hier das Methodrepositiory initiiert



                    var getUser = new SqlCommand("Select * from Mitarbeiter m join user_login u on(m.MNr = u.MNr) where u.Username = @user", _con);     //Die Daten des Mitarbeiters anhand des Benutzernamens herausfinden, und in die Variable _currentUser schreiben
                    getUser.Parameters.Add("@user", SqlDbType.VarChar, 255).Value = anmelden.GetUser;

                    _con.Open();

                    var reader = getUser.ExecuteReader();
                    reader.Read();
                    _currentUser = new UserData(int.Parse(reader[0].ToString()), reader[1].ToString(), reader[2].ToString(), reader[9].ToString());

                    lblWelcome.Text = "Guten Tag, " + _currentUser.GetVorname + " " + _currentUser.GetNachname + "!";       //Den Benutzer mit Vor- und Nachnamen grüßen
                    reader.Close();


                    #region isUserAdmin     
                    var command = new SqlCommand("Select isAdmin from user_login where username = @user", _con);        //Hier wird geprüft ob der angemeldete Benutzer Adminrechte besitzt
                    command.Parameters.Add("@user", SqlDbType.VarChar, 255).Value = anmelden.GetUser;                   //Hat er diese nicht, werden die Buttons zum Anlegen und Bearbeiten eines Mitarbeiters nicht angezeigt, da ansonsten bei einem Versuch einen MA anzulegen nur eine FEhlermeldung bekommen würde

                    isUserAdmin = Convert.ToBoolean(command.ExecuteScalar().ToString());                                //Ob der Benutzer Admin ist wird auch noch für später gespeichert

                    if (!isUserAdmin)
                        msLine.Visible = msMaEdit.Visible = msDelete.Visible = msMaCreate.Visible = reaktivierenToolStripMenuItem.Visible = passwortÄndernToolStripMenuItem.Visible = false;

                    #endregion

                    #region ComboBox füllen
                    SqlDataReader r = new SqlCommand("select Nachname + ' ' + Vorname, MNr as 'Name' from Mitarbeiter where MNr > 0", _con).ExecuteReader();             //Die Namen der Mitarbeiter werden in die ComboBox geladen

                    MitarbeiterAuswahl.Add("%");

                    while (r.Read())
                    {
                        cBMitarbeiter.Items.Add(r[0]);
                        MitarbeiterAuswahl.Add(r[1].ToString());
                    }

                    r.Close();
                    cBMitarbeiter.SelectedIndex = 0;
                    cB_MA_SelectedIndexChanged(null, null);
                    #endregion

                    #region Geburtstagswunsch

                    var datum = _currentUser.GetGeburtsdatum;           //War ein Wunsch des Delfi - Teams. Hat der Benutzer Geburtstag wird ihm/ihr gratuliert

                    if (!string.IsNullOrEmpty(datum) && datum.Substring(0, 5) == DateTime.Now.ToString().Substring(0, 5))
                        MessageBox.Show("Alles Gute zum Geburtstag " + _currentUser.GetVorname, "Glückwunsch");

                    #endregion

                    #region Backup
                    var path = _conf.getString("backup-path").ToString();

                    if (!Directory.Exists(path))                                                                          //hier wird überprüft ob der folderPath schon existiert
                        Directory.CreateDirectory(path);

                    var files = Directory.GetFiles(path);
                    #endregion

                }
                else
                    Close();               //Hier wird einfach ein Close() ausgeführt, und somit das Programm geschlossen

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }

        }

        private void hilfeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Resources.HilfeText);
        }


        private void beendenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void neuenBefassungskontextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var dt = new DataTable();
                _getAdapter.AdaptClient.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;

                //neue Auswahl aller Klienten
                Auswahl auswahl = new Auswahl(_bs, _con, "Client");
                if (auswahl.ShowDialog() == DialogResult.OK)
                {
                    BefKon befKon = new BefKon(_con, auswahl.GetId, "new");
                    befKon.ShowDialog();
                    befKon.Dispose();
                }
                cB_MA_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void neuerMitarbeiterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var dt = new DataTable();
                _getAdapter.AdaptMa.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;

                _bs.AddNew();
                using (Mitarbeiter ma = new Mitarbeiter(_con, _bs, ref dt))
                {
                    //Mitarbeiter ma = new Mitarbeiter(_con, _bs, ref dt);
                    ma.ShowDialog();
                   
                }
               

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }

        }

        private void fortbildungEintragenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var dt = new DataTable();
                _getAdapter.AdaptEvent.Fill(dt);

                _bs = new BindingSource
                {
                    DataSource = dt
                };

                _bs.AddNew();

                Event ev = new Event(_con, _bs, ref dt, isUserAdmin);
                ev.ShowDialog();

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void klientenBearbeitenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var dtClient = new DataTable();
                _getAdapter.AdaptClient.Fill(dtClient);

                _bs = new BindingSource();
                _bs.DataSource = dtClient;

                Auswahl ausw = new Auswahl(_bs, _con,"Client");

                if (ausw.ShowDialog() == DialogResult.OK)
                {
                    Client ck = new Client(_bs, _con, ausw.GetType, ref dtClient);
                    ck.ShowDialog();
                    ck.Dispose();
                }
                ausw.Dispose();
                cB_MA_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        /*Es wird bei fast jeder neuen Form die Bindingsource mitgegeben, die SqlConnection, ein Typ und ein DataTable.
         * Der GRund für die Bindingsource ist, damit man beim Speichern die EndEdit() - Methode aufrufen kann, oder beim Verwerfen die CancelEdit() - Methode
         * Die Connection falls noch andere SqlStatements ausgeführt werden müssen
         * Der Typ ist bei jeder Form unterschiedlich, dient aber im Grunde dazu, damit das Programm weiß was als nächstes gemacht werden muss, ob z.B. ein Klient oder ein Angehöriger angelegt wird
         * Der Datatable wird mitgegeben damit man mittels dem passenden DataAdapter (dieser wird aus der Klasse MethodRrpository genommen) die Daten speichern kann
         * */

        private void befassungskontextBearbeitenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var dt = new DataTable();
                _getAdapter.AdaptBefKon.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;

                Auswahl auswahl = new Auswahl(_bs, _con, "BefKon");
                if (auswahl.ShowDialog() == DialogResult.OK)
                {
                    BefKon b = new BefKon(_con, auswahl.GetId, "old");
                    b.Show();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void cB_MA_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dtx = new DataTable();
                SqlDataAdapter adpt;
                if(!cbKlienten.Checked)
                if (cBMitarbeiter.Text != "alle")
                {
                    adpt = new SqlDataAdapter("select Convert(varchar(255),b.BefKonNr,104) 'BefKonNr',p.PNr, p.Nachname, p.Vorname, Convert(varchar(25),p.Geburtsdatum,104) 'Geburtstag' from Person p join BefKon b on(p.PNr = b.PNr) join Mitarbeiter m on (b.Fallfuehrender = m.MNr) where p.PTyp='IK' AND m.MNr = @para and b.Datum_Ende is null", _con);
                    adpt.SelectCommand.Parameters.Add("@para", SqlDbType.VarChar, 50, "Nachname").Value = MitarbeiterAuswahl[cBMitarbeiter.SelectedIndex];
                }
                else
                    adpt = new SqlDataAdapter("select Convert(varchar(255),b.BefKonNr,104) 'BefKonNr',p.PNr, p.Nachname, p.Vorname, Convert(varchar(25),p.Geburtsdatum,104) 'Geburtstag' from Person p join BefKon b on(p.PNr = b.PNr) join Mitarbeiter m on (b.Fallfuehrender = m.MNr) where p.PTyp='IK' AND m.MNr >= 0 and b.Datum_Ende is null", _con);
               else
                    if (cBMitarbeiter.Text != "alle")
                {
                    adpt = new SqlDataAdapter("select Convert(varchar(255),b.BefKonNr,104) 'BefKonNr',p.PNr, p.Nachname, p.Vorname, Convert(varchar(25),p.Geburtsdatum,104) 'Geburtstag' from Person p join BefKon b on(p.PNr = b.PNr) join Mitarbeiter m on (b.Fallfuehrender = m.MNr) where p.PTyp='IK' AND m.MNr = @para", _con);
                    adpt.SelectCommand.Parameters.Add("@para", SqlDbType.VarChar, 50, "Nachname").Value = MitarbeiterAuswahl[cBMitarbeiter.SelectedIndex];
                }
                else
                    adpt = new SqlDataAdapter("select Convert(varchar(255),b.BefKonNr,104) 'BefKonNr',p.PNr, p.Nachname, p.Vorname, Convert(varchar(25),p.Geburtsdatum,104) 'Geburtstag' from Person p join BefKon b on(p.PNr = b.PNr) join Mitarbeiter m on (b.Fallfuehrender = m.MNr) where p.PTyp='IK' AND m.MNr >= 0", _con);

                adpt.Fill(dtx);
                _bsDgv = new BindingSource();
                _bsDgv.DataSource = dtx;
                dgvAnzeige.DataSource = _bsDgv;
                dgvAnzeige.Refresh();

                _filterBar = new SelectionBar(dgvAnzeige);

                _txt = new FilterTextBox(dgvAnzeige, this);
                _txt.ValChangedHandler += txt_TextChanged;
            }catch(Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }
        private void txt_TextChanged(object sender, FilterTextBox.TextChangeEventArgs e)
        {
            try
            {
                string filter = "";

                for (int i = 0; i < dgvAnzeige.Columns.Count; i++)
                {
                    if (_txt[i] != "")
                    {
                        if (filter != "")
                            filter += " AND " + dgvAnzeige.Columns[i].HeaderText + " like '*" + _txt[i] + "*'";
                        else
                            filter = dgvAnzeige.Columns[i].HeaderText + " like '*" + _txt[i] + "*'";
                    }
                }
                _bsDgv.Filter = filter;
            }catch(Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }

        }


        private void bestehendenTerminBearbeitenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var dt = new DataTable();
                _getAdapter.AdaptTermin.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;

                Auswahl id = new Auswahl(_bs, _con, "Termin");
                if (id.ShowDialog() == DialogResult.OK)
                {
                    Termin termin = new Termin(_con, _bs, id.GetId, _currentUser.GetMNr.ToString(), ref dt);
                    termin.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }

        }

        private void mitarbeiterBearbeitenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var dt = new DataTable();
                _getAdapter.AdaptMa.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;

                Auswahl auswahl = new Auswahl(_bs, _con, "Mitarbeiter");
                if (auswahl.ShowDialog() == DialogResult.OK)
                {
                    Mitarbeiter ma = new Mitarbeiter(_con, _bs, ref dt);
                    if (ma.ShowDialog() == DialogResult.OK)
                        _getAdapter.AdaptMa.Update(dt);
                }


            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void neuerKlientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var dt = new DataTable();
                _getAdapter.AdaptClient.Fill(dt);


                _bs = new BindingSource();
                _bs.DataSource = dt;
                _bs.AddNew();


                Client client = new Client(_bs, _con, "Client", ref dt);
                if (client.ShowDialog() == DialogResult.OK)
                {
                    _getAdapter.AdaptClient.Update(dt);
                    if (
                        MessageBox.Show("Wollen Sie gleich einen neuen Befassungskontext erstellen?",
                            Resources.MessageBoxHeaderHinweis, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        var BefKon = new BefKon(_con, client.PNr, "new");
                        BefKon.ShowDialog();
                    }
                }


            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }



        private void neuToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            try
            {
                var dt = new DataTable();
                _getAdapter.AuswahlBefKon.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;

                Auswahl id = new Auswahl(_bs, _con);
                if (id.ShowDialog() == DialogResult.OK)
                {
                    var dtTermin = new DataTable();
                    _getAdapter.AdaptTermin.Fill(dtTermin);

                    _bs = new BindingSource();
                    _bs.DataSource = dtTermin;
                    _bs.AddNew();

                    Termin termin = new Termin(_con, _bs, id.GetId, _currentUser.GetMNr.ToString(), ref dtTermin);
                    termin.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }


        private void neuhinzufuegenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var dt = new DataTable();
                _getAdapter.AdaptInst.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;

                _bs.AddNew();

                Institution inst = new Institution(_con, _bs, ref dt);

                if (inst.ShowDialog() == DialogResult.OK)
                    _getAdapter.AdaptInst.Update(dt);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void bearbeitenToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            try
            {
                var dt = new DataTable();
                _getAdapter.AdaptInst.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;

                Auswahl aus = new Auswahl(_bs, _con, "INST");
                if (aus.ShowDialog() == DialogResult.OK)
                {
                    Institution inst = new Institution(_con, _bs, ref dt);
                    inst.ShowDialog();
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }
             

        private void fortbildungToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            try
            {
                var dt = new DataTable();
                _getAdapter.AdaptEvent.Fill(dt);

                _bs = new BindingSource
                {
                    DataSource = dt
                };

                Auswahl auswahl = new Auswahl(_bs, _con,"Event");
                if (auswahl.ShowDialog() == DialogResult.OK)
                {
                    Event ev = new Event(_con, _bs, ref dt, isUserAdmin);
                    ev.ShowDialog();

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void Startseite_FormClosing(object sender, FormClosingEventArgs e)
        {
           
        }

        private void dataGridView1_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            _txt.resizeColumns(dgvAnzeige);
        }

        private void Startseite_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawLine(Pens.Black, 0, dgvAnzeige.Location.Y + dgvAnzeige.Height - 1, dgvAnzeige.Location.X, dgvAnzeige.Location.Y + dgvAnzeige.Height - 1);
        }

        private void eigeneAbfrageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                NewCommandBuilder command = new NewCommandBuilder(_con);
                command.ShowDialog();

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void prozessToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var adapter = _getAdapter.AdaptProzess;
                var dt = new DataTable();
                adapter.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;
                _bs.AddNew();

                Client client = new Client(_bs, _con, "Prozessbegleitung", ref dt);
                if (client.ShowDialog() == DialogResult.OK)
                    adapter.Update(dt);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void prozessbegleitungToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var adapter = _getAdapter.AdaptProzess;
                var dt = new DataTable();
                adapter.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;

                Auswahl auswahl = new Auswahl(_bs, _con, "Client");
                if (auswahl.ShowDialog() == DialogResult.OK)
                {
                    Client client = new Client(_bs, _con, "Prozessbegleitung", ref dt);
                    if (client.ShowDialog() == DialogResult.OK)
                        adapter.Update(dt);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void terminHinzufuegenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dt = new DataTable();
            _getAdapter.AuswahlBefKon.Fill(dt);

            _bs = new BindingSource();
            _bs.DataSource = dt;

            var dtTermin = new DataTable();
            _getAdapter.AdaptTermin.Fill(dtTermin);

            _bs = new BindingSource();
            _bs.DataSource = dtTermin;
            _bs.AddNew();

            Termin termin = new Termin(_con, _bs, dgvAnzeige.SelectedRows[0].Cells[0].Value.ToString(), _currentUser.GetMNr.ToString(), ref dtTermin);
            termin.ShowDialog();
        }

        private void stammdatenpflegeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Stammdatenpflege sp = new Stammdatenpflege(_con);
                sp.ShowDialog();
                sp.Dispose();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void telefonischToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                var adaptTel = _getAdapter.AdpaptTelefon;
                var dtTel = new DataTable();
                adaptTel.Fill(dtTel);

                _bs = new BindingSource();
                _bs.DataSource = dtTel;
                _bs.AddNew();

                Client client = new Client(_bs, _con, "Telefon", ref dtTel);
                client.ShowDialog();
                   
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void intervisionMitFachwissenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var adapter = _getAdapter.AdaptIntervision;
                var dt = new DataTable();
                adapter.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;
                _bs.AddNew();

                Client client = new Client(_bs, _con, "Intervision", ref dt);
                if (client.ShowDialog() == DialogResult.OK)
                    adapter.Update(dt);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void sonstigeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var adapter = _getAdapter.AdaptSonstige;
                var dt = new DataTable();
                adapter.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;
                _bs.AddNew();

                Client client = new Client(_bs, _con, "Sonstige", ref dt);
                if (client.ShowDialog() == DialogResult.OK)
                    adapter.Update(dt);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void telefonischToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            try
            {
                var adapter = _getAdapter.AdpaptTelefon;
                var dt = new DataTable();
                adapter.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;

                Auswahl auswahl = new Auswahl(_bs, _con, "Client");
                if (auswahl.ShowDialog() == DialogResult.OK)
                {
                    Client client = new Client(_bs, _con, "Telefon", ref dt);
                    if (client.ShowDialog() == DialogResult.OK)
                        adapter.Update(dt);
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void intervisionMitFachwissenToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                var adapter = _getAdapter.AdaptIntervision;
                var dt = new DataTable();
                adapter.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;

                Auswahl auswahl = new Auswahl(_bs, _con, "Client");
                if (auswahl.ShowDialog() == DialogResult.OK)
                {
                    Client client = new Client(_bs, _con, "Intervision", ref dt);
                    if (client.ShowDialog() == DialogResult.OK)
                        adapter.Update(dt);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void sonstigeToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            try
            {
                var adapter = _getAdapter.AdaptSonstige;
                var dt = new DataTable();
                adapter.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;

                Auswahl auswahl = new Auswahl(_bs, _con, "Client");
                if (auswahl.ShowDialog() == DialogResult.OK)
                {
                    Client client = new Client(_bs, _con, "Sonstige", ref dt);
                    if (client.ShowDialog() == DialogResult.OK)
                        adapter.Update(dt);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void institutionToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            try
            {
                var dt = new DataTable();
                _getAdapter.AdaptInst.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;

                Auswahl aus = new Auswahl(_bs, _con, "INST");
                if (aus.ShowDialog() == DialogResult.OK)
                {
                    if (MessageBox.Show("Sind Sie sich ganz sicher, dass Sie diese Institution löschen wollen? (Diese wird nicht vollständig gelöscht. Unter Stammdaten -> Reaktivieren -> Institution kann diese wieder reaktiviert werden!)", Resources.MessageBoxHeaderHinweis, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        new SqlCommand("Update Institution set geloescht = 1 where INr = " + aus.GetId, _con).ExecuteNonQuery();
                        MessageBox.Show("Diese Institution wurde erfolgreich gelöscht!", Resources.MessageBoxHeaderHinweis);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void persönlicheDatenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (_currentUser != null)
                {
                    string Message = "Mitarbeiternummer: \t" + _currentUser.GetMNr + Environment.NewLine +
                        "Nachname: \t\t" + _currentUser.GetNachname + Environment.NewLine +
                        "Vorname: \t\t" + _currentUser.GetVorname + Environment.NewLine +
                        "Geburtsdatum: \t\t" + _currentUser.GetGeburtsdatum + Environment.NewLine + Environment.NewLine;

                    if (!isUserAdmin)
                        Message += "Für weitere Daten oder Änderungen wenden Sie sich bitte an einen Admin";
                    else
                        Message += "Für weitere Daten oder Änderungen gehen Sie bitte auf Stammdaten -> Bearbeiten -> Mitarbeiter";
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void mitarbeiterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SqlTransaction trans = null;
            try
            {
                var dt = new DataTable();
                _getAdapter.AdaptMa.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;

                Auswahl auswahl = new Auswahl(_bs, _con, "Mitarbeiter");
                if (auswahl.ShowDialog() == DialogResult.OK)
                {
                    if (MessageBox.Show("Sind Sie sich ganz sicher, dass Sie diesen Mitarbeiter löschen wollen? (Er/Sie wird nicht vollständig gelöscht. Unter Stammdaten -> Reaktivieren -> Mitarbeiter kann er wieder reaktiviert werden!)", Resources.MessageBoxHeaderHinweis, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        trans = _con.BeginTransaction();
                        new SqlCommand("Update Mitarbeiter set geloescht = 1 where MNr = " + auswahl.GetId, _con)
                        {
                            Transaction = trans
                        }.ExecuteNonQuery();

                        var Username = new SqlCommand("select Username from user_login where MNr =" + auswahl.GetId, _con)
                        {
                            Transaction = trans
                        }.ExecuteScalar().ToString();

                        new SqlCommand("ALTER LOGIN [" + Username + "] DISABLE", _con)
                        {
                            Transaction = trans
                        }.ExecuteNonQuery();

                        trans.Commit();
                        MessageBox.Show("Der Mitarbeiter wurde erfolgreich gelöscht!", Resources.MessageBoxHeaderHinweis);
                    }
                }
            }
            catch (Exception ex)
            {
                trans.Rollback();
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void mitarbeiterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SqlTransaction trans = null;
            try
            {
                var adapter = new MethodRepository(_con, true);

                var dt = new DataTable();
                adapter.AdaptMa.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;


                Auswahl auswahl = new Auswahl(_bs, _con, "Mitarbeiter");
                if (auswahl.ShowDialog() == DialogResult.OK)
                {
                    if (MessageBox.Show("Sind Sie sich ganz sicher, dass Sie diesen Mitarbeiter reaktivieren wollen?", Resources.MessageBoxHeaderHinweis, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        trans = _con.BeginTransaction();
                        new SqlCommand("Update Mitarbeiter set geloescht = 0 where MNr = " + auswahl.GetId, _con)
                        {
                            Transaction = trans
                        }.ExecuteNonQuery();

                        var Username = new SqlCommand("select Username from user_login where MNr =" + auswahl.GetId, _con)
                        {
                            Transaction = trans
                        }.ExecuteScalar().ToString();

                        new SqlCommand("ALTER LOGIN [" + Username + "] ENABLE", _con)
                        {
                            Transaction = trans
                        }.ExecuteNonQuery();

                        trans.Commit();
                        MessageBox.Show("Der Mitarbeiter wurde erfolgreich reaktiviert!", Resources.MessageBoxHeaderHinweis);
                    }
                }
            }
            catch (Exception ex)
            {
                trans.Rollback();
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void institutionToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                var adapter = new MethodRepository(_con, true);

                var dt = new DataTable();
                adapter.AdaptInst.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;

                Auswahl aus = new Auswahl(_bs, _con, "INST");
                if (aus.ShowDialog() == DialogResult.OK)
                {
                    if (MessageBox.Show("Sind Sie sich ganz sicher, dass Sie diese Institution reaktivieren wollen?", Resources.MessageBoxHeaderHinweis, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        new SqlCommand("Update Institution set geloescht = 0 where INr = " + aus.GetId, _con).ExecuteNonQuery();
                        MessageBox.Show("Diese Institution wurde erfolgreich reaktiviert!", Resources.MessageBoxHeaderHinweis);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void statistikAnzeigenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (_con.State == ConnectionState.Closed)
                    _con.Open();

                DatePicker dp = new DatePicker();

                if (dp.ShowDialog() == DialogResult.OK)
                {
                    string Beginn = dp.Beginn.ToString("yyyy-MM-dd");
                    string Ende = dp.Ende.ToString("yyyy-MM-dd");

                    var ExcelApp = new Excel.Application();
                    ExcelApp.Visible = true;
                    ExcelApp.WindowState = Excel.XlWindowState.xlMaximized;
                    ExcelApp.Workbooks.Add();
                    Excel._Worksheet workSheet = (Excel.Worksheet)ExcelApp.ActiveSheet;

                    SqlDataReader rdr;
                    var row = new int();

                    #region Überweisende Einrichtungen

                    workSheet.Name = "Überweisende Einrichtungen";
                    workSheet.Cells[1, "A"] = "Anzahl";
                    workSheet.Cells[1, "B"] = "Bezeichnung";


                    rdr = new SqlCommand("select count(*) 'Anzahl', i.Bezeichnung from Person p, BefKon b, Institution i where p.PNr = b.PNr AND b.ITyp_Ueberweiser = i.ITyp AND b.Ueberweiser = i.INr AND Anmeldedatum BETWEEN '" + Beginn + "' AND '" + Ende + "' GROUP BY i.Bezeichnung", _con).ExecuteReader();

                    row = 1;
                    while (rdr.Read())
                    {
                        row++;
                        workSheet.Cells[row, "A"] = rdr[0].ToString();
                        workSheet.Cells[row, "B"] = rdr[1].ToString();
                    }

                    workSheet.Columns[1].AutoFit();
                    workSheet.Columns[2].AutoFit();

                    rdr.Close();

                    #endregion
                    #region Anzahl der miteinbezogenen Institutionen

                    ExcelApp.Worksheets.Add();
                    workSheet = (Excel.Worksheet)ExcelApp.ActiveSheet;
                    workSheet.Name = "Anzahl MEI";

                    workSheet.Cells[1, "A"] = "Anzahl miteinbezogener Institutionen";
                    workSheet.Cells[1, "B"] = "Häufigkeit";

                    rdr = new SqlCommand("select Anzahl 'Anzahl miteinbezogener Institutionen', count(*) 'Häufigkeit' from (select b.BefKonNr, count(*) 'Anzahl' from BefKon b, Person p, Institution i, BefKon_Bez_Institution bi where p.PNr = b.PNr AND bi.BefKonNr = b.BefKonNr AND bi.Bezug_Nr = i.INr AND bi.Bezug_Typ = i.ITyp AND Anmeldedatum BETWEEN '" + Beginn + "' AND '" + Ende + "' GROUP BY b.BefKonNr) AS t1 GROUP BY Anzahl", _con).ExecuteReader();

                    row = 1;
                    while (rdr.Read())
                    {
                        row++;
                        workSheet.Cells[row, "A"] = rdr[0].ToString();
                        workSheet.Cells[row, "B"] = rdr[1].ToString();
                    }

                    workSheet.Columns[1].AutoFit();
                    workSheet.Columns[2].AutoFit();
                    rdr.Close();

                    #endregion
                    #region Anzahl der miteinbezogenen Angehörigen

                    ExcelApp.Worksheets.Add();
                    workSheet = (Excel.Worksheet)ExcelApp.ActiveSheet;
                    workSheet.Name = "Anzahl MEA";

                    workSheet.Cells[1, "A"] = "miteinbezogene Angehörige";
                    workSheet.Cells[1, "B"] = "Häufigkeit";

                    rdr = new SqlCommand("select Anzahl 'miteinbezogene Angehörige', count(*) 'Häufigkeit' from(select b.BefKonNr, count(*) 'Anzahl' from BefKon b, Person p, Person a, BefKon_Bez_Person ba where p.PNr = b.PNr AND ba.BefKonNr = b.BefKonNr AND ba.Bezug_Nr = a.PNr AND ba.Bezug_Typ = a.PTyp AND Anmeldedatum  BETWEEN '" + Beginn + "' AND '" + Ende + "' GROUP BY b.BefKonNr) as t1 GROUP BY Anzahl", _con).ExecuteReader();

                    row = 1;
                    while (rdr.Read())
                    {
                        row++;
                        workSheet.Cells[row, "A"] = rdr[0].ToString();
                        workSheet.Cells[row, "B"] = rdr[1].ToString();
                    }

                    workSheet.Columns[1].AutoFit();
                    workSheet.Columns[2].AutoFit();
                    rdr.Close();

                    #endregion
                    #region Welche Institutionen waren miteinbezogen

                    ExcelApp.Worksheets.Add();
                    workSheet = (Excel.Worksheet)ExcelApp.ActiveSheet;
                    workSheet.Name = "MEI";

                    workSheet.Cells[1, "A"] = "Bezeichnung";
                    workSheet.Cells[1, "B"] = "Anzahl";

                    rdr = new SqlCommand("select i.Bezeichnung, count(*) 'Anzahl' from BefKon b, Person p, Institution i, BefKon_Bez_Institution bi where p.PNr = b.PNr AND bi.BefKonNr = b.BefKonNr AND bi.Bezug_Nr = i.INr AND bi.Bezug_Typ = i.ITyp AND Anmeldedatum BETWEEN '" + Beginn + "' AND '" + Ende + "' GROUP BY i.Bezeichnung", _con).ExecuteReader();

                    row = 1;
                    while (rdr.Read())
                    {
                        row++;
                        workSheet.Cells[row, "A"] = rdr[0].ToString();
                        workSheet.Cells[row, "B"] = rdr[1].ToString();
                    }

                    workSheet.Columns[1].AutoFit();
                    workSheet.Columns[2].AutoFit();
                    rdr.Close();

                    #endregion
                    #region Behandlungskategorien

                    ExcelApp.Worksheets.Add();
                    workSheet = (Excel.Worksheet)ExcelApp.ActiveSheet;
                    workSheet.Name = "Behandlungskategorien";

                    workSheet.Cells[1, "A"] = "Kategorie";
                    workSheet.Cells[1, "B"] = "Anzahl";

                    rdr = new SqlCommand("select Kategorie, count(*) 'Anzahl' from BefKon b, Interventionskategorien i where b.Interventionskategorie = i.Kennzahl AND Anmeldedatum BETWEEN '" + Beginn + "' AND '" + Ende + "' group by Kategorie", _con).ExecuteReader();

                    row = 1;
                    while (rdr.Read())
                    {
                        row++;
                        workSheet.Cells[row, "A"] = rdr[0].ToString();
                        workSheet.Cells[row, "B"] = rdr[1].ToString();
                    }

                    workSheet.Columns[1].AutoFit();
                    workSheet.Columns[2].AutoFit();
                    rdr.Close();

                    #endregion
                    #region Settings bei Terminen

                    ExcelApp.Worksheets.Add();
                    workSheet = (Excel.Worksheet)ExcelApp.ActiveSheet;
                    workSheet.Name = "Settings Klientenkontakte";

                    workSheet.Cells[1, "A"] = "Typ";
                    workSheet.Cells[1, "B"] = "Anzahl";

                    rdr = new SqlCommand("select Typ, count(*) 'Anzahl' from Termin t, Termin_Setting ts where t.BefKonNr = ts.BefKonNr AND ts.TNr = t.TNr AND Datum  BETWEEN '" + Beginn + "' AND '" + Ende + "' group by Typ", _con).ExecuteReader();

                    row = 1;
                    while (rdr.Read())
                    {
                        row++;
                        workSheet.Cells[row, "A"] = rdr[0].ToString();
                        workSheet.Cells[row, "B"] = rdr[1].ToString();
                    }

                    workSheet.Columns[1].AutoFit();
                    workSheet.Columns[2].AutoFit();
                    rdr.Close();

                    #endregion
                    #region Kontakte nach Befassungskategorie

                    ExcelApp.Worksheets.Add();
                    workSheet = (Excel.Worksheet)ExcelApp.ActiveSheet;
                    workSheet.Name = "Kontakte Befassungskategorie";

                    workSheet.Cells[1, "A"] = "Anzahl";
                    workSheet.Cells[1, "B"] = "Kategorie";

                    rdr = new SqlCommand("select count(*) 'Anzahl', a.Kategorie from Termin t, Angebotskategorie a where t.Angebotskategorie = a.lfdNr AND t.Datum BETWEEN '" + Beginn + "' AND '" + Ende + "' group by a.Kategorie", _con).ExecuteReader();

                    row = 1;
                    while (rdr.Read())
                    {
                        row++;
                        workSheet.Cells[row, "A"] = rdr[0].ToString();
                        workSheet.Cells[row, "B"] = rdr[1].ToString();
                    }

                    workSheet.Columns[1].AutoFit();
                    workSheet.Columns[2].AutoFit();
                    rdr.Close();

                    #endregion
                    #region Zusammensetzung der Klienten

                    ExcelApp.Worksheets.Add();
                    workSheet = (Excel.Worksheet)ExcelApp.ActiveSheet;
                    workSheet.Name = "Zusammensetzung Klienten";

                    workSheet.Cells[1, "A"] = "laufende Klienten diesen Jahres";
                    workSheet.Cells[1, "B"] = "laufende Klienten letzten Jahres";
                    workSheet.Cells[1, "C"] = "laufende Klienten vorletzten Jahres";

                    rdr = new SqlCommand("select (select count(*) from Person p, BefKon b where p.PNr = b.PNr  AND b.Datum_Ende = null AND YEAR(b.Anmeldedatum) = YEAR(CURRENT_TIMESTAMP)) 'laufende Klienten diesen Jahres' , (select count(*) from Person p, BefKon b where p.PNr = b.PNr AND b.Datum_Ende = null AND YEAR(b.Anmeldedatum) =  (YEAR(CURRENT_TIMESTAMP) - 1)) 'laufende Klienten letzten Jahres', (select count(*)  from Person p, BefKon b where p.PNr = b.PNr AND b.Datum_Ende = null AND YEAR(b.Anmeldedatum) =  (YEAR(CURRENT_TIMESTAMP) - 2)) 'laufende Klienten vorletzten Jahres'", _con).ExecuteReader();

                    row = 1;
                    while (rdr.Read())
                    {
                        row++;
                        workSheet.Cells[row, "A"] = rdr[0].ToString();
                        workSheet.Cells[row, "B"] = rdr[1].ToString();
                        workSheet.Cells[row, "C"] = rdr[1].ToString();
                    }

                    workSheet.Columns[1].AutoFit();
                    workSheet.Columns[2].AutoFit();
                    workSheet.Columns[3].AutoFit();
                    rdr.Close();

                    #endregion
                    #region regionale Aufteilung

                    ExcelApp.Worksheets.Add();
                    workSheet = (Excel.Worksheet)ExcelApp.ActiveSheet;
                    workSheet.Name = "regionale Verteilung";

                    workSheet.Cells[1, "A"] = "Ort";
                    workSheet.Cells[1, "B"] = "Häufigkeit";

                    rdr = new SqlCommand("select Ort, count(*) 'Verteilung Ort' from Person p, BefKon b where b.PNr = p.PNr AND b.Anmeldedatum BETWEEN '" + Beginn + "' AND '" + Ende + "' GROUP BY Ort", _con).ExecuteReader();

                    row = 1;
                    while (rdr.Read())
                    {
                        row++;
                        workSheet.Cells[row, "A"] = rdr[0].ToString();
                        workSheet.Cells[row, "B"] = rdr[1].ToString();
                    }

                    workSheet.Columns[1].AutoFit();
                    workSheet.Columns[2].AutoFit();
                    rdr.Close();

                    #endregion
                    #region Aufteilung erwachsen/minderjährig

                    ExcelApp.Worksheets.Add();
                    workSheet = (Excel.Worksheet)ExcelApp.ActiveSheet;
                    workSheet.Name = "Altersaufteilung";

                    workSheet.Cells[1, "A"] = "erwachsen";
                    workSheet.Cells[1, "B"] = "minderjährig";
                    workSheet.Cells[5, "A"] = "insgesamt";

                    rdr = new SqlCommand("select  (select count(*) from Person p, BefKon b where p.PNr = b.PNr AND DATEDIFF(YEAR, Geburtsdatum, Convert(date, CURRENT_TIMESTAMP)) >= 18 AND p.PNr like CONCAT(YEAR(CURRENT_TIMESTAMP), '%') AND p.PTyp = 'IK' AND Anmeldedatum BETWEEN '" + Beginn + "' AND '" + Ende + "') 'erwachsen' , (select count(*) from Person p, BefKon b where p.PNr = b.PNr AND DATEDIFF(YEAR, Geburtsdatum, Convert(date, CURRENT_TIMESTAMP)) < 18 AND p.PNr like CONCAT(YEAR(CURRENT_TIMESTAMP), '%') AND p.PTyp = 'IK' AND Anmeldedatum BETWEEN '" + Beginn + "' AND '" + Ende + "') 'minderjährig'", _con).ExecuteReader();

                    row = 1;
                    while (rdr.Read())
                    {
                        row++;
                        workSheet.Cells[row, "A"] = rdr[0].ToString();
                        workSheet.Cells[row, "B"] = rdr[1].ToString();
                    }

                    workSheet.Cells[5, "B"] = "=A2+B2";
                    workSheet.Columns[1].AutoFit();
                    workSheet.Columns[2].AutoFit();
                    rdr.Close();

                    #endregion
                    #region Aufteilung Minderjährige

                    ExcelApp.Worksheets.Add();
                    workSheet = (Excel.Worksheet)ExcelApp.ActiveSheet;
                    workSheet.Name = "Aufteilung Minderjährige";

                    workSheet.Cells[1, "A"] = "unter 6 Jahren";
                    workSheet.Cells[2, "A"] = "Geschlecht";
                    workSheet.Cells[2, "B"] = "Anzahl";

                    rdr = new SqlCommand("select Geschlecht, (select count(*) from Person p, BefKon b where b.PNr = p.PNr AND DATEDIFF(YEAR, Geburtsdatum, Convert(date, CURRENT_TIMESTAMP)) < 6 AND Anmeldedatum BETWEEN '" + Beginn + "' AND '" + Ende + "') from Person GROUP by Geschlecht", _con).ExecuteReader();

                    row = 2;
                    while (rdr.Read())
                    {
                        row++;
                        workSheet.Cells[row, "A"] = rdr[0].ToString();
                        workSheet.Cells[row, "B"] = rdr[1].ToString();
                    }
                    rdr.Close();

                    //So würde man ein Kuchendiagramm erstellen
                    //#region Kuchendiagramm unter 6 Jahre
                    //// Add chart.
                    //var charts = workSheet.ChartObjects() as
                    //    Microsoft.Office.Interop.Excel.ChartObjects;
                    //var chartObject = charts.Add(150, 0, 250, 250) as
                    //    Microsoft.Office.Interop.Excel.ChartObject;

                    //var chart = chartObject.Chart;

                    //// Set chart range.
                    //var range = workSheet.get_Range("A3:B3", "A4:B4");          

                    //// Set chart properties.
                    //chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlPie;
                    //chart.ChartWizard(Source: range, Title: "unter 6 Jahren");

                    //#endregion


                    row += 2;

                    workSheet.Cells[row + 1, "A"] = "unter 10 Jahren";
                    workSheet.Cells[row + 2, "A"] = "Geschlecht";
                    workSheet.Cells[row + 2, "B"] = "Anzahl";

                    rdr = new SqlCommand("select Geschlecht, (select count(*) from Person p, BefKon b where b.PNr = p.PNr AND DATEDIFF(YEAR, Geburtsdatum, Convert(date, CURRENT_TIMESTAMP)) >5 AND DATEDIFF(YEAR, Geburtsdatum, Convert(date, CURRENT_TIMESTAMP)) < 10 AND Anmeldedatum BETWEEN '" + Beginn + "' AND '" + Ende + "') from Person GROUP by Geschlecht", _con).ExecuteReader();

                    row += 2;
                    while (rdr.Read())
                    {
                        row++;
                        workSheet.Cells[row, "A"] = rdr[0].ToString();
                        workSheet.Cells[row, "B"] = rdr[1].ToString();
                    }
                    rdr.Close();


                    row += 2;

                    workSheet.Cells[row + 1, "A"] = "unter 14 Jahren";
                    workSheet.Cells[row + 2, "A"] = "Geschlecht";
                    workSheet.Cells[row + 2, "B"] = "Anzahl";

                    rdr = new SqlCommand("select Geschlecht, (select count(*) from Person p, BefKon b where b.PNr = p.PNr AND DATEDIFF(YEAR, Geburtsdatum, Convert(date, CURRENT_TIMESTAMP)) >10 AND DATEDIFF(YEAR, Geburtsdatum, Convert(date, CURRENT_TIMESTAMP)) < 14 AND Anmeldedatum BETWEEN '" + Beginn + "' AND '" + Ende + "') from Person GROUP by Geschlecht", _con).ExecuteReader();

                    row += 2;
                    while (rdr.Read())
                    {
                        row++;
                        workSheet.Cells[row, "A"] = rdr[0].ToString();
                        workSheet.Cells[row, "B"] = rdr[1].ToString();
                    }
                    rdr.Close();

                    row += 2;

                    workSheet.Cells[row + 1, "A"] = "unter 18 Jahren";
                    workSheet.Cells[row + 2, "A"] = "Geschlecht";
                    workSheet.Cells[row + 2, "B"] = "Anzahl";

                    rdr = new SqlCommand("select Geschlecht, (select count(*) from Person p, BefKon b where b.PNr = p.PNr AND DATEDIFF(YEAR, Geburtsdatum, Convert(date, CURRENT_TIMESTAMP)) >14 AND DATEDIFF(YEAR, Geburtsdatum, Convert(date, CURRENT_TIMESTAMP)) < 18 AND Anmeldedatum BETWEEN '" + Beginn + "' AND '" + Ende + "') from Person GROUP by Geschlecht", _con).ExecuteReader();

                    row += 2;
                    while (rdr.Read())
                    {
                        row++;
                        workSheet.Cells[row, "A"] = rdr[0].ToString();
                        workSheet.Cells[row, "B"] = rdr[1].ToString();
                    }
                    workSheet.Columns[1].AutoFit();
                    workSheet.Columns[2].AutoFit();
                    rdr.Close();
                }

                #endregion
            }catch(Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }

        }

        private void passwortÄndernToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var dt = new DataTable();
                _getAdapter.AdaptMa.Fill(dt);

                _bs = new BindingSource();
                _bs.DataSource = dt;

                Auswahl auswahl = new Auswahl(_bs, _con, "Mitarbeiter");
                if (auswahl.ShowDialog() == DialogResult.OK)
                {
                    ChangePassword pwd = new ChangePassword(auswahl.GetId, _con);
                    pwd.ShowDialog();
                    pwd.Dispose();
                }
            }catch(Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            cB_MA_SelectedIndexChanged(null, null);
        }

        private void befassungskontextToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            BefKon b = new BefKon(_con, dgvAnzeige.SelectedRows[0].Cells[0].Value.ToString(), "old");
            b.ShowDialog();
            b.Dispose();
        }

        private void klientBearbeitenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dt = new DataTable();
            _getAdapter.AdaptClient.Fill(dt);

            _bs = new BindingSource();
            _bs.DataSource = dt;

            _bs.Position = _bs.Find("PNr", dgvAnzeige.SelectedRows[0].Cells[1].Value.ToString());
            Client client = new Client(_bs, _con, "Client", ref dt);
            client.ShowDialog();
            client.Dispose();
        }
       
        private void backupOrdnerÖffnenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Directory.Exists(_conf.getString("backup-path")))
                    Process.Start("explorer.exe", _conf.getString("backup-path"));
                else
                    MessageBox.Show("Der Pfad existiert nicht!", Resources.MessageBoxHeaderHinweis);
            }
            catch(Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void dailyBackup()
        {
            try
            {
                var path = _conf.getString("backup-path");
                if (Directory.Exists(path))
                {
                    var pathFiles = Directory.GetFiles(path);
                    var exists = false;

                    foreach(var element in pathFiles)
                    {
                        if (element.Contains(DateTime.Now.ToShortDateString()))
                            exists = true;
                    }

                    if (exists)
                        Directory.Delete(Path.Combine(path, DateTime.Now.ToShortDateString()));


                    var command = new SqlCommand(@"BACKUP DATABASE [Benutzerdaten]  TO  DISK = N'"+ Path.Combine(path, DateTime.Now.ToShortDateString() + ".bak") + "' WITH NOFORMAT, NOINIT, NAME = N'Benutzerdaten_"+ DateTime.Now.ToShortDateString() + "', SKIP, NOREWIND, NOUNLOAD,  STATS = 10", _con);
                    command.ExecuteNonQuery();

                    lblBackup.Text = DateTime.Now.ToString();

                    MessageBox.Show("Das Backup wurde erfolgreich erstellt", Resources.MessageBoxHeaderHinweis);
                }
            }
            catch(Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void täglichesBackupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dailyBackup();
        }
    }
}
