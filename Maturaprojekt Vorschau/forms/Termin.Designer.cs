﻿namespace Maturaprojekt_Vorschau
{
    partial class Termin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblGender = new System.Windows.Forms.Label();
            this.lblTelephone = new System.Windows.Forms.Label();
            this.lblBirthday = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.lblLastName = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.nein = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblBefArt = new System.Windows.Forms.Label();
            this.lblBerater2 = new System.Windows.Forms.Label();
            this.lblZweitberater2 = new System.Windows.Forms.Label();
            this.lblAngebotskategorie = new System.Windows.Forms.Label();
            this.lblDateTime = new System.Windows.Forms.Label();
            this.cmbBoxZweitberater = new System.Windows.Forms.ComboBox();
            this.mTxtBis = new System.Windows.Forms.MaskedTextBox();
            this.cmbBoxBerater = new System.Windows.Forms.ComboBox();
            this.mTxtVon = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbTxtBefassungsart = new System.Windows.Forms.ComboBox();
            this.cmbTxtAngebotskategorie = new System.Windows.Forms.ComboBox();
            this.txtDauer = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblTNr = new System.Windows.Forms.Label();
            this.lblBefKonNr = new System.Windows.Forms.Label();
            this.gBSetting = new System.Windows.Forms.GroupBox();
            this.txtAndere = new System.Windows.Forms.TextBox();
            this.cbAndere = new System.Windows.Forms.CheckBox();
            this.cbKoop = new System.Windows.Forms.CheckBox();
            this.cbGE = new System.Windows.Forms.CheckBox();
            this.cbPfM = new System.Windows.Forms.CheckBox();
            this.cbStV = new System.Windows.Forms.CheckBox();
            this.cbStM = new System.Windows.Forms.CheckBox();
            this.cbKM = new System.Windows.Forms.CheckBox();
            this.cbKV = new System.Windows.Forms.CheckBox();
            this.cbIK = new System.Windows.Forms.CheckBox();
            this.lblWarnung = new System.Windows.Forms.Label();
            this.lblBerater = new System.Windows.Forms.Label();
            this.lblZweitberater = new System.Windows.Forms.Label();
            this.dtpDatum = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.lblBefArt2 = new System.Windows.Forms.Label();
            this.lblAngebotskategorie2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtNotizen = new Maturaprojekt_Vorschau.ownTextbox();
            this.txtAkt = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gBSetting.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGender.Location = new System.Drawing.Point(866, 27);
            this.lblGender.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(13, 17);
            this.lblGender.TabIndex = 24;
            this.lblGender.Text = "-";
            // 
            // lblTelephone
            // 
            this.lblTelephone.AutoSize = true;
            this.lblTelephone.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelephone.Location = new System.Drawing.Point(704, 29);
            this.lblTelephone.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTelephone.Name = "lblTelephone";
            this.lblTelephone.Size = new System.Drawing.Size(13, 17);
            this.lblTelephone.TabIndex = 23;
            this.lblTelephone.Text = "-";
            // 
            // lblBirthday
            // 
            this.lblBirthday.AutoSize = true;
            this.lblBirthday.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBirthday.Location = new System.Drawing.Point(550, 29);
            this.lblBirthday.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBirthday.Name = "lblBirthday";
            this.lblBirthday.Size = new System.Drawing.Size(13, 17);
            this.lblBirthday.TabIndex = 21;
            this.lblBirthday.Text = "-";
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFirstName.Location = new System.Drawing.Point(425, 27);
            this.lblFirstName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(13, 17);
            this.lblFirstName.TabIndex = 20;
            this.lblFirstName.Text = "-";
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastName.Location = new System.Drawing.Point(289, 29);
            this.lblLastName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(13, 17);
            this.lblLastName.TabIndex = 19;
            this.lblLastName.Text = "-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(866, 9);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 17);
            this.label6.TabIndex = 18;
            this.label6.Text = "Geschlecht";
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label.Location = new System.Drawing.Point(704, 9);
            this.label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(120, 17);
            this.label.TabIndex = 17;
            this.label.Text = "Telefonnummer";
            // 
            // nein
            // 
            this.nein.AutoSize = true;
            this.nein.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nein.Location = new System.Drawing.Point(550, 9);
            this.nein.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nein.Name = "nein";
            this.nein.Size = new System.Drawing.Size(110, 17);
            this.nein.TabIndex = 15;
            this.nein.Text = "Geburtsdatum";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(425, 9);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = "Vorname";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(289, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 17);
            this.label1.TabIndex = 13;
            this.label1.Text = "Nachname";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(34, 66);
            this.lblDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(52, 18);
            this.lblDate.TabIndex = 25;
            this.lblDate.Text = "Datum";
            // 
            // lblBefArt
            // 
            this.lblBefArt.AutoSize = true;
            this.lblBefArt.Location = new System.Drawing.Point(418, 103);
            this.lblBefArt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBefArt.Name = "lblBefArt";
            this.lblBefArt.Size = new System.Drawing.Size(103, 18);
            this.lblBefArt.TabIndex = 29;
            this.lblBefArt.Text = "Befassungsart";
            // 
            // lblBerater2
            // 
            this.lblBerater2.AutoSize = true;
            this.lblBerater2.Location = new System.Drawing.Point(418, 67);
            this.lblBerater2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBerater2.Name = "lblBerater2";
            this.lblBerater2.Size = new System.Drawing.Size(56, 18);
            this.lblBerater2.TabIndex = 30;
            this.lblBerater2.Text = "Berater";
            // 
            // lblZweitberater2
            // 
            this.lblZweitberater2.AutoSize = true;
            this.lblZweitberater2.Location = new System.Drawing.Point(753, 67);
            this.lblZweitberater2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblZweitberater2.Name = "lblZweitberater2";
            this.lblZweitberater2.Size = new System.Drawing.Size(89, 18);
            this.lblZweitberater2.TabIndex = 33;
            this.lblZweitberater2.Text = "Zweitberater";
            // 
            // lblAngebotskategorie
            // 
            this.lblAngebotskategorie.AutoSize = true;
            this.lblAngebotskategorie.Location = new System.Drawing.Point(753, 103);
            this.lblAngebotskategorie.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAngebotskategorie.Name = "lblAngebotskategorie";
            this.lblAngebotskategorie.Size = new System.Drawing.Size(131, 18);
            this.lblAngebotskategorie.TabIndex = 32;
            this.lblAngebotskategorie.Text = "Angebotskategorie";
            // 
            // lblDateTime
            // 
            this.lblDateTime.AutoSize = true;
            this.lblDateTime.Location = new System.Drawing.Point(226, 104);
            this.lblDateTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDateTime.Name = "lblDateTime";
            this.lblDateTime.Size = new System.Drawing.Size(27, 18);
            this.lblDateTime.TabIndex = 31;
            this.lblDateTime.Text = "bis";
            // 
            // cmbBoxZweitberater
            // 
            this.cmbBoxZweitberater.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxZweitberater.FormattingEnabled = true;
            this.cmbBoxZweitberater.Location = new System.Drawing.Point(937, 63);
            this.cmbBoxZweitberater.Margin = new System.Windows.Forms.Padding(2);
            this.cmbBoxZweitberater.Name = "cmbBoxZweitberater";
            this.cmbBoxZweitberater.Size = new System.Drawing.Size(152, 26);
            this.cmbBoxZweitberater.TabIndex = 6;
            this.cmbBoxZweitberater.SelectedIndexChanged += new System.EventHandler(this.cmbBoxZweitberater_SelectedIndexChanged);
            // 
            // mTxtBis
            // 
            this.mTxtBis.Location = new System.Drawing.Point(257, 101);
            this.mTxtBis.Margin = new System.Windows.Forms.Padding(2);
            this.mTxtBis.Mask = "00:00";
            this.mTxtBis.Name = "mTxtBis";
            this.mTxtBis.Size = new System.Drawing.Size(61, 24);
            this.mTxtBis.TabIndex = 3;
            // 
            // cmbBoxBerater
            // 
            this.cmbBoxBerater.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxBerater.FormattingEnabled = true;
            this.cmbBoxBerater.Location = new System.Drawing.Point(579, 63);
            this.cmbBoxBerater.Margin = new System.Windows.Forms.Padding(2);
            this.cmbBoxBerater.Name = "cmbBoxBerater";
            this.cmbBoxBerater.Size = new System.Drawing.Size(152, 26);
            this.cmbBoxBerater.TabIndex = 5;
            this.cmbBoxBerater.SelectedIndexChanged += new System.EventHandler(this.cmbBoxBerater_SelectedIndexChanged);
            // 
            // mTxtVon
            // 
            this.mTxtVon.Location = new System.Drawing.Point(134, 100);
            this.mTxtVon.Margin = new System.Windows.Forms.Padding(2);
            this.mTxtVon.Mask = "00:00";
            this.mTxtVon.Name = "mTxtVon";
            this.mTxtVon.Size = new System.Drawing.Size(61, 24);
            this.mTxtVon.TabIndex = 2;
            this.mTxtVon.TextChanged += new System.EventHandler(this.mTxtVon_TextChanged);
            this.mTxtVon.Leave += new System.EventHandler(this.mTxtVon_MaskInputRejected);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 104);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 18);
            this.label3.TabIndex = 61;
            this.label3.Text = "Uhrzeit  von";
            // 
            // cmbTxtBefassungsart
            // 
            this.cmbTxtBefassungsart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTxtBefassungsart.FormattingEnabled = true;
            this.cmbTxtBefassungsart.Location = new System.Drawing.Point(579, 100);
            this.cmbTxtBefassungsart.Margin = new System.Windows.Forms.Padding(2);
            this.cmbTxtBefassungsart.Name = "cmbTxtBefassungsart";
            this.cmbTxtBefassungsart.Size = new System.Drawing.Size(152, 26);
            this.cmbTxtBefassungsart.TabIndex = 7;
            this.cmbTxtBefassungsart.SelectedIndexChanged += new System.EventHandler(this.cmbTxtBefassungsart_SelectedIndexChanged);
            // 
            // cmbTxtAngebotskategorie
            // 
            this.cmbTxtAngebotskategorie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTxtAngebotskategorie.FormattingEnabled = true;
            this.cmbTxtAngebotskategorie.Location = new System.Drawing.Point(937, 100);
            this.cmbTxtAngebotskategorie.Margin = new System.Windows.Forms.Padding(2);
            this.cmbTxtAngebotskategorie.Name = "cmbTxtAngebotskategorie";
            this.cmbTxtAngebotskategorie.Size = new System.Drawing.Size(152, 26);
            this.cmbTxtAngebotskategorie.TabIndex = 8;
            this.cmbTxtAngebotskategorie.SelectedIndexChanged += new System.EventHandler(this.cmbTxtAngebotskategorie_SelectedIndexChanged);
            // 
            // txtDauer
            // 
            this.txtDauer.Location = new System.Drawing.Point(134, 139);
            this.txtDauer.Margin = new System.Windows.Forms.Padding(2);
            this.txtDauer.Name = "txtDauer";
            this.txtDauer.Size = new System.Drawing.Size(61, 24);
            this.txtDauer.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(34, 142);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 18);
            this.label5.TabIndex = 70;
            this.label5.Text = "Dauer";
            // 
            // lblTNr
            // 
            this.lblTNr.AutoSize = true;
            this.lblTNr.Location = new System.Drawing.Point(162, 27);
            this.lblTNr.Name = "lblTNr";
            this.lblTNr.Size = new System.Drawing.Size(13, 18);
            this.lblTNr.TabIndex = 76;
            this.lblTNr.Text = "-";
            // 
            // lblBefKonNr
            // 
            this.lblBefKonNr.AutoSize = true;
            this.lblBefKonNr.Location = new System.Drawing.Point(32, 29);
            this.lblBefKonNr.Name = "lblBefKonNr";
            this.lblBefKonNr.Size = new System.Drawing.Size(13, 18);
            this.lblBefKonNr.TabIndex = 75;
            this.lblBefKonNr.Text = "-";
            // 
            // gBSetting
            // 
            this.gBSetting.Controls.Add(this.txtAndere);
            this.gBSetting.Controls.Add(this.cbAndere);
            this.gBSetting.Controls.Add(this.cbKoop);
            this.gBSetting.Controls.Add(this.cbGE);
            this.gBSetting.Controls.Add(this.cbPfM);
            this.gBSetting.Controls.Add(this.cbStV);
            this.gBSetting.Controls.Add(this.cbStM);
            this.gBSetting.Controls.Add(this.cbKM);
            this.gBSetting.Controls.Add(this.cbKV);
            this.gBSetting.Controls.Add(this.cbIK);
            this.gBSetting.Location = new System.Drawing.Point(421, 142);
            this.gBSetting.Margin = new System.Windows.Forms.Padding(2);
            this.gBSetting.Name = "gBSetting";
            this.gBSetting.Padding = new System.Windows.Forms.Padding(2);
            this.gBSetting.Size = new System.Drawing.Size(459, 109);
            this.gBSetting.TabIndex = 78;
            this.gBSetting.TabStop = false;
            this.gBSetting.Text = "Setting";
            // 
            // txtAndere
            // 
            this.txtAndere.Location = new System.Drawing.Point(328, 71);
            this.txtAndere.Name = "txtAndere";
            this.txtAndere.Size = new System.Drawing.Size(112, 24);
            this.txtAndere.TabIndex = 17;
            this.txtAndere.TextChanged += new System.EventHandler(this.txtAndere_TextChanged);
            // 
            // cbAndere
            // 
            this.cbAndere.AutoSize = true;
            this.cbAndere.Location = new System.Drawing.Point(302, 75);
            this.cbAndere.Name = "cbAndere";
            this.cbAndere.Size = new System.Drawing.Size(18, 17);
            this.cbAndere.TabIndex = 17;
            this.cbAndere.TabStop = false;
            this.cbAndere.UseVisualStyleBackColor = true;
            // 
            // cbKoop
            // 
            this.cbKoop.AutoSize = true;
            this.cbKoop.Location = new System.Drawing.Point(302, 47);
            this.cbKoop.Name = "cbKoop";
            this.cbKoop.Size = new System.Drawing.Size(66, 22);
            this.cbKoop.TabIndex = 16;
            this.cbKoop.Text = "Koop";
            this.cbKoop.UseVisualStyleBackColor = true;
            // 
            // cbGE
            // 
            this.cbGE.AutoSize = true;
            this.cbGE.Location = new System.Drawing.Point(302, 22);
            this.cbGE.Name = "cbGE";
            this.cbGE.Size = new System.Drawing.Size(52, 22);
            this.cbGE.TabIndex = 15;
            this.cbGE.Text = "GE";
            this.cbGE.UseVisualStyleBackColor = true;
            // 
            // cbPfM
            // 
            this.cbPfM.AutoSize = true;
            this.cbPfM.Location = new System.Drawing.Point(158, 72);
            this.cbPfM.Name = "cbPfM";
            this.cbPfM.Size = new System.Drawing.Size(84, 22);
            this.cbPfM.TabIndex = 14;
            this.cbPfM.Text = "PfM/PfV";
            this.cbPfM.UseVisualStyleBackColor = true;
            // 
            // cbStV
            // 
            this.cbStV.AutoSize = true;
            this.cbStV.Location = new System.Drawing.Point(158, 47);
            this.cbStV.Name = "cbStV";
            this.cbStV.Size = new System.Drawing.Size(53, 22);
            this.cbStV.TabIndex = 13;
            this.cbStV.Text = "StV";
            this.cbStV.UseVisualStyleBackColor = true;
            // 
            // cbStM
            // 
            this.cbStM.AutoSize = true;
            this.cbStM.Location = new System.Drawing.Point(158, 22);
            this.cbStM.Name = "cbStM";
            this.cbStM.Size = new System.Drawing.Size(57, 22);
            this.cbStM.TabIndex = 12;
            this.cbStM.Text = "StM";
            this.cbStM.UseVisualStyleBackColor = true;
            // 
            // cbKM
            // 
            this.cbKM.AutoSize = true;
            this.cbKM.Location = new System.Drawing.Point(11, 72);
            this.cbKM.Name = "cbKM";
            this.cbKM.Size = new System.Drawing.Size(53, 22);
            this.cbKM.TabIndex = 11;
            this.cbKM.Text = "KM";
            this.cbKM.UseVisualStyleBackColor = true;
            // 
            // cbKV
            // 
            this.cbKV.AutoSize = true;
            this.cbKV.Location = new System.Drawing.Point(11, 47);
            this.cbKV.Name = "cbKV";
            this.cbKV.Size = new System.Drawing.Size(49, 22);
            this.cbKV.TabIndex = 10;
            this.cbKV.Text = "KV";
            this.cbKV.UseVisualStyleBackColor = true;
            // 
            // cbIK
            // 
            this.cbIK.AutoSize = true;
            this.cbIK.Location = new System.Drawing.Point(11, 22);
            this.cbIK.Name = "cbIK";
            this.cbIK.Size = new System.Drawing.Size(43, 22);
            this.cbIK.TabIndex = 9;
            this.cbIK.Text = "IK";
            this.cbIK.UseVisualStyleBackColor = true;
            // 
            // lblWarnung
            // 
            this.lblWarnung.AutoSize = true;
            this.lblWarnung.ForeColor = System.Drawing.Color.Red;
            this.lblWarnung.Location = new System.Drawing.Point(296, 8);
            this.lblWarnung.Name = "lblWarnung";
            this.lblWarnung.Size = new System.Drawing.Size(0, 18);
            this.lblWarnung.TabIndex = 79;
            // 
            // lblBerater
            // 
            this.lblBerater.AutoSize = true;
            this.lblBerater.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBerater.Location = new System.Drawing.Point(531, 69);
            this.lblBerater.Name = "lblBerater";
            this.lblBerater.Size = new System.Drawing.Size(14, 18);
            this.lblBerater.TabIndex = 80;
            this.lblBerater.Text = "-";
            // 
            // lblZweitberater
            // 
            this.lblZweitberater.AutoSize = true;
            this.lblZweitberater.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblZweitberater.Location = new System.Drawing.Point(892, 68);
            this.lblZweitberater.Name = "lblZweitberater";
            this.lblZweitberater.Size = new System.Drawing.Size(14, 18);
            this.lblZweitberater.TabIndex = 82;
            this.lblZweitberater.Text = "-";
            // 
            // dtpDatum
            // 
            this.dtpDatum.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDatum.Location = new System.Drawing.Point(134, 61);
            this.dtpDatum.Name = "dtpDatum";
            this.dtpDatum.Size = new System.Drawing.Size(129, 24);
            this.dtpDatum.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(886, 628);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 34);
            this.button1.TabIndex = 21;
            this.button1.Text = "Schließen";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(990, 628);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(101, 34);
            this.button2.TabIndex = 20;
            this.button2.Text = "Speichern";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // lblBefArt2
            // 
            this.lblBefArt2.AutoSize = true;
            this.lblBefArt2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBefArt2.Location = new System.Drawing.Point(531, 104);
            this.lblBefArt2.Name = "lblBefArt2";
            this.lblBefArt2.Size = new System.Drawing.Size(14, 18);
            this.lblBefArt2.TabIndex = 91;
            this.lblBefArt2.Text = "-";
            // 
            // lblAngebotskategorie2
            // 
            this.lblAngebotskategorie2.AutoSize = true;
            this.lblAngebotskategorie2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAngebotskategorie2.Location = new System.Drawing.Point(892, 104);
            this.lblAngebotskategorie2.Name = "lblAngebotskategorie2";
            this.lblAngebotskategorie2.Size = new System.Drawing.Size(14, 18);
            this.lblAngebotskategorie2.TabIndex = 92;
            this.lblAngebotskategorie2.Text = "-";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(32, 9);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 17);
            this.label4.TabIndex = 93;
            this.label4.Text = "BefKonNr";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(162, 9);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 17);
            this.label7.TabIndex = 94;
            this.label7.Text = "TerminNr";
            // 
            // txtNotizen
            // 
            this.txtNotizen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNotizen.editTitle = "private Notizen";
            this.txtNotizen.Location = new System.Drawing.Point(35, 438);
            this.txtNotizen.Margin = new System.Windows.Forms.Padding(2);
            this.txtNotizen.Name = "txtNotizen";
            this.txtNotizen.Size = new System.Drawing.Size(1039, 157);
            this.txtNotizen.TabIndex = 19;
            // 
            // txtAkt
            // 
            this.txtAkt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAkt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAkt.Location = new System.Drawing.Point(3, 20);
            this.txtAkt.Multiline = true;
            this.txtAkt.Name = "txtAkt";
            this.txtAkt.Size = new System.Drawing.Size(1033, 134);
            this.txtAkt.TabIndex = 18;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblWarnung);
            this.groupBox1.Controls.Add(this.txtAkt);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(35, 256);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1039, 157);
            this.groupBox1.TabIndex = 96;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Akt";
            // 
            // Termin
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.AntiqueWhite;
            this.ClientSize = new System.Drawing.Size(1107, 673);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblAngebotskategorie2);
            this.Controls.Add(this.lblBefArt2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.dtpDatum);
            this.Controls.Add(this.txtNotizen);
            this.Controls.Add(this.lblZweitberater);
            this.Controls.Add(this.lblBerater);
            this.Controls.Add(this.gBSetting);
            this.Controls.Add(this.lblTNr);
            this.Controls.Add(this.lblBefKonNr);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtDauer);
            this.Controls.Add(this.cmbTxtAngebotskategorie);
            this.Controls.Add(this.cmbTxtBefassungsart);
            this.Controls.Add(this.mTxtVon);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbBoxBerater);
            this.Controls.Add(this.mTxtBis);
            this.Controls.Add(this.cmbBoxZweitberater);
            this.Controls.Add(this.lblZweitberater2);
            this.Controls.Add(this.lblAngebotskategorie);
            this.Controls.Add(this.lblDateTime);
            this.Controls.Add(this.lblBerater2);
            this.Controls.Add(this.lblBefArt);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.lblGender);
            this.Controls.Add(this.lblTelephone);
            this.Controls.Add(this.lblBirthday);
            this.Controls.Add(this.lblFirstName);
            this.Controls.Add(this.lblLastName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label);
            this.Controls.Add(this.nein);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Termin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Termin";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Termin_FormClosing);
            this.Load += new System.EventHandler(this.new_Termin_Load);
            this.gBSetting.ResumeLayout(false);
            this.gBSetting.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblTelephone;
        private System.Windows.Forms.Label lblBirthday;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label nein;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblBefArt;
        private System.Windows.Forms.Label lblBerater2;
        private System.Windows.Forms.Label lblZweitberater2;
        private System.Windows.Forms.Label lblAngebotskategorie;
        private System.Windows.Forms.Label lblDateTime;
        private System.Windows.Forms.ComboBox cmbBoxZweitberater;
        private System.Windows.Forms.MaskedTextBox mTxtBis;
        private System.Windows.Forms.ComboBox cmbBoxBerater;
        private System.Windows.Forms.MaskedTextBox mTxtVon;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbTxtBefassungsart;
        private System.Windows.Forms.ComboBox cmbTxtAngebotskategorie;
        private System.Windows.Forms.TextBox txtDauer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblTNr;
        private System.Windows.Forms.Label lblBefKonNr;
        private System.Windows.Forms.GroupBox gBSetting;
        private System.Windows.Forms.TextBox txtAndere;
        private System.Windows.Forms.CheckBox cbAndere;
        private System.Windows.Forms.CheckBox cbKoop;
        private System.Windows.Forms.CheckBox cbGE;
        private System.Windows.Forms.CheckBox cbPfM;
        private System.Windows.Forms.CheckBox cbStV;
        private System.Windows.Forms.CheckBox cbStM;
        private System.Windows.Forms.CheckBox cbKM;
        private System.Windows.Forms.CheckBox cbKV;
        private System.Windows.Forms.CheckBox cbIK;
        private System.Windows.Forms.Label lblWarnung;
        private System.Windows.Forms.Label lblBerater;
        private System.Windows.Forms.Label lblZweitberater;
        private ownTextbox txtNotizen;
        private System.Windows.Forms.DateTimePicker dtpDatum;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label lblBefArt2;
        private System.Windows.Forms.Label lblAngebotskategorie2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtAkt;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}