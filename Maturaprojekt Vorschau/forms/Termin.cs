﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Maturaprojekt_Vorschau.Properties;
using Tools;
using NLog;

namespace Maturaprojekt_Vorschau
{
    public partial class Termin : Form
    {
        #region Variablen
        private SqlConnection _con;
        private BindingSource _bs;
        private DataTable _dt;
        private string _BefKonNr;
        private string _TNr;
        private string _MNr;
        private List<NumberString> Mitarbeiter;
        private bool neu = false;
        private MethodRepository _adapter;
        private Logger _logger;
        #endregion

        public Termin()
        {
            InitializeComponent();
        }
        public Termin(SqlConnection con, BindingSource bs, string BNr,ref DataTable dt)
        {
            _con = con;
            _bs = bs;
            _BefKonNr = BNr;
            _dt = dt;
            InitializeComponent();
        }
        public Termin(SqlConnection con, BindingSource bs, string BNr, string MNr,ref DataTable dt) : this(con, bs, BNr, ref dt)
        {
            //_con = con;
            //_bs = bs;
            //_BefKonNr = BNr;
            _MNr = MNr;
            //_dt = dt;
            //InitializeComponent();
        }

        private void new_Termin_Load(object sender, EventArgs e)
        {
            try
            {
                var conf = new Configuration(Resources.ConfigurationPath);
                this.Font = new Font("Microsoft Sans Serif", conf.getInt("text-size"));

                _logger = LogManager.GetCurrentClassLogger();

                dtpDatum.DataBindings.Add("Text", _bs, "Datum");
                lblBerater.DataBindings.Add("Text", _bs, "Berater");
                lblZweitberater.DataBindings.Add("Text", _bs, "Zweitberater");
                lblBefArt2.DataBindings.Add("Text", _bs, "Befassungsart");
                lblAngebotskategorie2.DataBindings.Add("Text", _bs, "Angebotskategorie");
                mTxtVon.DataBindings.Add("Text", _bs, "Beginn");
                mTxtBis.DataBindings.Add("Text", _bs, "Ende");
                txtDauer.DataBindings.Add("Text", _bs, "Dauer");
                txtAkt.DataBindings.Add("Text", _bs, "Akt");

                lblBefKonNr.DataBindings.Add("Text", _bs, "BefKonNr");
                lblTNr.DataBindings.Add("Text", _bs, "TNr");

                if (_con.State == ConnectionState.Closed)
                    _con.Open();

                _adapter = new MethodRepository(_con);

                if (lblTNr.Text == "")
                {
                    newNr();
                    lblBefKonNr.Text = _BefKonNr;

                    neu = true;
                    dtpDatum.Text = DateTime.Now.ToShortDateString();


                }
                else
                {
                    _TNr = lblTNr.Text;
                    SqlCommand sqlCommand = new SqlCommand("Select Typ from Termin_Setting where TNr='" + _TNr + "'", _con);

                    SqlDataReader read = sqlCommand.ExecuteReader();

                    List<string> setting = new List<string>();
                    while (read.Read())
                    {
                        setting.Add(read[0].ToString());
                    }
                    read.Close();

                    if (setting.Contains("IK"))
                        cbIK.Checked = true;
                    if (setting.Contains("KV"))
                        cbKV.Checked = true;
                    if (setting.Contains("KM"))
                        cbKM.Checked = true;
                    if (setting.Contains("StM"))
                        cbStM.Checked = true;
                    if (setting.Contains("StV"))
                        cbStV.Checked = true;
                    if (setting.Contains("PfM"))
                        cbPfM.Checked = true;
                    if (setting.Contains("GE"))
                        cbGE.Checked = true;
                    if (setting.Contains("Koop"))
                        cbKoop.Checked = true;


                    for (int i = 0; i < setting.Count; i++)
                        if (setting[i].Substring(0, 2) == "xx")
                        {
                            cbAndere.Checked = true;
                            txtAndere.Text = setting[i].Substring(2, setting[i].Length - 2);
                            i = setting.Count;
                        }



                    //Notizen füllen    
                    txtNotizen.Text = new SqlCommand("Select Notizen from Termin_txt where BefKonNr= " + _BefKonNr + " and TNr= " + _TNr + " and MNr= " + _MNr, _con).ExecuteScalar().ToString();
                }


                //Klientenlabel füllen
                var command = new SqlCommand("Select Nachname, Vorname, Geburtsdatum, TelNr, Geschlecht from Person where PNr = @pnr and PTyp = 'IK'", _con);
                command.Parameters.AddWithValue("@pnr",
                    new SqlCommand("Select PNr from BefKon where BefKonNr = " + lblBefKonNr.Text, _con).ExecuteScalar()
                        .ToString());

                SqlDataReader rdr = command.ExecuteReader();
                rdr.Read();

                lblLastName.Text = rdr[0].ToString();
                lblFirstName.Text = rdr[1].ToString();
                lblBirthday.Text = rdr[2].ToString();
                lblTelephone.Text = rdr[3].ToString();
                lblGender.Text = rdr[4].ToString();

                rdr.Close();

                if (lblBirthday.Text != "")
                    lblBirthday.Text = Convert.ToDateTime(lblBirthday.Text).ToShortDateString();

                #region Mitarbeiter füllen
                command = new SqlCommand("Select MNr, Nachname + ' ' + Vorname from Mitarbeiter where MNr > 0 and geloescht = 0", _con);

                SqlDataReader reader = command.ExecuteReader();

                Mitarbeiter = new List<NumberString>();

                while (reader.Read())
                {
                    cmbBoxBerater.Items.Add(reader[1].ToString());
                    cmbBoxZweitberater.Items.Add(reader[1].ToString());
                    Mitarbeiter.Add(new NumberString(reader[0].ToString(), reader[1].ToString()));
                }

                cmbBoxBerater.Text = new NumberString("", "").findName(lblBerater.Text, Mitarbeiter);
                cmbBoxZweitberater.Text = new NumberString("", "").findName(lblZweitberater.Text, Mitarbeiter);

                reader.Close();
#endregion

                #region Angebotskategorie
                reader = new SqlCommand("Select Kategorie from Angebotskategorie", _con).ExecuteReader();
                while (reader.Read())
                    cmbTxtAngebotskategorie.Items.Add(reader[0].ToString());
                reader.Close();
                #endregion

                #region Befassungsart
                reader = new SqlCommand("Select Kategorie from Befassungsart", _con).ExecuteReader();
                while (reader.Read())
                    cmbTxtBefassungsart.Items.Add(reader[0].ToString());
                reader.Close();
#endregion

                #region comboboxen anzeigen
                if (!string.IsNullOrEmpty(lblBefArt2.Text))
                    cmbTxtBefassungsart.Text = new SqlCommand("Select Kategorie from Befassungsart where lfdNr = " + lblBefArt2.Text, _con).ExecuteScalar().ToString();

                if (!string.IsNullOrEmpty(lblAngebotskategorie2.Text))
                    cmbTxtAngebotskategorie.Text = new SqlCommand("Select Kategorie from Angebotskategorie where lfdNr = " + lblAngebotskategorie2.Text, _con).ExecuteScalar().ToString();
                #endregion


            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void newNr()
        {
            string temp;

            SqlCommand comm = new SqlCommand("select max(TNr) from Termin where BefKonNr = " + _BefKonNr, _con);
            SqlDataReader read = comm.ExecuteReader();

            try
            {
                read.Read();
                temp = (Convert.ToInt32(read[0].ToString()) + 1).ToString();
                read.Close();
            }
            catch (Exception ex) { temp = "1"; read.Close(); }

            _TNr = temp;
            lblTNr.Text = temp;
        }
        public void makeSetting()
        {

            if (cbGE.Checked)
                try
                {
                    new SqlCommand("Insert into Termin_Setting values(" + _BefKonNr + "," + _TNr + ",'GE')", _con).ExecuteNonQuery();
                }
                catch (Exception ex) { }
            if (cbIK.Checked)
                try
                {
                    new SqlCommand("Insert into Termin_Setting values(" + _BefKonNr + "," + _TNr + ",'IK')", _con).ExecuteNonQuery();
                }
                catch { }
            if (cbKM.Checked)
                try
                {
                    new SqlCommand("Insert into Termin_Setting values(" + _BefKonNr + "," + _TNr + ",'KM')", _con).ExecuteNonQuery();
                }
                catch { }
            if (cbKoop.Checked)
                try
                {
                    new SqlCommand("Insert into Termin_Setting values(" + _BefKonNr + "," + _TNr + ",'Koop')", _con).ExecuteNonQuery();
                }
                catch { }
            if (cbKV.Checked)
                try
                {
                    new SqlCommand("Insert into Termin_Setting values(" + _BefKonNr + "," + _TNr + ",'KV')", _con).ExecuteNonQuery();
                }
                catch { }
            if (cbPfM.Checked)
                try
                {
                    new SqlCommand("Insert into Termin_Setting values(" + _BefKonNr + "," + _TNr + ",'PfM')", _con).ExecuteNonQuery();
                }
                catch { }
            if (cbStM.Checked)
                try
                {
                    new SqlCommand("Insert into Termin_Setting values(" + _BefKonNr + "," + _TNr + ",'StM')", _con).ExecuteNonQuery();
                }
                catch { }
            if (cbStV.Checked)
                try
                {
                    new SqlCommand("Insert into Termin_Setting values(" + _BefKonNr + "," + _TNr + ",'StV')", _con).ExecuteNonQuery();
                }
                catch { }
            if (cbAndere.Checked)
                try
                {
                    new SqlCommand("Insert into Termin_Setting values(" + _BefKonNr + "," + _TNr + ",'xx" + txtAndere.Text + "')", _con).ExecuteNonQuery();
                }
                catch { }




        }
        public void Change()
        {
            try
            {
                SqlCommand command = new SqlCommand("Insert into Termin_txt values(" + _BefKonNr + "," + _TNr + "," + _MNr + ",'" + txtNotizen.Text + "')", _con);

                command.ExecuteNonQuery();

            }
            catch
            {
                try
                {
                    SqlCommand command = new SqlCommand("Update Termin_txt set Notizen = '" + txtNotizen.Text + "' where BefKonNr = " + _BefKonNr + " and TNr = " + _TNr + " and MNr = " + _MNr, _con);

                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }

        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (neu)
                    newNr();

                _bs.EndEdit();
                _adapter.AdaptTermin.Update(_dt);
                Change();
                makeSetting();
                neu = false;
                MessageBox.Show("Termin erfolgreich gespeichert", Resources.MessageBoxHeaderHinweis);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }


        private void txtAndere_TextChanged(object sender, EventArgs e)
        {
            if (!cbAndere.Checked && txtAndere.Text != "")
                cbAndere.Checked = true;
            if (cbAndere.Checked && txtAndere.Text == "")
                cbAndere.Checked = false;
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbBoxBerater_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblBerater.Text = new NumberString("", "").findID(cmbBoxBerater.Text, Mitarbeiter);
        }

        private void cmbBoxZweitberater_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblZweitberater.Text = new NumberString("", "").findID(cmbBoxZweitberater.Text, Mitarbeiter);
        }

        private void Termin_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (e.CloseReason == CloseReason.UserClosing)
                {
                    var box = MessageBox.Show("Wollen Sie die Änderungen speichern?",
                    Resources.MessageBoxHeaderHinweis, MessageBoxButtons.YesNoCancel);

                    if (box == DialogResult.Yes)
                    {
                        if (neu)
                            newNr();
                        _bs.EndEdit();
                        _adapter.AdaptTermin.Update(_dt);
                        Change();
                        makeSetting();
                        DialogResult = DialogResult.OK;
                    }
                    else if (box == DialogResult.No)
                    {
                        _bs.CancelEdit();
                        DialogResult = DialogResult.Cancel;
                    }
                    else
                        e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage + ex.Message, Resources.MessageBoxHeaderHinweis, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void mTxtVon_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (neu)
                    mTxtBis.Text = Convert.ToDateTime(mTxtVon.Text).AddHours(1.0).ToShortTimeString();
            }
            catch { }
        }

        private void cmbTxtBefassungsart_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbTxtBefassungsart.Text))
                lblBefArt2.Text = new SqlCommand("Select lfdNr from Befassungsart where Kategorie = '" + cmbTxtBefassungsart.Text + "'", _con).ExecuteScalar().ToString();
        }

        private void cmbTxtAngebotskategorie_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbTxtAngebotskategorie.Text))
                lblAngebotskategorie2.Text = new SqlCommand("Select lfdNr from Angebotskategorie where Kategorie = '" + cmbTxtAngebotskategorie.Text+"'", _con).ExecuteScalar().ToString();
        }

        private void mTxtVon_MaskInputRejected(object sender, EventArgs e)
        {
                        if (mTxtVon.Text != "  :")
            {
                var array = mTxtVon.Text.Split(':');

                if (int.Parse(array[0]) > 23)
                {
                    mTxtVon.Text = "00" + array[1];
                    array[0] = "00";
                }

                if (int.Parse(array[1]) > 59)
                {
                    mTxtVon.Text = array[0] + "59";
                }
            }
        }
    }
}
