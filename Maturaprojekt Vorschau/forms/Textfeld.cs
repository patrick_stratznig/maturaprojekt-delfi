﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Maturaprojekt_Vorschau.Properties;
using Tools;
using NLog;

namespace Maturaprojekt_Vorschau
{
    public partial class Textfeld : Form
    {
        private string eingabe;
        private Logger _logger;

        public Textfeld()
        {
            InitializeComponent();
        }
        public Textfeld(string eingabe)
        {
            InitializeComponent();
            this.eingabe = eingabe;
            txtEingabe.Text = eingabe;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            eingabe = txtEingabe.Text;
            DialogResult = DialogResult.OK;
            this.Close();
        }
        public string Eingabe
        {
            get { return eingabe; }
        }

        private void Textfeld_Load(object sender, EventArgs e)
        {
            try
            {
                var conf = new Configuration(Resources.ConfigurationPath);
                this.Font = new Font("Microsoft Sans Serif", conf.getInt("text-size"));
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MessageBox.Show(Resources.ErrorMessage, Resources.MessageBoxHeaderHinweis);
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Textfeld_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnOK_Click(null, null);
        }
    }
}
