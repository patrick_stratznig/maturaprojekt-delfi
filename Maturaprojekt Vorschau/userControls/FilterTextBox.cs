﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Maturaprojekt_Vorschau
{
    public partial class FilterTextBox : UserControl
    {
        DataGridView dgv;
        List<TextBox> txt;
        public FilterTextBox()
        {
            InitializeComponent();
        }

        public string this[int index]
        {
            get { return txt[index].Text; }
        }
        public FilterTextBox(DataGridView dgv, Form f1)
        {
            InitializeComponent();
            try {
                this.dgv = dgv;
                txt = new List<TextBox>();
                int x = dgv.Location.X;
                int y = dgv.Location.Y;
                int width = dgv.RowHeadersWidth + dgv.Location.X; 

                for (int count = 0; count < dgv.Columns.Count; count++)
                {

                        this.txt.Add(new TextBox());
                    if (dgv.Columns[count].Visible)
                    {

                        this.txt[count].Location = new System.Drawing.Point(width, dgv.Location.Y - 25);
                        this.txt[count].Size = new System.Drawing.Size(dgv.Columns[count].Width, 29);
                        f1.Controls.Add(txt[count]);
                        if (!dgv.Columns[count].ValueType.Name.Contains("Date"))
                            this.txt[count].Visible = true;
                        else
                            txt[count].Visible = false;
                        this.txt[count].Name = "tx" + count;
                        width += dgv.Columns[count].Width;
                    }
                        txt[count].TextChanged += valChanged;
                    
                }
            }
            catch(Exception r4dl) { MessageBox.Show(r4dl.ToString()); }
        }
        public FilterTextBox(DataGridView dgv, Form f1, bool[] b)
        {
            InitializeComponent();
            try
            {
                this.dgv = dgv;
                txt = new List<TextBox>();
                int x = dgv.Location.X;
                int y = dgv.Location.Y;
                int width = dgv.RowHeadersWidth + dgv.Location.X;

                for (int count = 0; count < dgv.Columns.Count; count++)
                {
                    if (b[count])
                    {
                        this.txt.Add(new TextBox());
                        this.txt[count].Location = new System.Drawing.Point(width, dgv.Location.Y - 25);
                        this.txt[count].Size = new System.Drawing.Size(dgv.Columns[count].Width, 29);
                        f1.Controls.Add(txt[count]);
                        this.txt[count].Visible = true;
                        this.txt[count].Name = "tx" + count;
                        width += dgv.Columns[count].Width;
                        txt[count].TextChanged += valChanged;
                    }
                }
            }
            catch (Exception r4dl) { MessageBox.Show(r4dl.ToString()); }
        }

        public delegate void TextboxTextChangeHandler(object sender, TextChangeEventArgs e);
        public event TextboxTextChangeHandler ValChangedHandler;

        
        public void valChanged(object Sender, EventArgs e)
        {
            TextChangeEventArgs ea = new TextChangeEventArgs(Convert.ToInt32((Sender as TextBox).Name.Trim('t', 'x')), (Sender as TextBox).Text);

            if (ValChangedHandler != null)
                ValChangedHandler(Sender, ea);
        }

        public class TextChangeEventArgs
        {
            public int index;
            public string filter;
            public TextChangeEventArgs(int index, string filter)
            {
                this.index = index;
                this.filter = filter;
            }
        }
        public void resizeColumns(DataGridView dgv)
        {
            int width = dgv.RowHeadersWidth + dgv.Location.X;
            for (int count = 0; count < dgv.Columns.Count;count++)
            {
                    this.txt[count].Location = new System.Drawing.Point(width, dgv.Location.Y - 25);
                    this.txt[count].Size = new System.Drawing.Size(dgv.Columns[count].Width, 29);
                if (dgv.Columns[count].Visible)
                    width += dgv.Columns[count].Width;
            }
        }

        private void FilterTextBox_Load(object sender, EventArgs e)
        {

        }
    }
}
