﻿namespace Maturaprojekt_Vorschau
{
    partial class FilterTextBox
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode fuer die Designerunterstuetzung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geaendert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // FilterTextBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "FilterTextBox";
            this.Size = new System.Drawing.Size(573, 200);
            this.Load += new System.EventHandler(this.FilterTextBox_Load);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
