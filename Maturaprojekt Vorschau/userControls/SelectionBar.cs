﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Tools
{
    /// <summary>
    /// Steuerelement, das über eier DataGridView angeordnet wird und für jede Spalte der DataGridView
    /// eine Textbox aufweist, damit ein Selektionstext eingegeben werden kann.
    /// </summary>
    public partial class SelectionBar : UserControl
    {
        // Delegate und Event für die Änderung eines Selektionstextes
        public delegate void ItemTextChangedHandler(object sender, ItemTextChangedEventArgs e);
        public event ItemTextChangedHandler ItemTextChanged;
        // Delegate und Event für das Verlassen einer Textbox
        public delegate void ItemChangedHandler(object sender, ItemChangedEventArgs e);
        public event ItemChangedHandler ItemChanged;
        // Liste aller Textboxen
        private List<TextBox> t;
        // Liste aller Textboxinhalte
        private List<string> tInhalt;
        // DataGridView, die mit den Selektionstexten versehen werden soll. 
        private DataGridView dgv;

        /// <summary>
        /// Konstruktor für die Anlage der Leiste mit den Textboxen
        /// </summary>
        /// <param name="dgv">Betroffene DataGridView</param>
        public SelectionBar(DataGridView dgv)
        {
            TextBox tb;
            this.dgv = dgv;
            t = new List<TextBox>();
            tInhalt = new List<string>();
            tb = null;
            for (int i = 0; i < dgv.Columns.Count; i++)
            {
                tb = new TextBox();
                tb.Name = dgv.Columns[i].Name;
                tb.Visible = dgv.Columns[i].Visible;
                tb.TextChanged += new EventHandler(textBoxChanged);
                tb.Leave += new EventHandler(textLeave);
                if(tb.Visible)
                    Controls.Add(tb);
                t.Add(tb);
                tInhalt.Add("");
            }
            resizeBoxes(); // Spaltenbreite für jede Textbox einstellen
            this.Left = dgv.Left;
            this.Top = dgv.Top - this.Height;
        }
        /// <summary>
        /// Bei Änderung des Textes in einer Textbox wird diese Methode aufgerufen.
        /// Hier wird der Event ItemTextChanged ausgelöst, die ItemTextChangedEventArgs werden angelegt.
        /// </summary>
        /// <param name="sender">Aufrufendes Objekt</param>
        /// <param name="e">Parameter</param>
        private void textBoxChanged(object sender, EventArgs e)
        {
            string name = ((TextBox)sender).Name;
            string text = ((TextBox)sender).Text;
            int i=0;
            while (t[i] != sender) 
                i++;
            ItemTextChangedEventArgs e1=new ItemTextChangedEventArgs(i,name,text);
            if(ItemTextChanged!=null)
                ItemTextChanged(this, e1);
        }
        /// <summary>
        /// Bei Verlassen einer Textbox wird diese Methode aufgerufen.
        /// Hier wird der Event ItemChanged ausgelöst, die ItemChangedEventArgs werden angelegt.
        /// </summary>
        /// <param name="sender">Aufrufendes Objekt</param>
        /// <param name="e">Parameter</param>
        private void textLeave(object sender, EventArgs e)
        {
            string name = ((TextBox)sender).Name;
            string text = ((TextBox)sender).Text;
            int i = 0;
            while (t[i] != sender)
                i++;
            ItemChangedEventArgs e1 = new ItemChangedEventArgs(i, name, text);
            if (ItemChanged != null && t[i].Text != tInhalt[i])
            {
                tInhalt[i] = t[i].Text;
                ItemChanged(this, e1);
            }
        }
        /// <summary>
        /// Position und Breite der Textboxen neu berechnen
        /// </summary>
        public void resizeBoxes()
        {
            if (t.Count == dgv.Columns.Count)
            {
                t[0].Left = dgv.Left + dgv.RowHeadersWidth - dgv.HorizontalScrollingOffset;
                for (int i = 0; i < dgv.Columns.Count; i++)
                {
                    if (i > 0) 
                        t[i].Left = t[i - 1].Left + t[i - 1].Width;
                    if (t[i].Visible)
                        t[i].Width = dgv.Columns[i].Width;
                    else
                        t[i].Width = 0;
                }
                if (t.Count > 0)
                {
                    this.Width = t[t.Count - 1].Left + t[t.Count - 1].Width;
                    this.Height = t[0].Height;
                }
            }
        }
        public int getRightMargin()
        {
            return this.Left + this.Width+12;
        }
        /// <summary>
        /// Ausgabe des Wertes einer Textbox nach dem Namen der Spalte der DataGridView
        /// </summary>
        /// <param name="name">Spaltenname aus der DataGridView</param>
        /// <returns>Inhalt der Textbox die der gewählten Spalte entspricht</returns>
        public string this[string name]
        {
            get
            {
                for (int i = 0; i < t.Count; i++)
                    if (t[i].Name == name)
                        return t[i].Text;
                throw new IndexOutOfRangeException("Wert für Index " + name + " nicht vorhanden.");
            }
            set
            {
                for (int i = 0; i < t.Count; i++)
                    if (t[i].Name == name)
                    {
                        t[i].Text = value;
                        if (ItemTextChanged != null && value != tInhalt[i])
                        {
                            ItemTextChanged(this, new ItemTextChangedEventArgs(i, name, value));
                            tInhalt[i] = value;
                        }
                        if (ItemChanged != null && value != tInhalt[i])
                        {
                            ItemChanged(this, new ItemChangedEventArgs(i, name, value));
                            tInhalt[i] = value;
                        }
                        return;
                    }
                throw new IndexOutOfRangeException("Wert für Index " + name + " nicht vorhanden!");
            }
        }
        /// <summary>
        /// Ausgabe des Wertes einer Textbox nach der Nummer der Spalte der DataGridView
        /// </summary>
        /// <param name="i">Spaltenindex</param>
        /// <returns>Inhalt der Textbox die der gewählten Spalte entspricht</returns>
        public string this[int i]
        {
            get
            {
                return t[i].Text;
            }
            set
            {
                t[i].Text = value; 
                if (ItemTextChanged != null && value != tInhalt[i])
                {
                    ItemTextChanged(this, new ItemTextChangedEventArgs(i, t[i].Name, value));
                    tInhalt[i] = value;
                }
                if (ItemChanged != null && value != tInhalt[i])
                {
                    ItemChanged(this, new ItemChangedEventArgs(i, t[i].Name, value));
                    tInhalt[i] = value;
                }
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // SelectionBar
            // 
            this.Name = "SelectionBar";
            this.Load += new System.EventHandler(this.SelectionBar_Load);
            this.ResumeLayout(false);

        }

        private void SelectionBar_Load(object sender, EventArgs e)
        {

        }
    }
    /// <summary>
    /// Ereignisparameter für das ItemTextChanged-Ereignis
    /// </summary>
    public class ItemTextChangedEventArgs
    {
        public readonly int index;      // Spaltennummer
        public readonly string name;    // Spaltenname
        public readonly string text;    // Textboxinhalt der Spalte
        public ItemTextChangedEventArgs(int index, string name, string text)
        {
            this.index = index;
            this.name = name;
            this.text = text;
        }
    }
    /// <summary>
    /// Ereignisparameter für das ItemChanged-Ereignis
    /// </summary>
    public class ItemChangedEventArgs
    {
        public readonly int index;      // Spaltennummer
        public readonly string name;    // Spaltenname
        public readonly string text;    // Textboxinhalt der Spalte
        public ItemChangedEventArgs(int index, string name, string text)
        {
            this.index = index;
            this.name = name;
            this.text = text;
        }
    }
}
