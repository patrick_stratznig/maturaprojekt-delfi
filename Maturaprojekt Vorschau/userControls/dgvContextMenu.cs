﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Maturaprojekt_Vorschau
{
    public partial class dgvContextMenu : UserControl
    {
        private DataGridView dgv;
        public dgvContextMenu()
        {
            InitializeComponent();
        }
        public dgvContextMenu(DataGridView dgvForm)
        {
            InitializeComponent();
            dgvForm.ContextMenuStrip = cMS;
            this.dgv = dgvForm;
        }
        public void deleteRow()
        {
            dgv.Rows.RemoveAt(dgv.SelectedRows[0].Index);
        }

        private void löschenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            deleteRow();
        }
    }
}
