﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Maturaprojekt_Vorschau
{
    public partial class ownTextbox : UserControl
    {
        public ownTextbox()
        {
            InitializeComponent();
        }
        public ownTextbox(string title)
        {
            InitializeComponent();
            groupBox10.Text = title;
        }
        public string editTitle
        {
            get { return groupBox10.Text; }
            set { groupBox10.Text = value; }
        }

        public override string Text
        {
            get { return txtTextbox.Text; }
            set { txtTextbox.Text = value; }
        }
    }
}
