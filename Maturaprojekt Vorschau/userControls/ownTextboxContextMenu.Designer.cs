﻿namespace Maturaprojekt_Vorschau
{
    partial class ownTextboxContextMenu
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode fuer die Designerunterstuetzung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geaendert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cMS = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.titelToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cMS.SuspendLayout();
            this.SuspendLayout();
            // 
            // cMS
            // 
            this.cMS.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.cMS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.titelToolStripMenuItem1});
            this.cMS.Name = "contextMenuStrip1";
            this.cMS.Size = new System.Drawing.Size(284, 42);
            // 
            // titelToolStripMenuItem1
            // 
            this.titelToolStripMenuItem1.Name = "titelToolStripMenuItem1";
            this.titelToolStripMenuItem1.Size = new System.Drawing.Size(283, 38);
            this.titelToolStripMenuItem1.Text = "Titel bearbeiten";
            this.titelToolStripMenuItem1.Click += new System.EventHandler(this.titelToolStripMenuItem1_Click);
            // 
            // ownTextboxContextMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ownTextboxContextMenu";
            this.Size = new System.Drawing.Size(367, 303);
            this.cMS.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip cMS;
        private System.Windows.Forms.ToolStripMenuItem titelToolStripMenuItem1;
    }
}
