﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Maturaprojekt_Vorschau
{
    public partial class ownTextboxContextMenu : UserControl
    {
        ownTextbox otxt;
        public ownTextboxContextMenu()
        {
            InitializeComponent();
        }
        public ownTextboxContextMenu(ownTextbox o)
        {
            this.otxt = o;
            o.ContextMenuStrip = cMS;
            InitializeComponent();
        }

        private void titelToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Textfeld txt = new Textfeld(otxt.editTitle);
            if (txt.ShowDialog() == DialogResult.OK)
                otxt.editTitle = txt.Eingabe;
        }
    }
}
